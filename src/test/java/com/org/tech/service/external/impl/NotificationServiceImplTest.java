package com.org.tech.service.external.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.DestinationNotFoundException;
import com.org.tech.factory.EquipmentFactory;
import com.org.tech.factory.UserFactory;
import com.org.tech.model.Employee;
import com.org.tech.model.Equipment;
import com.org.tech.model.dto.MailBodyDto;
import com.org.tech.providers.MessageProvider;
import com.org.tech.utils.MailSenderUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class NotificationServiceImplTest {

    private Employee employee;
    private Employee employeeB;
    private Equipment equipment;
    private Equipment equipmentB;
    @Mock
    private RestTemplate restTemplate;

    @Mock
    private MessageProvider message;

    @Mock
    private MailSenderUtil mailSenderUtil;

    @InjectMocks
    private NotificationServiceImpl notificationService;

    @BeforeEach
    void setUp() {

        employee = new UserFactory().newInstanceEmployee();

        employeeB = new UserFactory().newInstanceEmployee();
        employeeB.setTokenNotification(null);

        equipment = new EquipmentFactory().newInstance();
        equipment.getAdmissionDataSheet().setEmployee(employee);

        equipmentB = new EquipmentFactory().newInstance();
        equipmentB.getAdmissionDataSheet().setEmployee(employeeB);

        ReflectionTestUtils.setField(notificationService, "ulrNotification", "https://fcm.googleapis.com/fcm/send");
        ReflectionTestUtils.setField(notificationService, "tokenServer", "key=AAAAdZg4Eic:APA91bGANm1HbysvPWebvQfXXdUCxQ9KvPeTNG8jv1hPLzH_mn3RJ4mH1JvPInuhA7kz9sCjUvpClyIZQoE5If8asjZsBEQXX4Wg8DXVa0ZA3iuwUq0ABsQOWx3O9LtcmPRAN70UugxV");

    }

    @Test
    void whenSendNotificationByEmployee_thenError() {
        given(message.get(any(MessageEnum.class))).willReturn("error");
        Assertions.assertThrows(DestinationNotFoundException.class,
                () -> notificationService.sendNotificationByEmployee(employeeB));
    }

    @Test
    void whenSendNotificationByEmployee_thenOk() {
        lenient().when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class)))
                .thenReturn(ResponseEntity.ok("ok"));
        try {
            notificationService.sendNotificationByEmployee(employee);
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    @Test
    void whenSendNotificationForEmail_thenOk() {
        given(message.get(any(MessageEnum.class), any(Object[].class))).willReturn("message");
        doNothing().when(mailSenderUtil).sendSimpleMail(any(MailBodyDto.class));
        try {
            notificationService.sendNotificationForEmail(equipment);
        }catch (Exception e){
            Assertions.fail();
        }
    }

    @Test
    void whenSendAlertStatus_thenOk() {
        lenient().when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class)))
                .thenReturn(ResponseEntity.ok("ok"));
        try {
            notificationService.sendAlertStatus(equipment);
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    @Test
    void whenSendAlertStatus_thenDoNothing() {
        lenient().when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class)))
                .thenReturn(ResponseEntity.ok("ok"));
        try {
            notificationService.sendAlertStatus(equipmentB);
        } catch (Exception e) {
            Assertions.fail();
        }
    }
}