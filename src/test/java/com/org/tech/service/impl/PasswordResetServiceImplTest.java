package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.PasswordMismatchException;
import com.org.tech.factory.UserFactory;
import com.org.tech.model.Administrator;
import com.org.tech.model.Employee;
import com.org.tech.model.dto.MailBodyDto;
import com.org.tech.model.dto.RecoverPasswordDto;
import com.org.tech.providers.MessageProvider;
import com.org.tech.service.AdministratorService;
import com.org.tech.service.EmployeeService;
import com.org.tech.utils.GeneratePassword;
import com.org.tech.utils.MailSenderUtil;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

@ExtendWith(MockitoExtension.class)
class PasswordResetServiceImplTest {

    public static final String TEMPORAL_PASSWORD_SEND = "A temporary password has been sent to your email.";
    public static final String UPDATED_SUCCESSFULLY = "Your password has been updated successfully";
    private DataFactory dataFactory;
    private Employee employee;
    private Administrator administrator;
    private RecoverPasswordDto recoverAdmin;
    private RecoverPasswordDto recoverEmployee;

    @Mock
    private AdministratorService administratorService;

    @Mock
    private EmployeeService employeeService;

    @Mock
    private MessageProvider message;

    @Spy
    private GeneratePassword generatePassword;

    @Spy
    private PasswordEncoder passwordEncoder;

    @Mock
    private MailSenderUtil mailSenderUtil;

    @InjectMocks
    private PasswordResetServiceImpl passwordResetService;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
        employee = new UserFactory().newInstanceEmployee();
        administrator = new UserFactory().newInstanceAdministrator();
        String password = dataFactory.getRandomWord();
        recoverAdmin = RecoverPasswordDto.builder()
                .oldPassword(administrator.getPassword())
                .password(password)
                .verifiedPassword(password)
                .rol("ADMIN")
                .username("jsrincon")
                .build();

        recoverEmployee = RecoverPasswordDto.builder()
                .oldPassword(employee.getPassword())
                .password(password)
                .verifiedPassword(password)
                .rol("EMPLOYEE")
                .username("jfrincon")
                .build();
    }

    @Test
    void whenRecoverPassEmployee_thenSendPassword() {
        given(employeeService.getEmployeeByUsername(anyString())).willReturn(employee);
        given(employeeService.update(any(Employee.class))).willReturn(employee);
        given(message.get(any(MessageEnum.class))).willReturn(TEMPORAL_PASSWORD_SEND);
        willDoNothing().given(mailSenderUtil).sendSimpleMail(any(MailBodyDto.class));

        String response = passwordResetService.recoverPassword(dataFactory.getName());

        assertEquals(TEMPORAL_PASSWORD_SEND, response);
    }

    @Test
    void whenRecoverPassAdministrator_thenSendPassword() {
        given(administratorService.getAdministratorByUsername(anyString())).willReturn(administrator);
        given(administratorService.update(any(Administrator.class))).willReturn(administrator);
        given(message.get(any(MessageEnum.class))).willReturn(TEMPORAL_PASSWORD_SEND);
        willDoNothing().given(mailSenderUtil).sendSimpleMail(any(MailBodyDto.class));

        String response = passwordResetService.recoverPassword(dataFactory.getName());

        assertEquals(TEMPORAL_PASSWORD_SEND, response);
    }

    @Test
    void whenRecoverPass_thenThrow() {
        String data = dataFactory.getName();
        assertThrows(BadCredentialsException.class, () -> passwordResetService.recoverPassword(data));
    }

    @Test
    void whenSetPasswordEmployee_thenUpdatePassword() {
        given(employeeService.getEmployeeByUsername(anyString())).willReturn(employee);
        given(employeeService.update(any(Employee.class))).willReturn(employee);
        given(passwordEncoder.matches(anyString(), anyString())).willReturn(true);
        given(message.get(any(MessageEnum.class))).willReturn(UPDATED_SUCCESSFULLY);

        String response = passwordResetService.setPassword(recoverEmployee);

        assertThat(UPDATED_SUCCESSFULLY).isEqualTo(response);
    }

    @Test
    void whenSetPasswordEmployee_thenThrow() {
        given(employeeService.getEmployeeByUsername(anyString())).willReturn(employee);

        assertThrows(PasswordMismatchException.class, () ->
                passwordResetService.setPassword(recoverEmployee)
        );
    }

    @Test
    void whenSetPasswordAdministrator_thenUpdatePassword() {
        given(administratorService.getAdministratorByUsername(anyString())).willReturn(administrator);
        given(administratorService.update(any(Administrator.class))).willReturn(administrator);
        given(passwordEncoder.matches(anyString(), anyString())).willReturn(true);
        given(message.get(any(MessageEnum.class))).willReturn(UPDATED_SUCCESSFULLY);

        String response = passwordResetService.setPassword(recoverAdmin);

        assertThat(UPDATED_SUCCESSFULLY).isEqualTo(response);
    }

    @Test
    void whenSetPasswordAdministrator_thenThrow() {
        given(administratorService.getAdministratorByUsername(anyString())).willReturn(administrator);

        assertThrows(PasswordMismatchException.class, () ->
                passwordResetService.setPassword(recoverAdmin)
        );
    }
}