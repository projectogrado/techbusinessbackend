package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.factory.RoleFactory;
import com.org.tech.factory.UserFactory;
import com.org.tech.model.Customer;
import com.org.tech.model.Role;
import com.org.tech.model.UserApp;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.CustomerRepository;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {

    private DataFactory dataFactory;
    private Customer sampleCustomer;
    private List<Customer> customerList;
    private Set<Role> role;
    private String name;
    @Mock
    private MessageProvider message;

    @Mock
    private RoleServiceImpl roleService;

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerServiceImpl customerService;

    @BeforeEach
    public void setup() {
        dataFactory = new DataFactory();
        sampleCustomer = new UserFactory().newInstanceCustomer();
        customerList = new ArrayList<>();
        name = dataFactory.getName();
        customerList.add(sampleCustomer);

        role = new HashSet<>();
        role.add(new RoleFactory().RoleFactoryDefault());
    }

    @Test
    void whenCreateCustomer_theReturnCustomer() {
        lenient().when(roleService.addRoleToUser(anyString())).thenReturn(role);
        lenient().when(customerRepository.save(any(Customer.class))).thenReturn(sampleCustomer);
        UserApp response = customerService.create(sampleCustomer);
        assertThat(response).isNotNull();
    }


    @Test
    void whenUpdateCustomer_theReturnCustomer() {
        lenient().when(customerRepository.existsById(anyLong())).thenReturn(true);
        lenient().when(customerRepository.save(any(Customer.class))).thenReturn(sampleCustomer);
        UserApp response = customerService.update(sampleCustomer);
        assertThat(response).isNotNull();
    }

    @Test
    void whenUpdateCustomer_thenThrow() {
        lenient().when(message.get(any(MessageEnum.class))).thenReturn(dataFactory.getName());
        lenient().when(customerRepository.existsById(anyLong())).thenReturn(false);
        lenient().when(customerRepository.save(any(Customer.class))).thenReturn(sampleCustomer);
        Assertions.assertThrows(ResourceNotFoundException.class,
                () -> customerService.update(sampleCustomer));
    }

    @Test
    void whenGetAllCustomers_thenReturnCustomers() {
        lenient().when(customerRepository.findAll()).thenReturn(customerList);
        List<Customer> responseList = customerService.getAllCustomers();
        assertThat(responseList).isNotEmpty();
    }

    @Test
    void whenGetCustomerByDocument_thenReturnCustomer() {
        lenient().when(customerRepository.findCustomerByDocument(anyString())).thenReturn(Optional.of(sampleCustomer));
        Customer response = customerService.getCustomerByDocument(dataFactory.getName());
        assertThat(response.getName()).isEqualTo(sampleCustomer.getName());
        assertThat(response.getLastname()).isEqualTo(sampleCustomer.getLastname());
        assertThat(response.getAddress()).isEqualTo(sampleCustomer.getAddress());
        assertThat(response.getDocument()).isEqualTo(sampleCustomer.getDocument());
    }

    @Test
    void whenGetCustomerByDocument_thenThrow() {
        lenient().when(customerRepository.findCustomerByDocument(anyString())).thenReturn(Optional.empty());
        Assertions.assertThrows(ResourceNotFoundException.class,
                () -> customerService.getCustomerByDocument(name));
    }
}