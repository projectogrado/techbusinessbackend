package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.factory.ParametersFactory;
import com.org.tech.model.Parameters;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.ParametersRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class ParametersServiceImplTest {
    private Parameters parameters;
    @Mock
    private ParametersRepository parametersRepository;

    @Mock
    private MessageProvider message;

    @InjectMocks
    private ParametersServiceImpl parametersService;

    @BeforeEach
    void setUp() {
        parameters = new ParametersFactory().newInstance();
    }

    @Test
    void whenCreateParameters_thenPersisted() {
        given(parametersRepository.save(any(Parameters.class))).willReturn(parameters);
        var response = parametersService.createParameters(parameters);

        assertThat(response).isNotNull();
        assertThat(parameters).isEqualTo(response);
    }

    @Test
    void whenGetParameters_thenReturn() {
        given(parametersRepository.findTopByOrderByDateCreatedDesc()).willReturn(Optional.of(parameters));
        var response = parametersService.getParameters();

        assertThat(response).isNotNull();
        assertThat(parameters).isEqualTo(response);
    }

    @Test
    void whenGetParameters_thenException() {
        given(parametersRepository.findTopByOrderByDateCreatedDesc()).willReturn(Optional.empty());
        given(message.get(any(MessageEnum.class))).willReturn("message");
        Assertions.assertThrows(ResourceNotFoundException.class, () -> parametersService.getParameters());
    }
}