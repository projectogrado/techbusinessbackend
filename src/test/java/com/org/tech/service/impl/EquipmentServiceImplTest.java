package com.org.tech.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.factory.EquipmentFactory;
import com.org.tech.model.Equipment;
import com.org.tech.model.enums.StatusEquipment;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.EquipmentRepository;
import com.org.tech.service.external.impl.NotificationServiceImpl;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class EquipmentServiceImplTest {

    private Equipment equipment;
    private Equipment equipmentStatusToDo;
    private Equipment equipmentStatus;
    private List<Equipment> equipmentList;
    private Map<String, String> params;
    private DataFactory dataFactory;

    @Mock
    private MessageProvider message;

    @Spy
    private ObjectMapper objectMapper;

    @Mock
    private NotificationServiceImpl notificationFirebaseService;

    @Mock
    private EquipmentRepository equipmentRepository;

    @InjectMocks
    private EquipmentServiceImpl equipmentService;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
        equipment = new EquipmentFactory().newInstance();
        equipmentStatusToDo = new EquipmentFactory().setStatus(StatusEquipment.TO_DO).newInstance();
        equipmentStatus = new EquipmentFactory().newInstance();
        equipmentStatus.setStatus(StatusEquipment.BY_ASSIGNING);
        params = new HashMap<>();
        equipmentList = new ArrayList<>();
        equipmentList.add(equipment);
    }

    @Test
    void whenCreate_thenEquipmentPersisted() {
        lenient().when(equipmentRepository.save(any(Equipment.class))).thenReturn(equipment);
        Equipment response = equipmentService.create(equipment);
        assertThat(response).isNotNull();
    }

    @Test
    void whenGetAllEquipments_theReturnEquipments() {
        lenient().when(equipmentRepository.findAll(any(Sort.class))).thenReturn(equipmentList);
        List<Equipment> responseList = equipmentService.getAllEquipment(params);
        assertThat(responseList).isNotEmpty();
    }

    @Test
    void whenGetAllFilterEquipments_thenReturnEquipmentFiltered() {
        params.put("sort", "model");
        params.put("model", "A504UF");
        lenient().when(equipmentRepository.findAll(any(Sort.class))).thenReturn(equipmentList);
        lenient().when(equipmentRepository.findAll(any(Example.class), any(Sort.class)))
                .thenReturn(equipmentList);
        List<Equipment> responseList = equipmentService.getAllEquipment(params);
        assertThat(responseList).isNotEmpty();
    }

    @Test
    void whenUpdate_thenEquipmentUpdated() {
        lenient().when(equipmentRepository.findById(anyLong())).thenReturn(Optional.of(equipment));
        lenient().when(equipmentRepository.save(any(Equipment.class))).thenReturn(equipment);
        Equipment response = equipmentService.update(equipment);
        assertThat(response).isNotNull();
    }

    @Test
    void whenUpdateStatus_thenEquipmentUpdated() {
        doNothing().when(notificationFirebaseService).sendNotificationForEmail(any(Equipment.class));
        lenient().when(equipmentRepository.findById(anyLong())).thenReturn(Optional.of(equipmentStatus));
        lenient().when(equipmentRepository.save(any(Equipment.class))).thenReturn(equipment);
        Equipment response = equipmentService.update(equipment);
        assertThat(response).isNotNull();
    }

    @Test
    void whenUpdate_thenThrows() {
        lenient().when(message.get(any(MessageEnum.class))).thenReturn(dataFactory.getName());
        lenient().when(equipmentRepository.findById(anyLong())).thenReturn(Optional.empty());
        lenient().when(equipmentRepository.save(any(Equipment.class))).thenReturn(equipment);

        Assertions.assertThrows(ResourceNotFoundException.class, () -> equipmentService.update(equipment));
    }

    @Test
    void whenUpdateStatusStatusToDo_thenEquipmentUpdated() {
        lenient().when(equipmentRepository.findById(anyLong())).thenReturn(Optional.of(equipmentStatus));
        lenient().when(equipmentRepository.save(any(Equipment.class))).thenReturn(equipmentStatusToDo);
        Equipment response = equipmentService.update(equipmentStatusToDo);
        assertThat(response).isNotNull();
    }
}