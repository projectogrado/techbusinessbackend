package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.factory.RoleFactory;
import com.org.tech.factory.UserFactory;
import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.Employee;
import com.org.tech.model.Role;
import com.org.tech.model.UserApp;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.EmployeeRepository;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplTest {

    public static final long OLD_EMPLOYEE_ID = 2L;
    public static final long NEW_EMPLOYEE_ID = 1L;
    public static final long ADMISSION_DATA_SHEET_NOT_EXIST_ID = 4L;
    public static final long ADMISSION_DATA_SHEET_ID = 2L;
    public static final String USERNAME = "jsrincon";

    private DataFactory dataFactory;
    private Employee sampleEmployee;
    private Employee sampleEmployee2;
    private List<Employee> employeeList;
    private Set<Role> role;

    @Mock
    private Authentication authentication;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private EmployeeRepository employeeRepository;
    @Mock
    private RoleServiceImpl roleService;
    @Spy
    private BCryptPasswordEncoder bcryptEncoder;
    @Mock
    private MessageProvider message;
    @InjectMocks
    private EmployeeServiceImpl employeeService;


    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
        AdmissionDataSheet admissionDataSheet = new AdmissionDataSheet();
        admissionDataSheet.setId(2L);

        sampleEmployee2 = new UserFactory().newInstanceEmployee();
        sampleEmployee2.setWorks(new ArrayList<>(
                Collections.singletonList(admissionDataSheet)));

        sampleEmployee = new UserFactory().newInstanceEmployee();

        employeeList = new ArrayList<>();
        employeeList.add(sampleEmployee);

        role = new HashSet<>();
        role.add(new RoleFactory().RoleEmployeeFactory());
        role.add(new RoleFactory().RoleFactoryDefault());
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    void whenCreate_theReturnEmployee() {
        lenient().when(roleService.addRoleToUser(anyString())).thenReturn(role);
        lenient().when(employeeRepository.save(any(Employee.class))).thenReturn(sampleEmployee);
        UserApp response = employeeService.create(sampleEmployee);
        assertThat(response).isNotNull();
    }

    @Test
    void whenUpdate_theReturnEmployee() {
        lenient().when(employeeRepository.existsById(anyLong())).thenReturn(true);
        lenient().when(employeeRepository.save(any(Employee.class))).thenReturn(sampleEmployee);
        UserApp response = employeeService.update(sampleEmployee);
        assertThat(response).isNotNull();
    }

    @Test
    void whenUpdate_thenThrow() {
        lenient().when(message.get(any(MessageEnum.class))).thenReturn(dataFactory.getName());
        lenient().when(employeeRepository.existsById(anyLong())).thenReturn(false);
        lenient().when(employeeRepository.save(any(Employee.class))).thenReturn(sampleEmployee);
        Assertions.assertThrows(ResourceNotFoundException.class,
                () -> employeeService.update(sampleEmployee));
    }

    @Test
    void whenGetAll_thenReturnEmployees() {
        Map<String,String> params = new HashMap<>();
        lenient().when(employeeRepository.findAll(any(Sort.class))).thenReturn(employeeList);
        List<Employee> responseList = employeeService.getAllEmployees(params);
        assertThat(responseList).isNotEmpty();
    }

    @Test
    void whenGetAllWithParam_thenReturnEmployees() {
        Map<String,String> params = new HashMap<>();
        params.put("document","1144201661");
        lenient().when(employeeRepository.findAllByDocument(anyString(),any(Sort.class))).thenReturn(employeeList);
        List<Employee> responseList = employeeService.getAllEmployees(params);
        assertThat(responseList).isNotEmpty();
    }

    @Test
    void whenFindByUsername_thenReturnEmployee() {
        lenient().when(employeeRepository.findByUsername(anyString()))
                .thenReturn(Optional.of(sampleEmployee));
        UserApp userDocument = employeeService.getEmployeeByUsername(USERNAME);
        assertThat(userDocument).isNotNull();
    }

    @Test
    void whenFindByUsername_thenReturnNull() {
        lenient().when(employeeRepository.findByUsername(anyString()))
                .thenReturn(Optional.empty());
        UserApp userDocument = employeeService.getEmployeeByUsername(USERNAME);
        assertThat(userDocument).isNull();
    }

    @Test
    void whenGetMe_thenReturnEmployee() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(SecurityContextHolder.getContext().getAuthentication().getName()).thenReturn(dataFactory.getName());
        lenient().when(employeeRepository.findByUsername(anyString())).thenReturn(Optional.of(sampleEmployee));
        Employee response = employeeService.getEmployeeContext();
        assertThat(response).isEqualTo(sampleEmployee);
    }

    @Test
    void whenGetMe_thenThrow() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(SecurityContextHolder.getContext().getAuthentication().getName()).thenReturn(dataFactory.getName());

        lenient().when(employeeRepository.findByUsername(anyString()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(ResourceNotFoundException.class, () -> employeeService
                .getEmployeeContext());
    }

    @Test
    void whenGetLastUpdateWithOutIds_thenReturn() {
        lenient().when(employeeRepository.findFirstByOrderByUpdatedDateTimeAsc()).thenReturn(Optional.of(sampleEmployee));
        var employee = employeeService.getEmployeeLastUpdate(Collections.emptyList());
        assertThat(sampleEmployee).isEqualTo(employee);
    }

    @Test
    void whenGetLastUpdateWithOutIds_thenException() {
        List<Long> list = Collections.emptyList();
        lenient().when(employeeRepository.findFirstByOrderByUpdatedDateTimeAsc()).thenReturn(Optional.empty());
        Assertions.assertThrows(ResourceNotFoundException.class,
                () -> employeeService.getEmployeeLastUpdate(list));
    }

    @Test
    void whenGetLastUpdate_thenReturn() {
        lenient().when(employeeRepository.findFirstByIdNotInOrderByUpdatedDateTimeAsc(any(List.class))).thenReturn(Optional.of(sampleEmployee));
        var employee = employeeService.getEmployeeLastUpdate(Collections.singletonList(1001l));
        assertThat(sampleEmployee).isEqualTo(employee);
    }

    @Test
    void whenGetLastUpdate_thenException() {
        var list = Collections.singletonList(1001L);
        lenient().when(employeeRepository.findFirstByIdNotInOrderByUpdatedDateTimeAsc(any(List.class))).thenReturn(Optional.empty());
        Assertions.assertThrows(ResourceNotFoundException.class,
                () -> employeeService.getEmployeeLastUpdate(list));
    }
}