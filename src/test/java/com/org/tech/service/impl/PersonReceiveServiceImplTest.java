package com.org.tech.service.impl;

import com.org.tech.factory.PersonReceiveFactory;
import com.org.tech.model.PersonReceive;
import com.org.tech.repository.PersonReceiveRepository;
import com.org.tech.service.DischargeDataSheetService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class PersonReceiveServiceImplTest {

    private PersonReceive personReceive;

    @Mock
    private PersonReceiveRepository deliveryRepository;

    @Mock
    private DischargeDataSheetService dischargeDataSheetService;

    @InjectMocks
    private PersonReceiveServiceImpl deliveryService;

    @BeforeEach
    void setUp() {
        personReceive = new PersonReceiveFactory().newInstance();
    }

    @Test
    void whenCreate_thenDeliverySave() {
        doNothing().when(dischargeDataSheetService).closeAdmissionDataSheet(anyLong());
        lenient().when(deliveryRepository.save(any(PersonReceive.class))).thenReturn(personReceive);
        PersonReceive response = deliveryService.create(personReceive);
        assertThat(response).isNotNull();
    }
}