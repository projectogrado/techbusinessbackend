package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.factory.RepairRouteFactory;
import com.org.tech.model.RepairRoute;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.RepairRouteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class RepairRouteServiceImplTest {

    RepairRoute repairRoute;

    @Mock
    private RepairRouteRepository repairRouteRepository;

    @Mock
    private MessageProvider message;

    @InjectMocks
    private RepairRouteServiceImpl repairRouteService;

    @BeforeEach
    void setUp() {
        repairRoute = new RepairRouteFactory().newInstance();
    }

    @Test
    void whenGetRepairRouteById_thenReturn() {
        given(repairRouteRepository.findById(anyLong())).willReturn(Optional.of(repairRoute));
        RepairRoute response = repairRouteService.getRepairRouteById(1000L);

        assertThat(response).isNotNull();
        assertThat(repairRoute).isEqualTo(response);
    }

    @Test
    void whenGetRepairRouteById_thenException() {
        given(repairRouteRepository.findById(anyLong())).willReturn(Optional.empty());
        given(message.get(any(MessageEnum.class))).willReturn("message");
        Assertions.assertThrows(ResourceNotFoundException.class, () ->repairRouteService.getRepairRouteById(1000L));
    }
}