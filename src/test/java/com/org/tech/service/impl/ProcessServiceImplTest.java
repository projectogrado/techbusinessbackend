package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.factory.ProcessFactory;
import com.org.tech.model.Process;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.ProcessRepository;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class ProcessServiceImplTest {

    private Process process;
    private List<Process> processList;
    private DataFactory dataFactory;


    @Mock
    private MessageProvider message;

    @Mock
    private ProcessRepository processRepository;

    @InjectMocks
    private ProcessServiceImpl processService;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
        process = new ProcessFactory().newInstance();
        processList = new ArrayList<>();
        processList.add(process);
    }

    @Test
    void whenCreate_thenProcessPersisted() {
        lenient().when(processRepository.save(any(Process.class))).thenReturn(process);
        Process response = processService.create(process);
        assertThat(response).isNotNull();
    }

    @Test
    void whenGetAllProcess_thenReturnProcess() {
        lenient().when(processRepository.findAll(any(Sort.class))).thenReturn(processList);
        List<Process> responseList = processService.getAllProcess();
        assertThat(responseList).isNotEmpty();
    }

    @Test
    void whenUpdate_thenProcessUpdated() {
        lenient().when(processRepository.existsById(anyLong())).thenReturn(true);
        lenient().when(processRepository.save(any(Process.class))).thenReturn(process);
        Process response = processService.update(process);
        assertThat(response).isNotNull();
    }

    @Test
    void update_thenThrows() {
        lenient().when(message.get(any(MessageEnum.class))).thenReturn(dataFactory.getName());
        lenient().when(processRepository.existsById(anyLong())).thenReturn(false);
        lenient().when(processRepository.save(any(Process.class))).thenReturn(process);

        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            processService.update(process);
        });

    }
}