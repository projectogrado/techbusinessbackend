package com.org.tech.service.impl;

import com.org.tech.factory.RoleFactory;
import com.org.tech.model.Role;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.RoleRepository;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class RoleServiceImplTest {

    private DataFactory dataFactory;
    private Role role;

    @Mock
    private MessageProvider message;

    @Mock
    private RoleRepository roleRepository;

    @InjectMocks
    private RoleServiceImpl roleService;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
        role = new RoleFactory().RoleFactoryDefault();
    }

    @Test
    void whenFindByNameIsNull_thenRoleReturnDefault() {
        lenient().when(roleRepository.findByName(anyString())).thenReturn(role);
        Set<Role> response = roleService.addRoleToUser(null);
        assertThat(response).isNotEmpty();
    }

    @Test
    void whenFindByName_thenRoleReturn() {
        lenient().when(roleRepository.findByName(anyString())).thenReturn(role);
        Set<Role> response = roleService.addRoleToUser(dataFactory.getName());
        assertThat(response).isNotEmpty();
    }
}