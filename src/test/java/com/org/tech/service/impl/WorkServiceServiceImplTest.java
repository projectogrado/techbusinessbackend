package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.ChecklistRepair;
import com.org.tech.model.RepairRoute;
import com.org.tech.model.WorkService;
import com.org.tech.model.keys.ChecklistRepairKey;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.WorkServiceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;

@ExtendWith(MockitoExtension.class)
class WorkServiceServiceImplTest {

    @Mock
    private ChecklistRepairServiceImpl mockChecklistRepairService;

    @Mock
    private WorkServiceRepository mockWorkServiceRepository;

    @Mock
    private RepairRouteServiceImpl mockRepairRouteService;

    @Mock
    private MessageProvider messageProvider;

    @InjectMocks
    private WorkServiceServiceImpl mockWorkServiceService;

    private WorkService workService;

    @BeforeEach
    void setUp() {
        workService = new WorkService();
    }

    @Test
    void whenCreateWorkService_thenReturnPersisted() {
        given(mockWorkServiceRepository.save(any(WorkService.class))).willReturn(workService);
        WorkService response = mockWorkServiceService.create(workService);
        assertThat(response).isNotNull();
    }


    @Test
    void whenAddCheckList_thenReturnPersisted() {
        ChecklistRepair checklistRepair = new ChecklistRepair();
        ChecklistRepairKey key = new ChecklistRepairKey();
        key.setWorkServiceId(1001L);
        key.setRepairRouteId(1001L);
        checklistRepair.setId(key);

        List<ChecklistRepair> items = Collections.singletonList(checklistRepair);
        RepairRoute repairRoute = new RepairRoute();
        repairRoute.setId(100L);

        given(mockWorkServiceRepository.findById(anyLong())).willReturn(Optional.of(workService));
        given(mockRepairRouteService.getRepairRouteById(anyLong())).willReturn(repairRoute);
        given(mockChecklistRepairService.saveAllChecklistRepair(any(List.class))).willReturn(items);
        given(mockWorkServiceRepository.save(any(WorkService.class))).willReturn(workService);
        given(messageProvider.get(any(MessageEnum.class))).willReturn("message");

        String response = mockWorkServiceService.addChecklistWorkService(items, 1001L);
        assertThat(response).isNotNull();
    }

    @Test
    void whenAddCheckList_thenException() {
        ChecklistRepair checklistRepair = new ChecklistRepair();
        ChecklistRepairKey key = new ChecklistRepairKey();
        key.setWorkServiceId(1001L);
        key.setRepairRouteId(1001L);
        checklistRepair.setId(key);

        List<ChecklistRepair> items = Collections.singletonList(checklistRepair);
        RepairRoute repairRoute = new RepairRoute();
        repairRoute.setId(100L);

        given(messageProvider.get(any(MessageEnum.class))).willReturn("message");
        given(mockWorkServiceRepository.findById(anyLong())).willReturn(Optional.empty());

        Assertions.assertThrows(ResourceNotFoundException.class, () -> mockWorkServiceService.addChecklistWorkService(items, 1001L));
    }

    @Test
    void whenAddCheckListNotChecklist_thenException() {
        ChecklistRepair checklistRepair = new ChecklistRepair();
        ChecklistRepairKey key = new ChecklistRepairKey();
        key.setWorkServiceId(1001L);
        key.setRepairRouteId(1001L);
        checklistRepair.setId(key);

        List<ChecklistRepair> items = Collections.singletonList(checklistRepair);
        RepairRoute repairRoute = new RepairRoute();
        repairRoute.setId(100L);
        given(mockWorkServiceRepository.findById(anyLong())).willReturn(Optional.of(workService));

        willThrow(new ResourceNotFoundException("Error"))
                .given(mockRepairRouteService)
                .getRepairRouteById(anyLong());

        Assertions.assertThrows(ResourceNotFoundException.class, () -> mockWorkServiceService.addChecklistWorkService(items, 1001L));
    }

    @Test
    void whenAddCheckListNotWorkService_thenException() {
        ChecklistRepair checklistRepair = new ChecklistRepair();
        ChecklistRepairKey key = new ChecklistRepairKey();
        key.setWorkServiceId(1002L);
        key.setRepairRouteId(1002L);
        checklistRepair.setId(key);

        List<ChecklistRepair> items = Collections.singletonList(checklistRepair);
        RepairRoute repairRoute = new RepairRoute();
        repairRoute.setId(100L);

        given(mockWorkServiceRepository.findById(anyLong()))
                .will((InvocationOnMock invocation) ->
                        invocation.getArgument(0).equals(1002L)
                                ? Optional.empty()
                                : Optional.of(workService));

        given(messageProvider.get(any(MessageEnum.class))).willReturn("message");

        Assertions.assertThrows(ResourceNotFoundException.class, () -> mockWorkServiceService.addChecklistWorkService(items, 1002L));
    }

    @Test
    void whenGetAllWorkServiceByAdmission_thenReturn() {
        given(mockWorkServiceRepository.findAllByAdmissionDataSheet_Id(anyLong())).willReturn(Collections.singletonList(workService));
        List<WorkService> response = mockWorkServiceService.getAllWorkServiceByAdmission(1002L);
        assertThat(response).isNotEmpty();
        assertThat(workService).isEqualTo(response.get(0));
    }
}