package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.factory.AdmissionDataSheetFactory;
import com.org.tech.factory.ServiceFactory;
import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.WorkService;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.AdmissionDataSheetRepository;
import com.org.tech.utils.PdfUtil;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;


@ExtendWith(MockitoExtension.class)
class AdmissionDataSheetServiceImplTest {

    private AdmissionDataSheet admission;
    private DataFactory dataFactory;

    @Mock
    private AdmissionDataSheetRepository admissionRepository;

    @Mock
    private AssignJobsServiceImpl assignJobsService;

    @Mock
    private BlacklistServiceImpl blacklistService;

    @Mock
    private MessageProvider message;

    @Mock
    private ServiceServiceImpl serviceService;

    @Mock
    private WorkServiceServiceImpl workServiceService;

    @Spy
    private SpringTemplateEngine templateEngine;

    @Mock
    private QRCodeServiceImpl qrCodeService;

    @Mock
    private PdfUtil pdfUtil;

    @InjectMocks
    private AdmissionDataSheetServiceImpl admissionService;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
        admission = new AdmissionDataSheetFactory(true).newInstance();
        ReflectionTestUtils.setField(admissionService, "dir", "/home/personal/");
    }

    @Test
    void whenGetAdmissionDataSheetById_thenReturnAdmissionDataSheet() {
        lenient().when(admissionRepository.findById(anyLong())).thenReturn(Optional.of(admission));
        AdmissionDataSheet response = admissionService.getAdmissionDataSheetById(1L);
        assertThat(response).isNotNull();
    }

    @Test
    void whenGetAdmissionDataSheetById_thenThrows() {
        lenient().when(message.get(any(MessageEnum.class))).thenReturn(dataFactory.getName());
        lenient().when(admissionRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> admissionService.getAdmissionDataSheetById(1L));
    }

    @Test
    void whenCreateAdmissionData_thenAdmissionPersisted() {
        lenient().when(serviceService.getServiceById(anyLong())).thenReturn(new ServiceFactory().newInstance());
        lenient().when(workServiceService.create(any())).thenReturn(new WorkService());
        lenient().when(admissionRepository.save(any(AdmissionDataSheet.class))).thenReturn(admission);
        doNothing().when(assignJobsService).assignJobs(anyLong());
        AdmissionDataSheet response = admissionService.create(admission);
        assertThat(response).isNotNull();
    }

    @Test
    void whenClimbJob_thenOk() {
        lenient().when(admissionRepository.findById(anyLong())).thenReturn(Optional.of(admission));
        doNothing().when(blacklistService).create(any(AdmissionDataSheet.class));
        doNothing().when(assignJobsService).assignJobs(anyLong());
        try {
            admissionService.climbJob(1001L);
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    @Test
    void whenTerminateStatus_thenOk() {
        lenient().when(admissionRepository.findById(anyLong())).thenReturn(Optional.of(admission));
        lenient().when(admissionRepository.save(any(AdmissionDataSheet.class))).thenReturn(admission);
        var response = admissionService.terminateAdmission(admission.getId(), admission.getEquipment().getStatus());
        assertThat("Update status Ok").isEqualTo(response);
    }
}