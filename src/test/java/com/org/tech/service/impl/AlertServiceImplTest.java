package com.org.tech.service.impl;

import com.org.tech.factory.EquipmentFactory;
import com.org.tech.factory.ParametersFactory;
import com.org.tech.model.Equipment;
import com.org.tech.model.Parameters;
import com.org.tech.model.enums.StatusEquipment;
import com.org.tech.service.external.impl.NotificationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class AlertServiceImplTest {
    List<Equipment> equipmentList;
    Parameters parameters;

    @Mock
    private EquipmentServiceImpl equipmentService;

    @Mock
    private NotificationServiceImpl notificationService;

    @Mock
    private ParametersServiceImpl parametersService;

    @InjectMocks
    private AlertServiceImpl alertService;

    @BeforeEach
    void setUp() {
        var equipmentA = new EquipmentFactory().setStatus(StatusEquipment.TO_DO).newInstanceWithDate();
        var equipmentB = new EquipmentFactory().setStatus(StatusEquipment.IN_REPAIR).newInstanceWithDate();
        var equipmentC = new EquipmentFactory().setStatus(StatusEquipment.REPAIRED).newInstanceWithDate();
        var equipmentD = new EquipmentFactory().setStatus(StatusEquipment.NOT_REPAIRED).newInstanceWithDate();
        var equipmentE = new EquipmentFactory().setStatus(StatusEquipment.BY_ASSIGNING).newInstanceWithDate();
        var equipmentF = new EquipmentFactory().setStatus(StatusEquipment.NOVELTY).newInstanceWithDate();
        var equipmentG = new EquipmentFactory().setDate(LocalDateTime.now()).setStatus(StatusEquipment.NOVELTY).newInstanceWithDate();

        equipmentList = Arrays.asList(equipmentA, equipmentB, equipmentC, equipmentD, equipmentE,equipmentF,equipmentG);
        parameters = new ParametersFactory().newInstance();
    }

    @Test
    void triggerAlertTime() {
        lenient().when(equipmentService.getActiveEquipment()).thenReturn(equipmentList);
        lenient().when(parametersService.getParameters()).thenReturn(parameters);
        doNothing().when(notificationService).sendAlertStatus(any(Equipment.class));
        try {
            alertService.triggerAlertTime();
        } catch (Exception e) {
            Assertions.fail();
        }
    }
}