package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.factory.RoleFactory;
import com.org.tech.factory.UserFactory;
import com.org.tech.model.Administrator;
import com.org.tech.model.Role;
import com.org.tech.model.UserApp;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.AdministratorRepository;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class AdministratorServiceImplTest {

    private DataFactory dataFactory;
    private Administrator sampleAdministrator;
    private List<Administrator> administratorList;
    private Set<Role> role;

    @Mock
    private AdministratorRepository administratorRepository;

    @Mock
    private RoleServiceImpl roleService;

    @Spy
    private PasswordEncoder passwordEncoder;

    @Mock
    private MessageProvider message;

    @InjectMocks
    private AdministratorServiceImpl administratorService;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
        sampleAdministrator = new UserFactory().newInstanceAdministrator();
        administratorList = new ArrayList<>();
        administratorList.add(sampleAdministrator);
        role = new HashSet<>();
        role.add(new RoleFactory().newInstance());
        role.add(new RoleFactory().RoleFactoryDefault());
    }

    @Test
    void whenCreate_theReturnAdministrator() {
        lenient().when(roleService.addRoleToUser(anyString())).thenReturn(role);
        lenient().when(administratorRepository.save(any(Administrator.class)))
                .thenReturn(sampleAdministrator);
        UserApp response = administratorService.create(sampleAdministrator);
        assertThat(response).isNotNull();
    }

    @Test
    void whenUpdate_theReturnAdministrator() {
        lenient().when(administratorRepository.existsById(anyLong())).thenReturn(true);
        lenient().when(administratorRepository.save(any(Administrator.class)))
                .thenReturn(sampleAdministrator);
        UserApp response = administratorService.update(sampleAdministrator);
        assertThat(response).isNotNull();
    }

    @Test
    void whenUpdate_thenThrow() {
        lenient().when(message.get(any(MessageEnum.class))).thenReturn(dataFactory.getName());
        lenient().when(administratorRepository.existsById(anyLong())).thenReturn(false);
        lenient().when(administratorRepository.save(any(Administrator.class)))
                .thenReturn(sampleAdministrator);

        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> administratorService.update(sampleAdministrator)
        );
    }

    @Test
    void whenGetAll_thenReturnAdministrator() {
        lenient().when(administratorRepository.findAll()).thenReturn(administratorList);
        List<Administrator> responseList = administratorService.getAllAdministrators();
        assertThat(responseList).isNotEmpty();
    }

    @Test
    void whenFindByUsername_thenReturnAdministrator() {
        lenient().when(administratorRepository.findByUsername(anyString()))
                .thenReturn(Optional.of(sampleAdministrator));
        UserApp userDocument = administratorService.getAdministratorByUsername("jsrincon");
        assertThat(userDocument).isNotNull();
    }
}