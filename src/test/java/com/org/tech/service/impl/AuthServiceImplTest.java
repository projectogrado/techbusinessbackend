package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.AuthException;
import com.org.tech.factory.UserFactory;
import com.org.tech.model.dto.JwtResponseDTO;
import com.org.tech.model.dto.LoginDto;
import com.org.tech.providers.MessageProvider;
import com.org.tech.security.JwtToken;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class AuthServiceImplTest {

    DataFactory dataFactory;
    @Mock
    private JwtToken jwtToken;

    private LoginDto sampleLoginAdmin;
    private LoginDto sampleLoginEmployee;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private MessageProvider message;

    @InjectMocks
    private AuthServiceImpl authService;

    @BeforeEach
    public void setup() {
        dataFactory = new DataFactory();
        sampleLoginAdmin = new UserFactory().newInstanceLogin();
        sampleLoginEmployee = new UserFactory().setUsername("jfrincon").setPassword("81943301Fe/")
                .newInstanceLogin();
    }

    @Test
    void whenFindByUsernameAdmin_thenLogin() {
        JwtResponseDTO jwtResponse = authService.authentication(sampleLoginAdmin);
        assertThat(jwtResponse).isNotNull();
    }

    @Test
    void whenFindByUsernameEmployee_thenLogin() {
        JwtResponseDTO jwtResponse = authService.authentication(sampleLoginEmployee);
        assertThat(jwtResponse).isNotNull();
    }

    @Test
    void whenAuthentication_thenBadCredential() {
        lenient().when(authenticationManager
                .authenticate(any(UsernamePasswordAuthenticationToken.class)))
                .thenThrow(BadCredentialsException.class);

        Assertions.assertThrows(AuthException.class,
                () -> authService.authentication(sampleLoginEmployee));
    }

    @Test
    void whenAuthentication_thenDisabledException() {
        lenient().when(message.get(any(MessageEnum.class))).thenReturn(dataFactory.getRandomWord());
        lenient().when(authenticationManager
                .authenticate(any(UsernamePasswordAuthenticationToken.class)))
                .thenThrow(DisabledException.class);

        Assertions.assertThrows(AuthException.class,
                () -> authService.authentication(sampleLoginEmployee));
    }
}