package com.org.tech.service.impl;

import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.factory.AdmissionDataSheetFactory;
import com.org.tech.factory.DischargeDataSheetFactory;
import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.DischargeDataSheet;
import com.org.tech.repository.DischargeDataSheetRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class DischargeDataSheetServiceImplTest {
    private DischargeDataSheet discharge;

    @Mock
    private DischargeDataSheetRepository mockDischargeDataSheetRepository;

    @Mock
    private AdmissionDataSheetServiceImpl mockAdmissionDataSheetService;

    @InjectMocks
    private DischargeDataSheetServiceImpl dischargeDataSheetService;

    @BeforeEach
    void setUp() {
        discharge = new DischargeDataSheetFactory().newInstance();
    }

    @Test
    void whenGetAdmissionDataSheetById_thenReturnAdmissionDataSheet() {
        lenient().when(mockDischargeDataSheetRepository.findByAdmissionDataSheet_Id(anyLong())).thenReturn(Optional.of(discharge));
        DischargeDataSheet response = dischargeDataSheetService.getDischargeDataSheetById(1L);
        assertThat(response).isNotNull();
    }

    @Test
    void whenGetAdmissionDataSheetById_thenCreated() {
        AdmissionDataSheet admission = new AdmissionDataSheetFactory(true).newInstance();

        lenient().when(mockDischargeDataSheetRepository.findById(anyLong())).thenReturn(Optional.empty());
        lenient().when(mockAdmissionDataSheetService.getAdmissionDataSheetById(anyLong())).thenReturn(admission);
        lenient().when(mockDischargeDataSheetRepository.save(any(DischargeDataSheet.class))).thenReturn(discharge);
        DischargeDataSheet response = dischargeDataSheetService.getDischargeDataSheetById(1L);
        assertThat(response).isNotNull();
    }

    @Test
    void whenCloseAdmission_thenOk() {
        lenient().when(mockDischargeDataSheetRepository.findById(anyLong())).thenReturn(Optional.of(discharge));
        lenient().when(mockDischargeDataSheetRepository.save(any(DischargeDataSheet.class))).thenReturn(discharge);
        try {
            dischargeDataSheetService.closeAdmissionDataSheet(1L);
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    @Test
    void whenCloseAdmission_thenException() {
        lenient().when(mockDischargeDataSheetRepository.findById(anyLong())).thenReturn(Optional.empty());
        Assertions.assertThrows(ResourceNotFoundException.class, () -> dischargeDataSheetService.closeAdmissionDataSheet(1000L));
    }
}