package com.org.tech.service.impl;

import com.org.tech.factory.AdmissionDataSheetFactory;
import com.org.tech.factory.BlacklistFactory;
import com.org.tech.factory.UserFactory;
import com.org.tech.model.Blacklist;
import com.org.tech.repository.BlacklistRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class BlacklistServiceImplTest {

    private Blacklist blacklist;

    @Mock
    private BlacklistRepository blacklistRepository;

    @InjectMocks
    private BlacklistServiceImpl blacklistService;

    @BeforeEach
    void setUp() {
        blacklist = new BlacklistFactory().newInstance();
    }

    @Test
    void whenCreate_thenBlacklist() {
        lenient().when(blacklistRepository.save(any(Blacklist.class))).thenReturn(blacklist);
        try {
            var admission = new AdmissionDataSheetFactory(true).newInstance();
            admission.setEmployee(new UserFactory().newInstanceEmployee());
            blacklistService.create(admission);
        } catch (Exception e) {
            Assertions.fail();
        }
    }


    @Test
    void whenGetBlacklist() {
        lenient().when(blacklistRepository.findAllByIdAdmissionId(anyLong())).thenReturn(Collections.singletonList(blacklist));
        List<Long> response = blacklistService.getDiscardedEmployeeByAdmission(1L);
        assertThat(response.size()).isPositive();
    }

}