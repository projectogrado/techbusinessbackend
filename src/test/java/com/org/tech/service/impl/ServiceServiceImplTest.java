package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.factory.ServiceFactory;
import com.org.tech.model.Services;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.ServiceRepository;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ServiceServiceImplTest {

    private Services service;
    private List<Services> serviceList;
    private DataFactory dataFactory;

    @Mock
    private MessageProvider message;

    @Mock
    private ServiceRepository serviceRepository;

    @InjectMocks
    private ServiceServiceImpl serviceService;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
        service = new ServiceFactory().newInstance();
        serviceList = new ArrayList<>();
        serviceList.add(service);
    }

    @Test
    void whenCreate_thenServicePersisted() {
        lenient().when(serviceRepository.save(any(Services.class))).thenReturn(service);
        Services response = serviceService.create(service);
        assertThat(response).isNotNull();
    }

    @Test
    void whenGetAll_thenReturnServiceList() {
        lenient().when(serviceRepository.findAll()).thenReturn(serviceList);
        List<Services> responseList = serviceService.getAllService();
        assertThat(responseList).isNotEmpty();
    }

    @Test
    void whenDelete_thenServiceDeleted() {
        Long id = 200L;
        lenient().when(serviceRepository.existsById(anyLong())).thenReturn(true);
        serviceService.delete(id);
        verify(serviceRepository).deleteById(id);
    }

    @Test
    void whenUpdate_thenServiceUpdated() {
        lenient().when(serviceRepository.existsById(anyLong())).thenReturn(true);
        lenient().when(serviceRepository.save(any(Services.class))).thenReturn(service);
        Services response = serviceService.update(service);
        assertThat(response).isNotNull();
    }

    @Test
    void update_thenThrows() {
        lenient().when(message.get(any(MessageEnum.class))).thenReturn(dataFactory.getName());
        lenient().when(serviceRepository.existsById(anyLong())).thenReturn(false);
        lenient().when(serviceRepository.save(any(Services.class))).thenReturn(service);

        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            serviceService.update(service);
        });
    }

    @Test
    void whenDelete_thenThrows() {
        lenient().when(message.get(any(MessageEnum.class))).thenReturn(dataFactory.getName());
        lenient().when(serviceRepository.existsById(anyLong())).thenReturn(false);

        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            serviceService.delete(1L);
        });
    }

    @Test
    void whenGetById_ThenRetrieveService() {
        lenient().when(serviceRepository.findById(anyLong())).thenReturn(Optional.of(service));
        Services response = serviceService.getServiceById(1L);
        assertThat(response).isNotNull();
    }

    @Test
    void whenGetById_thenThrows() {
        lenient().when(message.get(any(MessageEnum.class))).thenReturn(dataFactory.getName());
        lenient().when(serviceRepository.findById(anyLong())).thenReturn(Optional.empty());
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            serviceService.getServiceById(1L);
        });
    }
}