package com.org.tech.service.impl;

import com.org.tech.model.ChecklistRepair;
import com.org.tech.model.keys.ChecklistRepairKey;
import com.org.tech.repository.ChecklistRepairRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class ChecklistRepairServiceImplTest {

    private List<ChecklistRepair> items;

    @Mock
    private ChecklistRepairRepository mockChecklistRepairRepository;

    @InjectMocks
    private ChecklistRepairServiceImpl mockChecklistRepairService;

    @BeforeEach
    void setUp() {
        ChecklistRepair checklistRepair = new ChecklistRepair();
        ChecklistRepairKey key = new ChecklistRepairKey();
        key.setWorkServiceId(1001L);
        key.setRepairRouteId(1001L);
        checklistRepair.setId(key);

        items = Collections.singletonList(checklistRepair);
    }

    @Test
    void whenSaveChecklist_thenPersisted() {
        given(mockChecklistRepairRepository.saveAll(any(List.class))).willReturn(items);
        var response = mockChecklistRepairService.saveAllChecklistRepair(items);
        assertThat(response).isNotEmpty();
        assertThat(items).isEqualTo(response);
    }
}