package com.org.tech.service.impl;

import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.factory.AdmissionDataSheetFactory;
import com.org.tech.factory.UserFactory;
import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.Employee;
import com.org.tech.repository.AdmissionDataSheetRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class AssignJobsServiceImplTest {

    AdmissionDataSheet admission;
    Employee employee;
    @Mock
    private AdmissionDataSheetRepository admissionDataSheetRepository;

    @Mock
    private EmployeeServiceImpl employeeService;

    @Mock
    private BlacklistServiceImpl blacklistService;

    @InjectMocks
    private AssignJobsServiceImpl assignJobsService;

    @BeforeEach
    void setUp() {
        admission = new AdmissionDataSheetFactory(true).newInstance();
        employee = new UserFactory().newInstanceEmployee();
    }

    @Test
    void assignJobs_thenOk() {
        lenient().when(admissionDataSheetRepository.findById(anyLong())).thenReturn(Optional.of(admission));
        lenient().when(blacklistService.getDiscardedEmployeeByAdmission(anyLong())).thenReturn(Collections.emptyList());
        lenient().when(employeeService.getEmployeeLastUpdate(any(List.class))).thenReturn(employee);
        lenient().when(admissionDataSheetRepository.save(any(AdmissionDataSheet.class))).thenReturn(admission);
        lenient().when(employeeService.update(any(Employee.class))).thenReturn(employee);
        try {
            assignJobsService.assignJobs(1002);
        }catch (Exception e){
            Assertions.fail();
        }
    }

    @Test
    void assignJobs_thenException() {
        lenient().when(admissionDataSheetRepository.findById(anyLong())).thenReturn(Optional.empty());

        Assertions.assertThrows(ResourceNotFoundException.class,
                () -> assignJobsService.assignJobs(1002));

    }
}