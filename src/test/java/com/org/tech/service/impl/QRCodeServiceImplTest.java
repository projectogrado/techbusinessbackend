package com.org.tech.service.impl;

import com.google.zxing.WriterException;
import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.QRCodeException;
import com.org.tech.providers.MessageProvider;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class QRCodeServiceImplTest {

    @InjectMocks
    QRCodeServiceImpl qrService;
    @Mock
    private MessageProvider message;
    private DataFactory dataFactory;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
    }

    @Test
    void whenCreate_thenReturnQRCode() throws IOException, WriterException {
        String qr = qrService.createQRCode("vacio", 2, 4);
        assertThat(qr).isNotNull();
    }

    @Test
    void whenCreate_thenThrow() {
        lenient().when(message.get(any(MessageEnum.class))).thenReturn(dataFactory.getName());

        Assertions.assertThrows(QRCodeException.class, () -> {
            qrService.createQRCode("", 2, 4);
        });
    }
}