package com.org.tech.factory;


import com.org.tech.model.Role;

public class RoleFactory {

    private Long id;
    private String name;
    private String description;

    public RoleFactory() {
        this.id = 1L;
        this.name = "ADMIN";
        this.description = "Admin_Role";
    }

    public Role RoleEmployeeFactory() {
        this.name = "EMPLOYEE";
        this.description = "Employee_Role";
        return newInstance();
    }


    public Role RoleFactoryDefault() {
        this.name = "USER";
        this.description = "User_Role";
        return newInstance();
    }

    public Role newInstance() {
        return Role.builder()
                .id(this.id)
                .name(this.name)
                .description(this.description)
                .build();
    }


    public String getName() {
        return name;
    }

}
