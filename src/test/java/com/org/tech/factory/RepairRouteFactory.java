package com.org.tech.factory;

import com.org.tech.model.Process;
import com.org.tech.model.RepairRoute;
import com.org.tech.model.Services;


public class RepairRouteFactory {

    private final Long id;
    private final Process process;
    private final Services services;

    public RepairRouteFactory() {
        this.id = 1L;
        this.process = new ProcessFactory().newInstance();
        this.services = new ServiceFactory().newInstance();
    }

    public RepairRoute newInstance() {
        return RepairRoute.builder()
                .id(this.id)
                .process(this.process)
                .services(this.services)
                .build();
    }


}
