package com.org.tech.factory;

import com.org.tech.model.Process;

public class ProcessFactory {

    private Long id;
    private String name;
    private String description;
    private Boolean status;

    public ProcessFactory() {
        this.id = 1L;
        this.name = "Reparación";
        this.description = "Mantenimiento preventivo";
        this.status = true;
    }

    public Process newInstance() {
        return Process.builder()
                .id(this.id)
                .name(this.description)
                .description(this.description)
                .status(this.status)
                .build();
    }

    public ProcessFactory setName(String name) {
        this.name = name;
        return this;
    }

    public ProcessFactory setId(Long id) {
        this.id = id;
        return this;
    }
}
