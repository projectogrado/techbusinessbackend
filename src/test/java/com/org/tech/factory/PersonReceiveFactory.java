package com.org.tech.factory;

import com.org.tech.model.DischargeDataSheet;
import com.org.tech.model.PersonReceive;
import com.org.tech.model.Role;
import com.org.tech.model.enums.TypeDocument;

import java.util.HashSet;
import java.util.Set;

public class PersonReceiveFactory {

    private final Long id;
    private final String name;
    private final String lastname;
    private final TypeDocument typeDocument;
    private final String document;
    private final String phone;
    private final String email;
    private final String address;
    private final String kinship;
    private final DischargeDataSheet dischargeDataSheet;

    public PersonReceiveFactory() {
        this.id = 1L;
        this.name = "Paula";
        this.lastname = "Santamaria";
        this.typeDocument = TypeDocument.CC;
        this.document = "1101760623";
        this.phone = "3224681936";
        this.email = "pausla076@gmail.com";
        this.address = "Doctrina Naranjos";
        this.kinship = "Hermana";
        this.dischargeDataSheet = new DischargeDataSheetFactory().newInstance();
    }

    public PersonReceive newInstance() {
        Set<Role> role = new HashSet<>();

        PersonReceive personReceive = new PersonReceive();
        personReceive.setId(this.id);
        personReceive.setName(this.name);
        personReceive.setLastname(this.lastname);
        personReceive.setDocument(this.document);
        personReceive.setTypeDocument(this.typeDocument);
        personReceive.setPhone(this.phone);
        personReceive.setEmail(this.email);
        personReceive.setAddress(this.address);
        personReceive.setKinship(this.kinship);
        personReceive.setDischargeDataSheet(this.dischargeDataSheet);
        personReceive.setRoles(role);

        return personReceive;
    }
}
