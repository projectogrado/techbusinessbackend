package com.org.tech.factory;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.org.tech.model.Administrator;
import com.org.tech.model.Customer;
import com.org.tech.model.Employee;
import com.org.tech.model.Equipment;
import com.org.tech.model.Role;
import com.org.tech.model.dto.LoginDto;
import com.org.tech.model.enums.TypeDocument;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class UserFactory {

    private final String name;
    private final String lastname;
    private final String address;
    private final String email;
    private final String phone;
    private final boolean status;
    private final String document;
    private final TypeDocument typeDocument;
    ObjectMapper objectMapper = new ObjectMapper();
    private Long id;
    private String username;
    private String password;


    public UserFactory() {
        this.id = 1L;
        this.username = "jsrincon";
        this.password = "81943301Se/";
        this.name = "ingrith";
        this.lastname = "santamaria";
        this.email = "sebas199765@gmail.com";
        this.document = "1144201661";
        this.typeDocument = TypeDocument.CC;
        this.phone = "3123196836";
        this.address = "carrera 11 D 45 31";
        this.status = true;
        Set<Equipment> equipment = new HashSet<>();
        equipment.add(new EquipmentFactory().newInstance());

    }

    public LoginDto newInstanceLogin() {
        return LoginDto.builder()
                .username(this.username)
                .password(this.password)
                .build();
    }

    public String employeeJson(Employee employee) {
        try {
            return objectMapper.writeValueAsString(employee);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String customerJson(Customer customer) {
        try {
            return objectMapper.writeValueAsString(customer);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String administratorJson(Administrator administrator) {
        try {
            return objectMapper.writeValueAsString(administrator);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String loginJson(LoginDto loginDto) {
        try {
            return objectMapper.writeValueAsString(loginDto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Employee newInstanceEmployee() {
        Set<Role> role = new HashSet<>();
        role.add(new RoleFactory().RoleEmployeeFactory());
        role.add(new RoleFactory().RoleFactoryDefault());

        Employee employee = new Employee();
        employee.setId(this.id);
        employee.setName(this.name);
        employee.setLastname(this.lastname);
        employee.setPhone(this.phone);
        employee.setEmail(this.email);
        employee.setAddress(this.address);
        employee.setUsername(this.username);
        employee.setPassword(this.password);
        employee.setStatus(this.status);
        employee.setDocument(this.document);
        employee.setTypeDocument(this.typeDocument);
        employee.setRoles(role);
        employee.setTokenNotification("eBt7gGkyQuqG6vggqyQotp:APA91bG0dzk1sBILJuIsZw_8ZUneRnii7xdN8uTgqaHf0t5wdZEwShZ0aW44nLuDpFp7TZOIqac9WGGbfaSn250BYdEplQfws_iOPldqd0a_zzchJ44f6Xsli_kxdxIiOeEwNF2u1OYY");
        employee.setWorks(new ArrayList<>(Collections.emptyList()));

        return employee;
    }

    public Administrator newInstanceAdministrator() {
        Set<Role> role = new HashSet<>();
        role.add(new RoleFactory().newInstance());
        role.add(new RoleFactory().RoleFactoryDefault());

        Administrator administrator = new Administrator();
        administrator.setId(this.id);
        administrator.setName(this.name);
        administrator.setLastname(this.lastname);
        administrator.setPhone(this.phone);
        administrator.setEmail(this.email);
        administrator.setAddress(this.address);
        administrator.setUsername(this.username);
        administrator.setPassword(this.password);
        administrator.setDocument(this.document);
        administrator.setTypeDocument(this.typeDocument);
        administrator.setRoles(role);

        return administrator;
    }

    public Customer newInstanceCustomer() {
        Set<Role> role = new HashSet<>();
        role.add(new RoleFactory().RoleFactoryDefault());

        Customer customer = new Customer();
        customer.setId(this.id);
        customer.setName(this.name);
        customer.setLastname(this.lastname);
        customer.setPhone(this.phone);
        customer.setEmail(this.email);
        customer.setAddress(this.address);
        customer.setDocument(this.document);
        customer.setTypeDocument(this.typeDocument);
        customer.setRoles(role);

        return customer;
    }

    public UserFactory setUsername(String username) {
        this.username = username;
        return this;
    }

    public UserFactory setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserFactory setId(Long id) {
        this.id = id;
        return this;
    }
}
