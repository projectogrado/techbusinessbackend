package com.org.tech.factory;

import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.DischargeDataSheet;

import java.util.Date;

public class DischargeDataSheetFactory {

    private final Long id;
    private final Date deliverDate;
    private final AdmissionDataSheet admissionDataSheet;

    public DischargeDataSheetFactory() {
        this.id = 1L;
        this.deliverDate = new Date();
        this.admissionDataSheet = new AdmissionDataSheetFactory(true).newInstance();
    }

    public DischargeDataSheet newInstance() {
        DischargeDataSheet dischargeInstance = new DischargeDataSheet();
        dischargeInstance.setId(this.id);
        dischargeInstance.setDeliverDate(this.deliverDate);
        dischargeInstance.setAdmissionDataSheet(this.admissionDataSheet);

        return dischargeInstance;
    }
}
