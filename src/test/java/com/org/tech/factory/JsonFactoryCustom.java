package com.org.tech.factory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.DischargeDataSheet;
import com.org.tech.model.Parameters;
import com.org.tech.model.Process;
import com.org.tech.model.dto.RecoverPasswordDto;

public class JsonFactoryCustom {
    ObjectMapper objectMapper = new ObjectMapper();

    public String toJson(AdmissionDataSheet object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String toJson(DischargeDataSheet object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String toJson(RecoverPasswordDto object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String toJson(Process object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String toJson(Parameters object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
