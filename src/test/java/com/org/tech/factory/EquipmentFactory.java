package com.org.tech.factory;

import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.Equipment;
import com.org.tech.model.enums.StatusEquipment;

import java.time.LocalDateTime;

public class EquipmentFactory {

    private final Long id;
    private final String mark;
    private final String model;
    private final String ram;
    private final String hardDisk;
    private final String processor;
    private final String board;
    private final String type;
    private StatusEquipment status;
    private LocalDateTime date;
    private final AdmissionDataSheet admissionDataSheet;

    public EquipmentFactory() {
        this.id = 1L;
        this.mark = "ASUS";
        this.ram = "2 GB SANDISK";
        this.model = "GP98T";
        this.hardDisk = "500GB Samsung";
        this.processor = "Core i3 3200";
        this.board = "Intel board";
        this.type = "portatil";
        this.status = StatusEquipment.IN_REPAIR;
        this.admissionDataSheet = new AdmissionDataSheetFactory(false).newInstanceWithOutEquipment();
        this.date = LocalDateTime.now().minusHours(5);
    }

    public Equipment newInstance() {
        Equipment equipment = new Equipment();
        equipment.setId(this.id);
        equipment.setMark(this.mark);
        equipment.setModel(this.model);
        equipment.setRam(this.ram);
        equipment.setHardDisk(this.hardDisk);
        equipment.setProcessor(this.processor);
        equipment.setBoard(this.board);
        equipment.setType(this.type);
        equipment.setStatus(this.status);
        equipment.setAdmissionDataSheet(this.admissionDataSheet);

        return equipment;
    }
    public Equipment newInstanceWithOutAdmission() {
        Equipment equipment = new Equipment();
        equipment.setId(this.id);
        equipment.setMark(this.mark);
        equipment.setModel(this.model);
        equipment.setRam(this.ram);
        equipment.setHardDisk(this.hardDisk);
        equipment.setProcessor(this.processor);
        equipment.setBoard(this.board);
        equipment.setType(this.type);
        equipment.setStatus(this.status);

        return equipment;
    }

    public Equipment newInstanceWithDate() {
        Equipment equipment = new Equipment();
        equipment.setId(this.id);
        equipment.setMark(this.mark);
        equipment.setModel(this.model);
        equipment.setRam(this.ram);
        equipment.setHardDisk(this.hardDisk);
        equipment.setProcessor(this.processor);
        equipment.setBoard(this.board);
        equipment.setType(this.type);
        equipment.setStatus(this.status);
        equipment.setAdmissionDataSheet(this.admissionDataSheet);
        equipment.setDateUpdatedStatus(date);
        return equipment;
    }

    public EquipmentFactory setStatus(StatusEquipment status) {
        this.status = status;
        return this;
    }

    public EquipmentFactory setDate(LocalDateTime date) {
        this.date = date;
        return this;
    }
}
