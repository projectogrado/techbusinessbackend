package com.org.tech.factory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.org.tech.model.Services;

import java.util.Collections;

public class ServiceFactory {

    ObjectMapper objectMapper = new ObjectMapper();
    private Long id;
    private String name;
    private String description;
    private Boolean status;


    public ServiceFactory() {
        this.id = 1L;
        this.name = "Reparación";
        this.description = "Mantenimiento preventivo";
        this.status = true;
    }

    public Services newInstance() {
        return Services.builder()
                .id(this.id)
                .name(this.description)
                .description(this.description)
                .status(this.status)
                .repairRoute(Collections.emptyList())
                .build();
    }

    public String toJson(Services services) {
        try {
            return objectMapper.writeValueAsString(services);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ServiceFactory setId(Long id) {
        this.id = id;
        return this;
    }

}
