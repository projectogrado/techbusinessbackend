package com.org.tech.factory;

import com.org.tech.model.Parameters;

import java.time.LocalDateTime;

public class ParametersFactory {

    private final Long id;
    private final Integer timeToDo;
    private final Integer timeInRepair;
    private final Integer timeNovelty;
    private final Integer timeNoRepaired;
    private final Integer timeRepaired;
    private final LocalDateTime dateCreated;

    public ParametersFactory() {
        this.id = 1L;
        this.timeToDo = 2;
        this.timeInRepair = 2;
        this.timeNovelty = 2;
        this.timeNoRepaired = 2;
        this.timeRepaired = 2;
        this.dateCreated = LocalDateTime.now();
    }

    public Parameters newInstance() {
        return Parameters.builder()
                .id(this.id)
                .timeToDo(this.timeToDo)
                .timeInRepair(this.timeInRepair)
                .timeNovelty(this.timeNovelty)
                .timeNoRepaired(this.timeNoRepaired)
                .timeRepaired(this.timeRepaired)
                .cronTask("0 */1 * * * *")
                .dateCreated(LocalDateTime.now())
                .build();
    }


}
