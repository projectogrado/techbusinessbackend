package com.org.tech.factory;

import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.Customer;
import com.org.tech.model.Equipment;
import com.org.tech.model.WorkService;
import com.org.tech.model.enums.StatusEquipment;
import com.org.tech.model.enums.TypeDocument;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AdmissionDataSheetFactory {

    private final Long id;
    private final Date date;
    private final String reason;
    private final String observation;
    private final List<WorkService> workServices;
    private final Equipment equipment;
    private final Customer customer;

    public AdmissionDataSheetFactory(boolean equipment) {
        this.id = 1L;
        this.date = new Date();
        this.reason = "No funciona el teclado";
        this.observation = "Se debe cambiar el teclado";
        this.workServices = new ArrayList<>();
        this.equipment = equipment ? new EquipmentFactory().setStatus(StatusEquipment.TO_DO).newInstanceWithOutAdmission() : null;
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setName("ingrith");
        customer.setLastname("santamaria");
        customer.setPhone("3123196836");
        customer.setEmail("sebas199765@gmail.com");
        customer.setAddress("carrera 11 D 45 31");
        customer.setDocument("1144201661");
        customer.setTypeDocument(TypeDocument.CC);
        this.customer = customer;

    }

    public AdmissionDataSheet newInstance() {
        AdmissionDataSheet admissionInstance = new AdmissionDataSheet();
        admissionInstance.setId(this.id);
        admissionInstance.setDate(this.date);
        admissionInstance.setReason(this.reason);
        admissionInstance.setObservation(this.observation);
        admissionInstance.setWorkServices(this.workServices);
        admissionInstance.setEquipment(this.equipment);
        admissionInstance.setCustomer(customer);

        return admissionInstance;
    }

    public AdmissionDataSheet newInstanceWithOutEquipment() {
        AdmissionDataSheet admissionInstance = new AdmissionDataSheet();
        admissionInstance.setId(this.id);
        admissionInstance.setDate(this.date);
        admissionInstance.setReason(this.reason);
        admissionInstance.setObservation(this.observation);
        admissionInstance.setWorkServices(this.workServices);
        admissionInstance.setCustomer(customer);
        return admissionInstance;
    }
}
