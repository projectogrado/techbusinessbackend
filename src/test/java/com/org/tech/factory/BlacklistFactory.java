package com.org.tech.factory;

import com.org.tech.model.Blacklist;
import com.org.tech.model.keys.BlacklistKey;

public class BlacklistFactory {

    private final BlacklistKey id;
    private final String reason;

    public BlacklistFactory() {
        this.id = new BlacklistKey();
        this.id.setAdmissionId(1l);
        this.id.setEmployeeId(1l);
        this.reason = "Reason";
    }

    public Blacklist newInstance() {
        return Blacklist.builder().id(this.id).reason(this.reason).build();
    }
}
