package com.org.tech.utils;

import com.org.tech.exception.MessageException;
import com.org.tech.model.dto.MailBodyDto;
import com.org.tech.providers.MessageProvider;
import freemarker.template.Template;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.mail.internet.MimeMessage;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith({MockitoExtension.class})
class MailSenderUtilTest {

    private MailBodyDto mailBodyDto;

    @Mock
    private MimeMessage mimeMessage;

    @Mock
    private MessageProvider message;

    @Mock
    private JavaMailSender mailSender;

    @Spy
    private FreeMarkerConfigurer freeMarkerConfigurer;

    @Mock
    private FreeMarkerTemplateUtils freeMarkerTemplateUtils;

    @Mock
    private Template template;

    @InjectMocks
    private MailSenderUtil mailSenderUtil;

    @BeforeEach
    void setUp() {
        mailBodyDto = MailBodyDto.builder()
                .email("sebas199765@gmail.com")
                .subject("subject")
                .content("content")
                .build();
    }

    @Test
    void whenSendMail_thenThrow() {
        given(mailSender.createMimeMessage()).willReturn(mimeMessage);

        assertThrows(MessageException.class, () -> {
            mailSenderUtil.sendSimpleMail(mailBodyDto);
        });
    }
}