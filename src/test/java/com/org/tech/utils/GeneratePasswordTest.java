package com.org.tech.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class GeneratePasswordTest {

    @InjectMocks
    private GeneratePassword generatePassword;

    @BeforeEach
    void setUp() {
    }

    @Test
    void generatePassword() {
        String password = generatePassword.generatePassword();
        assertThat(password).isNotNull();
    }
}