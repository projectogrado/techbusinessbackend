package com.org.tech.controller;

import com.org.tech.factory.DischargeDataSheetFactory;
import com.org.tech.model.DischargeDataSheet;
import com.org.tech.service.DischargeDataSheetService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class DischargeDataSheetControllerTest {

    private DischargeDataSheet discharge;
    @Autowired
    private MockMvc mvc;

    @MockBean
    private DischargeDataSheetService mockDischargeDataSheetService;

    @BeforeEach
    void setUp() {
        discharge = new DischargeDataSheetFactory().newInstance();
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenGetAllDischargeDataSheet_thenReturnDischarge() throws Exception {
        discharge.setId(null);
        when(mockDischargeDataSheetService.getDischargeDataSheetById(anyLong())).thenReturn(discharge);

        String URL_DISCHARGE = "/discharge/1";
        mvc.perform(get(URL_DISCHARGE)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(discharge.getId())));
    }
}