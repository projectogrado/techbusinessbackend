package com.org.tech.controller.request;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class EmployeeControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @WithMockUser(username = "jfrincon", roles = {"EMPLOYEE"})
    void whenSendNotification_thenSendNotification() throws Exception {
        String URL_EMPLOYEE_NOTIFICATION = "/employee/send-notification/?employee=1006";
        mvc.perform(get(URL_EMPLOYEE_NOTIFICATION)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "jfrincon", roles = {"EMPLOYEE"})
    void whenSendNotificationEmployeeNotFound_thenError() throws Exception {
        String URL_EMPLOYEE_NOTIFICATION = "/employee/send-notification/?employee=1009";
        mvc.perform(get(URL_EMPLOYEE_NOTIFICATION)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
