package com.org.tech.controller.request;

import com.org.tech.factory.JsonFactoryCustom;
import com.org.tech.factory.ProcessFactory;
import com.org.tech.model.Process;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class ProcessControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @WithMockUser(username = "jfrincon", roles = {"EMPLOYEE"})
    void whenCreate_Process() throws Exception {
        String process = new JsonFactoryCustom().toJson(new ProcessFactory().setName("asd").newInstance());
        String URL_PROCESS_INSERT = "/process/insert-process";
        mvc.perform(post(URL_PROCESS_INSERT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(process)
        ).andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "jfrincon", roles = {"EMPLOYEE"})
    void whenUpdate_Process() throws Exception {
        Process object =Process.builder().id(1001L).name("asd").description("asdad").build();
        String process = new JsonFactoryCustom().toJson(object);
        String URL_PROCESS_UPDATE = "/process/update-process";
        mvc.perform(put(URL_PROCESS_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(process)
        ).andExpect(status().isBadRequest());
    }

}
