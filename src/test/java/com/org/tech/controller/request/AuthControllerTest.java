package com.org.tech.controller.request;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.org.tech.factory.UserFactory;
import com.org.tech.model.dto.LoginDto;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class AuthControllerTest {

    DataFactory dataFactory = new DataFactory();

    @LocalServerPort
    private int port;

    private String token;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void LoginSuccess() throws URISyntaxException {
        final String baseUrl = "http://127.0.0.1:" + port + "/api/tech_business/auth/login";
        URI uri = new URI(baseUrl);

        LoginDto loginDto = new UserFactory().newInstanceLogin();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        HttpEntity<LoginDto> request = new HttpEntity<>(loginDto, headers);
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
        String json = result.getBody();
        JsonObject jsonObject = JsonParser.parseString(Objects.requireNonNull(json))
                .getAsJsonObject();
        token = jsonObject.get("token").getAsString();
        Assertions.assertNotNull(result.getBody());
    }

    @Test
    void LoginFail() throws URISyntaxException {
        final String baseUrl = "http://127.0.0.1:" + port + "/api/tech_business/auth/login";
        URI uri = new URI(baseUrl);

        LoginDto login = new UserFactory().newInstanceLogin();
        login.setUsername(dataFactory.getName());

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        HttpEntity<LoginDto> request = new HttpEntity<>(login, headers);
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);

        Assertions.assertNotNull(result.getBody());
    }


    @Test
    void Home() throws URISyntaxException {
        String baseUrl = "http://127.0.0.1:" + port + "/api/tech_business/auth/login";
        URI uri = new URI(baseUrl);

        LoginDto loginDto = new UserFactory().newInstanceLogin();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        HttpEntity<LoginDto> request = new HttpEntity<>(loginDto, headers);
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
        String json = result.getBody();
        JsonObject jsonObject = JsonParser.parseString(Objects.requireNonNull(json))
                .getAsJsonObject();
        token = jsonObject.get("token").getAsString();

        baseUrl = "http://localhost:" + port + "/api/tech_business/home/index";
        uri = new URI(baseUrl);
        headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Authorization", "Bearer " + token);

        request = new HttpEntity<>(null, headers);

        result = this.restTemplate.postForEntity(uri, request, String.class);
        Assertions.assertNotNull(result.getBody());
    }

    @Test
    void HomeWrong() throws URISyntaxException {
        String baseUrl = "http://127.0.0.1:" + port + "/api/tech_business/auth/login";
        URI uri = new URI(baseUrl);

        LoginDto loginDto = new UserFactory().newInstanceLogin();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        HttpEntity<LoginDto> request = new HttpEntity<>(loginDto, headers);
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
        String json = result.getBody();
        JsonObject jsonObject = JsonParser.parseString(Objects.requireNonNull(json))
                .getAsJsonObject();
        token = jsonObject.get("token").getAsString();

        baseUrl = "http://localhost:" + port + "/api/tech_business/home/index";
        uri = new URI(baseUrl);
        headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Authorization", "Bearer " + token + "adas");

        request = new HttpEntity<>(null, headers);

        result = this.restTemplate.postForEntity(uri, request, String.class);
        Assertions.assertNull(result.getBody());
    }

    @Test
    void PasswordReset() throws URISyntaxException {
        final String baseUrl = "http://127.0.0.1:" + port + "/api/tech_business/password-reset/recover-password/?username=otheruser";
        URI uri = new URI(baseUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, null, String.class);
        String json = result.getBody();
        Assertions.assertNotNull(result.getBody());
    }
}