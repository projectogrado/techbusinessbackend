package com.org.tech.controller.request;

import com.org.tech.service.AdmissionDataSheetService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.file.Paths;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class AdmissionControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private AdmissionDataSheetService admissionDataSheetService;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(admissionDataSheetService, "dir", Paths.get("")
                .toAbsolutePath()
                .toString()+"/file");

    }

    @Test
    @WithMockUser(username = "jfrincon", roles = {"EMPLOYEE"})
    void whenDownloadPdf_thenOk() throws Exception {
        String URL_ADMISSION_PDF = "/admission/generate-pdf/1001";
        mvc.perform(get(URL_ADMISSION_PDF)
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "jfrincon", roles = {"EMPLOYEE"})
    void whenSendEmailPdf_thenOk() throws Exception {
        String URL_ADMISSION_EMAIL = "/admission/send-pdf/1001";
        mvc.perform(get(URL_ADMISSION_EMAIL)
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

}
