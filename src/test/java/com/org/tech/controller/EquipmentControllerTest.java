package com.org.tech.controller;

import com.org.tech.factory.EquipmentFactory;
import com.org.tech.model.Equipment;
import com.org.tech.service.impl.EquipmentServiceImpl;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class EquipmentControllerTest {

    DataFactory dataFactory;
    private Equipment sampleEquipment;
    private List<Equipment> equipmentList;
    private Map<String, String> params;

    @Mock
    private EquipmentServiceImpl equipmentService;

    @InjectMocks
    private EquipmentController equipmentController;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
        sampleEquipment = new EquipmentFactory().newInstance();
        params = new HashMap<>();
        equipmentList = new ArrayList<>();
        equipmentList.add(sampleEquipment);
    }

    @Test
    void whenSave_thenPersisted() {
        lenient().when(equipmentService.create(any(Equipment.class))).thenReturn(sampleEquipment);
        ResponseEntity<Equipment> equipment = equipmentController.create(sampleEquipment);

        Equipment persistedEquipment = equipment.getBody();
        assertThat(persistedEquipment.getBoard())
                .isEqualTo(sampleEquipment.getBoard());

        assertThat(persistedEquipment.getMark())
                .isEqualTo(sampleEquipment.getMark());

        assertThat(persistedEquipment.getModel())
                .isEqualTo(sampleEquipment.getModel());

    }

    @Test
    void whenGetAll_thenReturnEquiments() {
        lenient().when(equipmentService.getAllEquipment(any(Map.class))).thenReturn(equipmentList);
        List<Equipment> list = equipmentController.getAll(params).getBody();
        assertThat(list.size() > 0).isTrue();
    }

    @Test
    void whenGetAllFilter_thenReturnEquipments() {
        lenient().when(equipmentService.getAllEquipment(any(Map.class))).thenReturn(equipmentList);
        List<Equipment> list = equipmentController.getAll(params).getBody();
        assertThat(list.size() > 0).isTrue();
    }


    @Test
    void whenUpdate_thenPersisted() {
        lenient().when(equipmentService.update(any(Equipment.class))).thenReturn(sampleEquipment);
        ResponseEntity<Equipment> equipment = equipmentController.update(sampleEquipment);

        Equipment persistedEquipment = equipment.getBody();
        assertThat(persistedEquipment.getBoard())
                .isEqualTo(sampleEquipment.getBoard());

        assertThat(persistedEquipment.getMark())
                .isEqualTo(sampleEquipment.getMark());

        assertThat(persistedEquipment.getModel())
                .isEqualTo(sampleEquipment.getModel());
    }

}