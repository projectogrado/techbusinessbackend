package com.org.tech.controller;

import com.org.tech.factory.ProcessFactory;
import com.org.tech.model.Process;
import com.org.tech.service.impl.ProcessServiceImpl;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class ProcessControllerTest {

    DataFactory dataFactory;
    private Process sampleProcess;
    private List<Process> processList;

    @Mock
    private ProcessServiceImpl processService;

    @InjectMocks
    private ProcessController processController;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
        sampleProcess = new ProcessFactory().newInstance();
        processList = new ArrayList<>();
        processList.add(sampleProcess);
    }

    @Test
    void whenSave_thenPersisted() {
        lenient().when(processService.create(any(Process.class))).thenReturn(sampleProcess);
        ResponseEntity<Process> process = processController.create(sampleProcess);

        Process persistedProcess = process.getBody();
        assertThat(persistedProcess.getName())
                .isEqualTo(sampleProcess.getName());

        assertThat(persistedProcess.getDescription())
                .isEqualTo(sampleProcess.getDescription());

        assertThat(persistedProcess.getStatus())
                .isEqualTo(sampleProcess.getStatus());
    }

    @Test
    void whenGetAll_thenReturnProcess() {
        lenient().when(processService.getAllProcess()).thenReturn(processList);
        List<Process> list = processController.getAllProcess().getBody();
        assertThat(list.size() > 0).isTrue();
    }

    @Test
    void whenUpdate_thenPersisted() {
        lenient().when(processService.update(any(Process.class))).thenReturn(sampleProcess);
        ResponseEntity<Process> process = processController.update(sampleProcess);

        Process persistedProcess = process.getBody();
        assertThat(persistedProcess.getName())
                .isEqualTo(sampleProcess.getName());

        assertThat(persistedProcess.getDescription())
                .isEqualTo(sampleProcess.getDescription());

        assertThat(persistedProcess.getStatus())
                .isEqualTo(sampleProcess.getStatus());
    }
}