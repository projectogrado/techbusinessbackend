package com.org.tech.controller;

import com.org.tech.factory.PersonReceiveFactory;
import com.org.tech.model.PersonReceive;
import com.org.tech.service.impl.PersonReceiveServiceImpl;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class PersonReceiveControllerTest {

    private PersonReceive personReceive;
    private DataFactory dataFactory;

    @Mock
    private PersonReceiveServiceImpl deliveryService;

    @InjectMocks
    private PersonReceiveController personReceiveController;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
        personReceive = new PersonReceiveFactory().newInstance();
    }

    @Test
    void whenSave_thenPersisted() {
        lenient().when(deliveryService.create(any(PersonReceive.class))).thenReturn(personReceive);
        ResponseEntity<PersonReceive> delivery = personReceiveController.create(personReceive);

        PersonReceive persistedDelivery = delivery.getBody();
        assertThat(persistedDelivery.getName())
                .isEqualTo(personReceive.getName());

        assertThat(persistedDelivery.getLastname())
                .isEqualTo(personReceive.getLastname());

        assertThat(persistedDelivery.getTypeDocument())
                .isEqualTo(personReceive.getTypeDocument());

        assertThat(persistedDelivery.getDocument())
                .isEqualTo(personReceive.getDocument());

        assertThat(persistedDelivery.getPhone())
                .isEqualTo(personReceive.getPhone());

        assertThat(persistedDelivery.getEmail())
                .isEqualTo(personReceive.getEmail());

        assertThat(persistedDelivery.getAddress())
                .isEqualTo(personReceive.getAddress());

        assertThat(persistedDelivery.getKinship())
                .isEqualTo(personReceive.getKinship());

        assertThat(persistedDelivery.getDischargeDataSheet())
                .isEqualTo(personReceive.getDischargeDataSheet());
    }
}