package com.org.tech.controller;

import com.org.tech.factory.UserFactory;
import com.org.tech.model.Employee;
import com.org.tech.service.impl.EmployeeServiceImpl;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class EmployeeControllerTest {

    private final String URL_EMPLOYEE_ME = "/employee/me";
    private final String URL_EMPLOYEE_UPDATE = "/employee/update-employee";
    private final String URL_EMPLOYEE_INSERT = "/employee/insert-employee";
    private final String URL_EMPLOYEE_ALL = "/employee/all";

    private Employee employeeSample;
    private Employee employeeCreate;
    private Employee employeeErrorCreate;
    private List<Employee> employees;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private EmployeeServiceImpl mocEmployeeService;


    @BeforeEach
    public void setup() {
        DataFactory dataFactory = new DataFactory();
        employeeSample = new UserFactory().newInstanceEmployee();
        employeeCreate = new UserFactory().setId(null).newInstanceEmployee();
        employeeErrorCreate = new UserFactory()
                .setId(null)
                .setPassword(dataFactory.getRandomWord())
                .newInstanceEmployee();

        employees = new ArrayList<>();
        employees.add(employeeSample);
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenCreateEmployee_thenReturnEmployee() throws Exception {
        when(mocEmployeeService.create(any(Employee.class))).thenReturn(employeeSample);
        mvc.perform(post(URL_EMPLOYEE_INSERT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new UserFactory().employeeJson(employeeCreate)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(employeeSample.getName())))
                .andExpect(jsonPath("$.username", is(employeeSample.getUsername())))
                .andExpect(jsonPath("$.password", is(employeeSample.getPassword())));
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenCreateEmployee_thenErrorMessage() throws Exception {
        when(mocEmployeeService.create(any(Employee.class))).thenReturn(employeeSample);
        mvc.perform(post(URL_EMPLOYEE_INSERT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new UserFactory().employeeJson(employeeErrorCreate)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenUpdateEmployee_thenReturnEmployee() throws Exception {
        when(mocEmployeeService.update(any(Employee.class))).thenReturn(employeeSample);
        mvc.perform(put(URL_EMPLOYEE_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new UserFactory().employeeJson(employeeSample)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(employeeSample.getName())))
                .andExpect(jsonPath("$.username", is(employeeSample.getUsername())))
                .andExpect(jsonPath("$.password", is(employeeSample.getPassword())));
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenGetEmployee_thenReturnEmployees() throws Exception {
        when(mocEmployeeService.getAllEmployees(anyMap())).thenReturn(employees);
        mvc.perform(get(URL_EMPLOYEE_ALL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(employeeSample.getName())))
                .andExpect(jsonPath("$[0].username", is(employeeSample.getUsername())))
                .andExpect(jsonPath("$[0].password", is(employeeSample.getPassword())));
    }

    @Test
    @WithMockUser(username = "jfrincon", roles = {"EMPLOYEE"})
    void whenGetEmployeeForContext_thenReturnEmployees() throws Exception {
        when(mocEmployeeService.getEmployeeContext()).thenReturn(employeeSample);
        mvc.perform(get(URL_EMPLOYEE_ME)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(employeeSample.getName())))
                .andExpect(jsonPath("$.username", is(employeeSample.getUsername())))
                .andExpect(jsonPath("$.password", is(employeeSample.getPassword())));
    }
}
