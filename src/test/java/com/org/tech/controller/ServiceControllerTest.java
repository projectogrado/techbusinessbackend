package com.org.tech.controller;

import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.factory.ServiceFactory;
import com.org.tech.model.Services;
import com.org.tech.service.impl.ServiceServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class ServiceControllerTest {

    private final String URL_SERVICES = "/services";
    private final String URL_SERVICES_ID = "/services/1";
    private final String URL_SERVICES_INSERT = "/services/insert-service";
    private final String URL_SERVICES_UPDATE = "/services/update-service";
    private final String URL_SERVICES_ALL = "/services/all";

    private Services servicesSample;
    private Services servicesCreate;
    private Services servicesErrorCreate;
    private List<Services> servicesList;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ServiceServiceImpl mockServiceService;

    @BeforeEach
    public void setup() {
        servicesSample = new ServiceFactory().newInstance();
        servicesCreate = new ServiceFactory().setId(null).newInstance();
        servicesErrorCreate = new ServiceFactory()
                .setId(2L)
                .newInstance();

        servicesList = new ArrayList<>();
        servicesList.add(servicesSample);
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenCreateService_thenReturnService() throws Exception {
        when(mockServiceService.create(any(Services.class))).thenReturn(servicesSample);
        mvc.perform(post(URL_SERVICES_INSERT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ServiceFactory().toJson(servicesCreate)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(servicesSample.getName())))
                .andExpect(jsonPath("$.description", is(servicesSample.getDescription())));
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenCreateService_thenErrorMessage() throws Exception {
        when(mockServiceService.create(any(Services.class))).thenReturn(servicesSample);
        mvc.perform(post(URL_SERVICES_INSERT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ServiceFactory().toJson(servicesErrorCreate)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenUpdateService_thenReturnService() throws Exception {
        when(mockServiceService.update(any(Services.class))).thenReturn(servicesSample);
        mvc.perform(put(URL_SERVICES_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ServiceFactory().toJson(servicesSample)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.description", is(servicesSample.getDescription())))
                .andExpect(jsonPath("$.name", is(servicesSample.getName())));
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenGetServices_thenReturnServices() throws Exception {
        when(mockServiceService.getAllService()).thenReturn(servicesList);
        mvc.perform(get(URL_SERVICES_ALL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].description", is(servicesSample.getDescription())))
                .andExpect(jsonPath("$[0].name", is(servicesSample.getName())));
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenDeleteServices_thenThrow() throws Exception {
        doThrow(new ResourceNotFoundException("services not found"))
                .when(mockServiceService).delete(anyLong());

        mvc.perform(delete(URL_SERVICES_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenDeleteServices_thenDeleted() throws Exception {
        doNothing().when(mockServiceService).delete(anyLong());

        mvc.perform(delete(URL_SERVICES_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenGetChecklist_thenReturnChecklist() throws Exception {
        when(mockServiceService.getServiceById(anyLong())).thenReturn(servicesSample);

        String URL_SERVICES_ID_CHECKLIST = "/services/1/checklist";

        mvc.perform(get(URL_SERVICES_ID_CHECKLIST)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
