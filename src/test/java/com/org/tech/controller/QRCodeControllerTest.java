package com.org.tech.controller;

import com.org.tech.exception.QRCodeException;
import com.org.tech.service.QRCodeService;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class QRCodeControllerTest {


    public static final String QRCODE = "/qrcode/generate";
    private DataFactory dataFactory;
    private String data;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private QRCodeService mockQrCodeService;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
        data = dataFactory.getRandomWord();

    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenGenerate_thenReturnQrCode() throws Exception {
        lenient().when(mockQrCodeService.createQRCode(anyString(), anyInt(), anyInt())).thenReturn(data);

        mvc.perform(get(QRCODE + "/?content=content")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(data)));

    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenCreate_thenThrow() throws Exception {
        lenient().when(mockQrCodeService.createQRCode(anyString(), anyInt(), anyInt())).thenThrow(new QRCodeException("EXITO"));

        mvc.perform(get(QRCODE + "/?content=content")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }
}