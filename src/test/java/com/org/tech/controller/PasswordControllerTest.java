package com.org.tech.controller;

import com.org.tech.factory.JsonFactoryCustom;
import com.org.tech.factory.UserFactory;
import com.org.tech.model.Employee;
import com.org.tech.model.dto.RecoverPasswordDto;
import com.org.tech.service.PasswordResetService;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class PasswordControllerTest {

    public static final String TEMPORAL_PASSWORD_SEND = "A temporary password has been sent to your email.";
    public static final String UPDATED_SUCCESSFULLY = "Your password has been updated successfully";
    private final String URL_RESTORE_PASSWORD = "/password-reset/recover-password";
    private final String URL_RESTORE_PASSWORD_UPDATE = "/password-reset/update-password";
    private RecoverPasswordDto recoverEmployee;
    private DataFactory dataFactory;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PasswordResetService mockPasswordResetService;


    @BeforeEach
    public void setup() {
        dataFactory = new DataFactory();
        Employee employee = new UserFactory().newInstanceEmployee();
        String password = dataFactory.getRandomWord();

        recoverEmployee = RecoverPasswordDto.builder()
                .oldPassword(employee.getPassword())
                .password(password)
                .verifiedPassword(password)
                .rol("EMPLOYEE")
                .username("jfrincon")
                .build();
    }

    @Test
    void whenRecoverPassword_thenResponse() throws Exception {
        when(mockPasswordResetService.recoverPassword(anyString()))
                .thenReturn(TEMPORAL_PASSWORD_SEND);
        mvc.perform(post(URL_RESTORE_PASSWORD + "/?username=jsrincon")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(TEMPORAL_PASSWORD_SEND)));
    }

    @Test
    void whenUpdatePassword_thenUpdated() throws Exception {
        when(mockPasswordResetService.setPassword(any(RecoverPasswordDto.class)))
                .thenReturn(UPDATED_SUCCESSFULLY);
        mvc.perform(put(URL_RESTORE_PASSWORD_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new JsonFactoryCustom().toJson(recoverEmployee)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(UPDATED_SUCCESSFULLY)));
    }

    @Test
    void whenUpdatePassword_thenThrow() throws Exception {
        recoverEmployee.setPassword(dataFactory.getRandomWord());
        when(mockPasswordResetService.setPassword(any(RecoverPasswordDto.class)))
                .thenReturn(UPDATED_SUCCESSFULLY);
        mvc.perform(put(URL_RESTORE_PASSWORD_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new JsonFactoryCustom().toJson(recoverEmployee)))
                .andExpect(status().isBadRequest());
    }
}
