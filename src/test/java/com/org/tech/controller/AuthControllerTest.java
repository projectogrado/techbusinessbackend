package com.org.tech.controller;

import com.org.tech.factory.UserFactory;
import com.org.tech.model.dto.LoginDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class AuthControllerTest {

    private final String URL_AUTH = "/auth/login";
    private String loginContent;
    private String loginEmployeeContent;
    private String loginContentTemporalPassword;
    private String loginEmployeeContentTemporalPassword;

    @Autowired
    private MockMvc mvc;

    @BeforeEach
    public void setup() {
        LoginDto sampleUserLogin = new UserFactory().newInstanceLogin();
        loginContent = new UserFactory().loginJson(sampleUserLogin);
        loginEmployeeContent = new UserFactory().loginJson(new UserFactory()
                .setUsername("jfrincon")
                .setPassword("81943301Fe/").newInstanceLogin());
        loginContentTemporalPassword = new UserFactory().loginJson(new UserFactory()
                .setUsername("jrinconc")
                .setPassword("81943301Se/").newInstanceLogin());
        loginEmployeeContentTemporalPassword = new UserFactory().loginJson(new UserFactory()
                .setUsername("fandrade")
                .setPassword("81943301Fe/").newInstanceLogin());
    }

    @Test
    void whenAuthentication_thenReturnToken() throws Exception {
        mvc.perform(post(URL_AUTH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginContent))
                .andExpect(status().isOk());
    }

    @Test
    void whenAuthenticationWithTemporalPassword_thenRedirect() throws Exception {
        mvc.perform(post(URL_AUTH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginContentTemporalPassword))
                .andExpect(status().isPermanentRedirect());
    }

    @Test
    void whenAuthenticationEmployee_thenReturnToken() throws Exception {
        mvc.perform(post(URL_AUTH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginEmployeeContent))
                .andExpect(status().isOk());
    }

    @Test
    void whenAuthenticationEmployeeWithTemporalPassword_thenRedirect() throws Exception {
        mvc.perform(post(URL_AUTH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginEmployeeContentTemporalPassword))
                .andExpect(status().isPermanentRedirect());
    }
}
