package com.org.tech.controller;

import com.org.tech.factory.AdmissionDataSheetFactory;
import com.org.tech.factory.JsonFactoryCustom;
import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.enums.StatusEquipment;
import com.org.tech.service.AdmissionDataSheetService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class AdmissionDataSheetControllerTest {

    private final String URL_ADMISSION = "/admission/insert-admission";
    private final String URL_ADMISSION_ID = "/admission/public/1001";
    private final String URL_ADMISSION_CLIMB = "/admission/climb-job?admission=1001";
    private final String URL_ADMISSION_STATUS = "/admission/update-status-admission?id=1000&status=TO_DO";
    private final String URL_PDF_ID = "/admission/generate-pdf/1001";
    private final String URL_SEND_PDF_ID = "/admission/send-pdf/1001";
    private AdmissionDataSheet admission;
    @Autowired
    private MockMvc mvc;

    @MockBean
    private AdmissionDataSheetService mockAdmissionDataSheetService;


    @BeforeEach
    void setUp() {
        admission = new AdmissionDataSheetFactory(true).newInstance();
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenSave_thenPersisted() throws Exception {
        admission.setId(null);
        when(mockAdmissionDataSheetService.create(any(AdmissionDataSheet.class))).thenReturn(admission);

        mvc.perform(post(URL_ADMISSION)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new JsonFactoryCustom().toJson(admission)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.observation", is(admission.getObservation())))
                .andExpect(jsonPath("$.reason", is(admission.getReason())))
                .andExpect(jsonPath("$.id", is(admission.getId())));
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenSave_thenThrow() throws Exception {
        when(mockAdmissionDataSheetService.create(any(AdmissionDataSheet.class))).thenReturn(admission);

        mvc.perform(post(URL_ADMISSION)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new JsonFactoryCustom().toJson(admission)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenGetAdmissionById_thenReturn() throws Exception {
        lenient().when(mockAdmissionDataSheetService.getAdmissionDataSheetById(anyLong())).thenReturn(admission);
        mvc.perform(get(URL_ADMISSION_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new JsonFactoryCustom().toJson(admission)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.observation", is(admission.getObservation())))
                .andExpect(jsonPath("$.reason", is(admission.getReason())));
    }


    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenClimbJob_thenMessage() throws Exception {
        doNothing().when(mockAdmissionDataSheetService).climbJob(anyLong());
        mvc.perform(get(URL_ADMISSION_CLIMB)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenUpdateStatus_thenMessage() throws Exception {
        var message = "Update success";
        lenient().when(mockAdmissionDataSheetService.terminateAdmission(anyLong(), any(StatusEquipment.class))).thenReturn(message);
        mvc.perform(patch(URL_ADMISSION_STATUS)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is(message)));
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenGetPdf_thenReturn() throws Exception {
        mvc.perform(get(URL_PDF_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new JsonFactoryCustom().toJson(admission)))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenSendPdf_thenReturn() throws Exception {
        mvc.perform(get(URL_SEND_PDF_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new JsonFactoryCustom().toJson(admission)))
                .andExpect(status().isOk());
    }

}