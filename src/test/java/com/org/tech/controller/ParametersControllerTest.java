package com.org.tech.controller;

import com.org.tech.factory.ParametersFactory;
import com.org.tech.model.Parameters;
import com.org.tech.service.ParametersService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ParametersControllerTest {

    private Parameters parameters;

    @Mock
    private ParametersService mockParametersService;

    @InjectMocks
    private ParametersController parametersController;

    @BeforeEach
    void setUp() {
        parameters = new ParametersFactory().newInstance();
    }

    @Test
    void whenSave_thenPersisted() {
        when(mockParametersService.createParameters(any(Parameters.class))).thenReturn(parameters);
        var response = parametersController.create(parameters);

        assertThat(HttpStatus.CREATED.value())
                .isEqualTo(response.getStatusCodeValue());

        assertThat(parameters).isEqualTo(response.getBody());
    }

    @Test
    void whenGet_thenReturnParameters() {
        when(mockParametersService.getParameters()).thenReturn(parameters);
        var response = parametersController.getLastParameters();

        assertThat(HttpStatus.OK.value())
                .isEqualTo(response.getStatusCodeValue());

        assertThat(parameters).isEqualTo(response.getBody());
    }

}