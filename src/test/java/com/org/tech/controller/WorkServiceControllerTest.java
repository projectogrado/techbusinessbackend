package com.org.tech.controller;

import com.org.tech.model.ChecklistRepair;
import com.org.tech.model.WorkService;
import com.org.tech.model.keys.ChecklistRepairKey;
import com.org.tech.service.WorkServiceService;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class WorkServiceControllerTest {

    DataFactory dataFactory;

    @Mock
    private WorkServiceService mockWorkServiceService;

    @InjectMocks
    private WorkServiceController workServiceController;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
    }

    @Test
    void whenSave_thenPersisted() {
        WorkService sample = new WorkService();
        lenient().when(mockWorkServiceService.create(any(WorkService.class))).thenReturn(sample);
        ResponseEntity<WorkService> works = workServiceController.create(sample);

        WorkService persistedWorkService = works.getBody();
        assertThat(persistedWorkService).isNotNull();
    }

    @Test
    void whenAddChecklistWorkList_thenReturnProcess() {
        ChecklistRepair checklistRepair = new ChecklistRepair();
        ChecklistRepairKey key = new ChecklistRepairKey();
        key.setWorkServiceId(1001L);
        key.setRepairRouteId(1001L);
        checklistRepair.setId(key);

        List<ChecklistRepair> items = Collections.singletonList(checklistRepair);
        lenient().when(mockWorkServiceService.addChecklistWorkService(any(List.class), anyLong())).thenReturn("exito");
        ResponseEntity<String> works = workServiceController.addChecklist(items, 100L);

        String message = works.getBody();
        assertThat(message).isNotNull();
    }

    @Test
    void whenGetServiceById_thenReturnService() {
        WorkService sample = new WorkService();
        List<WorkService> items = Collections.singletonList(sample);
        lenient().when(mockWorkServiceService.getAllWorkServiceByAdmission(anyLong())).thenReturn(items);
        ResponseEntity<List<WorkService>> works = workServiceController.getWorkServiceById(100L);
        assertThat(HttpStatus.OK.value()).isEqualTo(works.getStatusCodeValue());
        assertThat(items).isEqualTo(works.getBody());
    }
}