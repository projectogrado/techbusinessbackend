package com.org.tech.controller;

import com.org.tech.factory.UserFactory;
import com.org.tech.model.Administrator;
import com.org.tech.service.AdministratorService;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class AdministratorControllerTest {

    private final String URL_ADMIN = "/admin";
    private final String URL_ADMIN_INSERT = "/admin/insert-administrator";
    private final String URL_ADMIN_UPDATE = "/admin/update-administrator";
    private final String URL_ADMIN_ALL = "/admin/all";

    private Administrator administratorSample;
    private Administrator administratorCreate;
    private Administrator administratorErrorCreate;
    private List<Administrator> administrators;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AdministratorService mockAdministratorService;


    @BeforeEach
    public void setup() {
        DataFactory dataFactory = new DataFactory();
        administratorSample = new UserFactory().newInstanceAdministrator();
        administratorCreate = new UserFactory().setId(null).newInstanceAdministrator();
        administratorErrorCreate = new UserFactory()
                .setId(null)
                .setPassword(dataFactory.getRandomWord())
                .newInstanceAdministrator();

        administrators = new ArrayList<>();
        administrators.add(administratorSample);
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenCreateAdministrator_thenReturnAdministrator() throws Exception {
        when(mockAdministratorService.create(any(Administrator.class)))
                .thenReturn(administratorSample);
        mvc.perform(post(URL_ADMIN_INSERT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new UserFactory().administratorJson(administratorCreate)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(administratorSample.getName())))
                .andExpect(jsonPath("$.username", is(administratorSample.getUsername())))
                .andExpect(jsonPath("$.password", is(administratorSample.getPassword())));
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenCreateAdministrator_thenErrorMessage() throws Exception {
        when(mockAdministratorService.create(any(Administrator.class)))
                .thenReturn(administratorSample);
        mvc.perform(post(URL_ADMIN_INSERT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new UserFactory().administratorJson(administratorErrorCreate)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenUpdateAdministrator_thenReturnAdministrator() throws Exception {
        when(mockAdministratorService.update(any(Administrator.class)))
                .thenReturn(administratorSample);
        mvc.perform(put(URL_ADMIN_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new UserFactory().administratorJson(administratorSample)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(administratorSample.getName())))
                .andExpect(jsonPath("$.username", is(administratorSample.getUsername())))
                .andExpect(jsonPath("$.password", is(administratorSample.getPassword())));
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenGetAdministrator_thenReturnAdministrator() throws Exception {
        when(mockAdministratorService.getAllAdministrators()).thenReturn(administrators);
        mvc.perform(get(URL_ADMIN_ALL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(administratorSample.getName())))
                .andExpect(jsonPath("$[0].username", is(administratorSample.getUsername())))
                .andExpect(jsonPath("$[0].password", is(administratorSample.getPassword())));
    }
}
