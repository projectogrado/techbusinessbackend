package com.org.tech.controller;

import com.org.tech.factory.UserFactory;
import com.org.tech.model.Customer;
import com.org.tech.service.impl.CustomerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Profile(value = {"dev","prod"})
@ActiveProfiles(value = {"dev","prod"})
class CustomerControllerTest {

    private final String URL_CUSTOMER_INSERT = "/customer/insert-customer";
    private final String URL_CUSTOMER_UPDATE = "/customer/update-customer";
    private final String URL_CUSTOMER_ID = "/customer/1";
    private final String URL_CUSTOMER_ALL = "/customer/all";

    private Customer customerSample;
    private Customer customerCreate;
    private List<Customer> customers;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CustomerServiceImpl mockCustomerService;


    @BeforeEach
    public void setup() {
        customerSample = new UserFactory().newInstanceCustomer();
        customerCreate = new UserFactory().setId(null).newInstanceCustomer();

        customers = new ArrayList<>();
        customers.add(customerSample);
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenCreateCustomer_thenReturnCustomer() throws Exception {
        when(mockCustomerService.create(any(Customer.class))).thenReturn(customerSample);
        mvc.perform(post(URL_CUSTOMER_INSERT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new UserFactory().customerJson(customerCreate)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(customerSample.getName())));
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenUpdateCustomer_thenReturnCustomer() throws Exception {
        when(mockCustomerService.update(any(Customer.class))).thenReturn(customerSample);
        mvc.perform(put(URL_CUSTOMER_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new UserFactory().customerJson(customerSample)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(customerSample.getName())));
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenGetCustomer_thenReturnCustomers() throws Exception {
        when(mockCustomerService.getAllCustomers()).thenReturn(customers);
        mvc.perform(get(URL_CUSTOMER_ALL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(customerSample.getName())));
    }

    @Test
    @WithMockUser(username = "jsrincon", roles = {"ADMIN"})
    void whenGetCustomerByDocument_thenReturnCustomers() throws Exception {
        when(mockCustomerService.getCustomerByDocument(anyString())).thenReturn(customerSample);
        mvc.perform(get(URL_CUSTOMER_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(customerSample.getName())))
                .andExpect(jsonPath("$.document", is(customerSample.getDocument())));
    }
}
