<!doctype html>
<html ⚡4email>
<head>
    <meta charset="utf-8">
    <style amp4email-boilerplate>body {
            visibility: hidden
        }</style>
    <script async src="https://cdn.ampproject.org/v0.js"></script>

    <style amp-custom>
        a[x-apple-data-detectors] {
            color: inherit;
            text-decoration: none;
            font-size: inherit;
            font-family: inherit;
            font-weight: inherit;
            line-height: inherit;
        }

        .es-desk-hidden {
            display: none;
            float: left;
            overflow: hidden;
            width: 0;
            max-height: 0;
            line-height: 0;
        }

        s {
            text-decoration: line-through;
        }

        body {
            width: 100%;
            font-family: "trebuchet ms", "lucida grande", "lucida sans unicode", "lucida sans", tahoma, sans-serif;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0px;
        }

        table td, html, body, .es-wrapper {
            padding: 0;
            Margin: 0;
        }

        .es-content, .es-header, .es-footer {
            table-layout: fixed;
            width: 100%;
        }

        p, hr {
            Margin: 0;
        }

        h1, h2, h3, h4, h5 {
            Margin: 0;
            line-height: 120%;
            font-family: "trebuchet ms", "lucida grande", "lucida sans unicode", "lucida sans", tahoma, sans-serif;
        }

        .es-left {
            float: left;
        }

        .es-right {
            float: right;
        }

        .es-p5 {
            padding: 5px;
        }

        .es-p5t {
            padding-top: 5px;
        }

        .es-p5b {
            padding-bottom: 5px;
        }

        .es-p5l {
            padding-left: 5px;
        }

        .es-p5r {
            padding-right: 5px;
        }

        .es-p10 {
            padding: 10px;
        }

        .es-p10t {
            padding-top: 10px;
        }

        .es-p10b {
            padding-bottom: 10px;
        }

        .es-p10l {
            padding-left: 10px;
        }

        .es-p10r {
            padding-right: 10px;
        }

        .es-p15 {
            padding: 15px;
        }

        .es-p15t {
            padding-top: 15px;
        }

        .es-p15b {
            padding-bottom: 15px;
        }

        .es-p15l {
            padding-left: 15px;
        }

        .es-p15r {
            padding-right: 15px;
        }

        .es-p20 {
            padding: 20px;
        }

        .es-p20t {
            padding-top: 20px;
        }

        .es-p20b {
            padding-bottom: 20px;
        }

        .es-p20l {
            padding-left: 20px;
        }

        .es-p20r {
            padding-right: 20px;
        }

        .es-p25 {
            padding: 25px;
        }

        .es-p25t {
            padding-top: 25px;
        }

        .es-p25b {
            padding-bottom: 25px;
        }

        .es-p25l {
            padding-left: 25px;
        }

        .es-p25r {
            padding-right: 25px;
        }

        .es-p30 {
            padding: 30px;
        }

        .es-p30t {
            padding-top: 30px;
        }

        .es-p30b {
            padding-bottom: 30px;
        }

        .es-p30l {
            padding-left: 30px;
        }

        .es-p30r {
            padding-right: 30px;
        }

        .es-p35 {
            padding: 35px;
        }

        .es-p35t {
            padding-top: 35px;
        }

        .es-p35b {
            padding-bottom: 35px;
        }

        .es-p35l {
            padding-left: 35px;
        }

        .es-p35r {
            padding-right: 35px;
        }

        .es-p40 {
            padding: 40px;
        }

        .es-p40t {
            padding-top: 40px;
        }

        .es-p40b {
            padding-bottom: 40px;
        }

        .es-p40l {
            padding-left: 40px;
        }

        .es-p40r {
            padding-right: 40px;
        }

        .es-menu td {
            border: 0;
        }

        a {
            font-family: "trebuchet ms", "lucida grande", "lucida sans unicode", "lucida sans", tahoma, sans-serif;
            font-size: 16px;
            text-decoration: underline;
        }

        h1 {
            font-size: 34px;
            font-style: normal;
            font-weight: normal;
            color: #333333;
        }

        h1 a {
            font-size: 34px;
        }

        h2 {
            font-size: 26px;
            font-style: normal;
            font-weight: normal;
            color: #333333;
        }

        h2 a {
            font-size: 26px;
        }

        h3 {
            font-size: 18px;
            font-style: normal;
            font-weight: normal;
            color: #333333;
        }

        h3 a {
            font-size: 18px;
        }

        p, ul li, ol li {
            font-size: 16px;
            font-family: "trebuchet ms", "lucida grande", "lucida sans unicode", "lucida sans", tahoma, sans-serif;
            line-height: 150%;
        }

        ul li, ol li {
            Margin-bottom: 15px;
        }

        .es-menu td a {
            text-decoration: none;
            display: block;
        }

        .es-menu amp-img, .es-button amp-img {
            vertical-align: middle;
        }

        .es-wrapper {
            width: 100%;
            height: 100%;
        }

        .es-wrapper-color {
            background-color: #FFFFFF;
        }

        .es-content-body {
            background-color: #FFFFFF;
        }

        .es-content-body p, .es-content-body ul li, .es-content-body ol li {
            color: #333333;
        }

        .es-content-body a {
            color: #1376C8;
        }

        .es-header {
            background-color: transparent;
        }

        .es-header-body {
            background-color: transparent;
        }

        .es-header-body p, .es-header-body ul li, .es-header-body ol li {
            color: #333333;
            font-size: 14px;
        }

        .es-header-body a {
            color: #1376C8;
            font-size: 14px;
        }

        .es-footer {
            background-color: #EDF0F3;
        }

        .es-footer-body {
            background-color: #EDF0F3;
        }

        .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li {
            color: #333333;
            font-size: 14px;
        }

        .es-footer-body a {
            color: #333333;
            font-size: 14px;
        }

        .es-infoblock, .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li {
            line-height: 120%;
            font-size: 12px;
            color: #CCCCCC;
        }

        .es-infoblock a {
            font-size: 12px;
            color: #CCCCCC;
        }

        a.es-button {
            border-style: solid;
            border-color: #EDF0F3;
            border-width: 10px 20px 10px 20px;
            display: inline-block;
            background: #EDF0F3;
            border-radius: 0px;
            font-size: 14px;
            font-family: arial, "helvetica neue", helvetica, sans-serif;
            font-weight: bold;
            font-style: normal;
            line-height: 120%;
            color: #1376C8;
            text-decoration: none;
            width: auto;
            text-align: center;
        }

        .es-button-border {
            border-style: solid solid solid solid;
            border-color: transparent transparent transparent transparent;
            background: #EDF0F3;
            border-width: 0px 0px 0px 0px;
            display: inline-block;
            border-radius: 0px;
            width: auto;
        }

        @media only screen and (max-width: 600px) {
            p, ul li, ol li, a {
                font-size: 14px
            }

            h1 {
                font-size: 30px;
                text-align: center
            }

            h2 {
                font-size: 24px;
                text-align: center
            }

            h3 {
                font-size: 18px;
                text-align: center
            }

            h1 a {
                font-size: 30px
            }

            h2 a {
                font-size: 24px
            }

            h3 a {
                font-size: 18px
            }

            .es-menu td a {
                font-size: 14px
            }

            .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a {
                font-size: 14px
            }

            .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a {
                font-size: 14px
            }

            .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a {
                font-size: 12px
            }

            *[class="gmail-fix"] {
                display: none
            }

            .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 {
                text-align: center
            }

            .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 {
                text-align: right
            }

            .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 {
                text-align: left
            }

            .es-m-txt-r amp-img {
                float: right
            }

            .es-m-txt-c amp-img {
                margin: 0 auto
            }

            .es-m-txt-l amp-img {
                float: left
            }

            .es-button-border {
                display: inline-block
            }

            .es-button {
                font-size: 14px;
                display: inline-block
            }

            .es-btn-fw {
                border-width: 10px 0px;
                text-align: center
            }

            .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right {
                width: 100%
            }

            .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header {
                width: 100%;
                max-width: 600px
            }

            .es-adapt-td {
                display: block;
                width: 100%
            }

            .adapt-img {
                width: 100%;
                height: auto
            }

            td.es-m-p0 {
                padding: 0px
            }

            td.es-m-p0r {
                padding-right: 0px
            }

            td.es-m-p0l {
                padding-left: 0px
            }

            td.es-m-p0t {
                padding-top: 0px
            }

            td.es-m-p0b {
                padding-bottom: 0
            }

            td.es-m-p20b {
                padding-bottom: 20px
            }

            .es-mobile-hidden, .es-hidden {
                display: none
            }

            tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden {
                width: auto;
                overflow: visible;
                float: none;
                max-height: inherit;
                line-height: inherit
            }

            tr.es-desk-hidden {
                display: table-row
            }

            table.es-desk-hidden {
                display: table
            }

            td.es-desk-menu-hidden {
                display: table-cell
            }

            .es-menu td {
                width: 1%
            }

            table.es-table-not-adapt, .esd-block-html table {
                width: auto
            }

            table.es-social {
                display: inline-block
            }

            table.es-social td {
                display: inline-block
            }
        }
    </style>
</head>
<body>
<div class="es-wrapper-color">
    <!--[if gte mso 9]>
    <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" color="#ffffff"></v:fill>
    </v:background>
    <![endif]-->
    <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td valign="top">
                <table cellpadding="0" cellspacing="0" class="es-content" align="center">
                    <tr>
                        <td class="es-adaptive" style="background-color: transparent" bgcolor="transparent"
                            align="center">
                            <table class="es-content-body" style="background-color: transparent" width="600"
                                   cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                                <tr>
                                    <td class="es-p10" align="left">
                                        <!--[if mso]>
                                        <table width="580">
                                            <tr>
                                                <td width="280" valign="top"><![endif]-->
                                        <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                                            <tr>
                                                <td width="280" align="left">
                                                    <table width="100%" cellspacing="0" cellpadding="0"
                                                           role="presentation">
                                                        <tr>
                                                            <td class="es-infoblock es-m-txt-c" align="left"><p></p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--[if mso]></td>
                                        <td width="20"></td>
                                        <td width="280" valign="top"><![endif]-->
                                        <table class="es-right" cellspacing="0" cellpadding="0" align="right">
                                            <tr>
                                                <td width="280" align="left">
                                                    <table width="100%" cellspacing="0" cellpadding="0"
                                                           role="presentation">
                                                        <tr>
                                                            <td align="right" class="es-infoblock es-m-txt-c"><p><a
                                                                            class="view"
                                                                            href="https://viewstripo.email"></a></p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--[if mso]></td></tr></table><![endif]--></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" class="es-header" align="center">
                    <tr>
                        <td align="center">
                            <table class="es-header-body" width="600" cellspacing="0" cellpadding="0"
                                   bgcolor="rgba(0, 0, 0, 0)" align="center">
                                <tr>
                                    <td class="es-p20t es-p20b es-p20r es-p20l"
                                        style="background-position: center bottom" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="560" valign="top" align="center">
                                                    <table width="100%" cellspacing="0" cellpadding="0"
                                                           role="presentation">
                                                        <tr>
                                                            <td align="center" style="font-size:0"><a target="_blank"
                                                                                                      href="https://viewstripo.email">
                                                                    <amp-img
                                                                            src="https://oerxoh.stripocdn.email/content/guids/CABINET_a906e668866a7123ea4f0e3eed846f3d/images/22291543329322607.png"
                                                                            alt style="display: block" width="128"
                                                                            height="51"></amp-img>
                                                                </a></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="es-content" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td class="es-adaptive" style="background-color: #1376c8" bgcolor="#1376c8" align="center">
                            <table class="es-header-body" style="background-color: transparent" width="600"
                                   cellspacing="0" cellpadding="0" bgcolor="transparent" align="center">
                                <tr>
                                    <td class="es-p15t es-p15b es-p15r es-p15l" style="background-position: center top"
                                        align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="570" valign="top" align="center">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="center" style="display: none"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="es-content" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td style="background-color: #f3f3f3" bgcolor="#f3f3f3" align="center">
                            <table class="es-content-body" style="background-color: transparent" width="600"
                                   cellspacing="0" cellpadding="0" bgcolor="transparent" align="center">
                                <tr>
                                    <td class="es-p25t es-p5b es-p20r es-p20l" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="560" valign="top" align="center">
                                                    <table width="100%" cellspacing="0" cellpadding="0"
                                                           role="presentation">
                                                        <tr>
                                                            <td class="es-p20t es-p10b" align="center"><h1>
                                                                    contraseña temporal</h1></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="es-m-txt-c es-p30b" align="center"><h3
                                                                        style="letter-spacing: 1px">
                                                                    <strong>${content}</strong></h3></td>
                                                        </tr>
                                                        <tr>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p20r es-p20l" style="background-position: right bottom" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="560" valign="top" align="center">
                                                    <table width="100%" cellspacing="0" cellpadding="0"
                                                           role="presentation">
                                                        <tr>
                                                            <td align="left" style="font-size:0">
                                                                <amp-img class="adapt-img"
                                                                         src="https://oerxoh.stripocdn.email/content/guids/CABINET_a20d5d34e9dff5d8e26f5eda22682851/images/21881547721107368.jpg"
                                                                         alt style="display: block" width="534"
                                                                         height="380" layout="responsive"></amp-img>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" class="es-content" align="center">
                    <tr>
                        <td align="center">
                            <table class="es-content-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff"
                                   align="center">
                                <tr>
                                    <td class="es-p20t es-p20b es-p20r es-p20l"
                                        style="background-position: center bottom" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="560" valign="top" align="center">
                                                    <table width="100%" cellspacing="0" cellpadding="0"
                                                           role="presentation">
                                                        <tr>
                                                            <td align="center" class="es-infoblock"><p>You have received
                                                                    this email as a registered user of
                                                                    veterinary.com.</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="es-p10t es-infoblock" align="center"><p>© Company
                                                                    Veterinary, Inc.</p></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="es-content" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td align="center">
                            <table class="es-content-body" style="background-color: transparent" width="600"
                                   cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                                <tr>
                                    <td class="es-p30t es-p30b es-p20r es-p20l" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="560" valign="top" align="center">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="center" style="display: none"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>