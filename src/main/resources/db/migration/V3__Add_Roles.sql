alter table user_tb
    add email character varying(250);

alter table user_tb
    add phone character varying(250);

alter table user_tb
    add name character varying(250);


create table role
(
    id          serial                 not null,
    name        character varying(250) not null,
    description character varying(250) not null,
    primary key (id)
);

create table user_role
(
    user_id integer,
    role_id integer,
    primary key (user_id, role_id)
);
ALTER TABLE user_role
    ADD CONSTRAINT fk_user_role_users FOREIGN KEY (user_id) REFERENCES user_tb (id);
ALTER TABLE user_role
    ADD CONSTRAINT fk_user_role_roles FOREIGN KEY (role_id) REFERENCES role (id);

