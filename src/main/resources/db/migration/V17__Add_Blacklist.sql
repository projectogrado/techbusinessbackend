CREATE TABLE blacklist
(
    admission_id bigint       NOT NULL,
    employee_id  bigint       NOT NULL,
    reason       varchar(255) NOT NULL,
    PRIMARY KEY (admission_id,
                 employee_id)
);

ALTER TABLE blacklist
    ADD CONSTRAINT fk_blacklist_admission FOREIGN KEY (admission_id) REFERENCES admission_data_sheet (id);
ALTER TABLE blacklist
    ADD CONSTRAINT fk_blacklist_employee FOREIGN KEY (employee_id) REFERENCES employee (uid);