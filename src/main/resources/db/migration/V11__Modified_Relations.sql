drop table checklist_repair;
drop table work_service;
drop table repair_route;

CREATE TABLE repair_route
(
    id         SERIAL NOT NULL,
    service_id bigint NOT NULL,
    process_id bigint NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE checklist_repair
(
    repair_route_id bigint       NOT NULL,
    work_service_id bigint       NOT NULL,
    status          bool,
    evidence        varchar(255) NOT NULL,
    PRIMARY KEY (repair_route_id,
                 work_service_id)
);
CREATE TABLE work_service
(
    id                      SERIAL NOT NULL,
    service_id              int4   NOT NULL,
    admission_data_sheet_id int4   NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE work_service
    ADD CONSTRAINT fk_work_service_service FOREIGN KEY (service_id) REFERENCES service (id);
ALTER TABLE work_service
    ADD CONSTRAINT fk_work_service_admission FOREIGN KEY (admission_data_sheet_id)
        REFERENCES admission_data_sheet (id);
ALTER TABLE repair_route
    ADD CONSTRAINT fk_repair_route_service FOREIGN KEY (service_id) REFERENCES service (id);
ALTER TABLE repair_route
    ADD CONSTRAINT fk_repair_route_process FOREIGN KEY (process_id) REFERENCES process (id);
ALTER TABLE checklist_repair
    ADD CONSTRAINT fk_checklist_repair_check FOREIGN KEY (repair_route_id) REFERENCES repair_route (id);
ALTER TABLE checklist_repair
    ADD CONSTRAINT fk_checklist_repair_work FOREIGN KEY (work_service_id) REFERENCES work_service (id);

