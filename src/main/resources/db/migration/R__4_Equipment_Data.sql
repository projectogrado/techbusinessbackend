INSERT INTO equipment(id, mark, model, type, hard_disk, ram, board, processor, status, reader_cd, other)
VALUES (1001, 'ASUS', 'X507UF', 'NOTEBOOK', '1TB + 256SSD', '12GB DDR4', 'INTEL', 'INTEL CORE I5 8400 - 1.6Ghz',
        'BY_ASSIGNING', 'N/A', 'WEB CAM'),
       (1002, 'HP', 'PROBOOK', 'NOTEBOOK', '1TB + 256SSD', '16GB DDR4', 'INTEL', 'INTEL CORE 75 10600 - 1.6Ghz',
        'BY_ASSIGNING', 'N/A', 'N/A')
