create table customer
(
    id       serial                not null,
    name     character varying(50) not null,
    lastname character varying(50) not null,
    card     character varying(12) not null,
    phone    character varying(10) not null,
    email    character varying(50) not null,
    address  character varying(50) not null,
    primary key (id)
);
