CREATE TABLE parameters
(
    id               serial                not null,
    time_to_do       int                   not null,
    time_in_repair   int                   not null,
    time_repaired    int                   not null,
    time_no_repaired int                   not null,
    time_novelty     int                   not null,
    cron_task        character varying(50) not null,
    date_created     timestamp             not null,
    PRIMARY KEY (id)
);