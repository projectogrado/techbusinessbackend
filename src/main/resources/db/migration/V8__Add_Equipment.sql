CREATE TABLE equipment
(
    id        serial                 not null,
    mark      character varying(250) not null,
    model     character varying(250) not null,
    type      character varying(250) not null,
    hard_disk character varying(250) not null,
    ram       character varying(250) not null,
    board     character varying(250) not null,
    processor character varying(250) not null,
    status    character varying(250) not null,
    PRIMARY KEY (id)
);
