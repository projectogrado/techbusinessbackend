INSERT INTO admission_data_sheet(id, date, reason, observation, employee_id, customer_id, equipment_id, status)
VALUES (1001, to_date('2021-03-24', 'YYYY-MM-dd'), 'NO INGRESA AL SISTEMA',
        'EL EQUIPO TIENE ARCHIVOS DE ARRANQUE DAÑADOS', 1002, 1003, 1001, false),
       (1002, to_date('2021-03-24', 'YYYY-MM-dd'), 'NO INGRESA AL SISTEMA',
        'EL EQUIPO NO ENCIENDEN', 1002, 1003, 1002, true);

INSERT INTO discharge_data_sheet(id, date, admission_data_sheet_id)
VALUES (1001, to_date('2021-03-26', 'YYYY-MM-dd'), 1001);


INSERT INTO work_service(id, service_id, admission_data_sheet_id)
VALUES (1001, 1001, 1001);