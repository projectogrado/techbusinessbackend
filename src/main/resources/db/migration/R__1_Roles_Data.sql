INSERT INTO role (id, description, name)
VALUES (1, 'Admin role', 'ADMIN');
INSERT INTO role (id, description, name)
VALUES (2, 'User role', 'USER');
INSERT INTO role (id, description, name)
VALUES (3, 'Employee role', 'EMPLOYEE');