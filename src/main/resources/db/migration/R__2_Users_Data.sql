INSERT INTO user_tb(id, name, lastname, document, type_document, phone, email, address)
VALUES (1001, 'Sebastian', 'Rincón', '1144201661', 'CC', '3123196836', 'sebas199765@gmail.com',
        'carrera 32 # 101 - 15'),
       (1002, 'Felipe', 'Rincón', '1144201659', 'CC', '321760015', 'rincón3549@gmail.com',
        'carrera 11D # 45 - 31'),
       (1003, 'Ingrith', 'Santamaria', '1101760015', 'CC', '3215880165', 'ingrithsanta@gmail.com',
        'carrera 32 # 101 - 15'),
       (1004, 'Jairo', 'Rincón', '16887927', 'CC', '3122341935', 'jrchgem@gmail.com',
        'calle 5 # 5 - 10'),
       (1005, 'Flor', 'Andrade', '1112223191', 'CC', '311682047', 'florandrade@gmail.com',
        'calle 5 # 5 - 10'),
       (1006, 'Other', 'other', '11122223191', 'CC', '311682047', 'other@gmail.com',
        'calle 5 # 5 - 10');

INSERT INTO employee(uid, username, password, status, is_temporal_password)
VALUES (1002, 'jfrincon', '$2a$10$waU6veh5AQikcUWvYs5t7OCwVe0FSnarm7T56lFSACfQVQUU8ZVb.', true, false),
       (1005, 'fandrade', '$2a$10$waU6veh5AQikcUWvYs5t7OCwVe0FSnarm7T56lFSACfQVQUU8ZVb.', true, true),
       (1006, 'otheruser', '$2a$10$waU6veh5AQikcUWvYs5t7OCwVe0FSnarm7T56lFSACfQVQUU8ZVb.', true, true);

INSERT INTO administrator(uid, username, password, is_temporal_password)
VALUES (1001, 'jsrincon', '$2a$10$bWXcz9jVeEKrX/XcBvryYO4YCdemusTx.PRkK/3ZCgvejaew/am0O', false),
       (1004, 'jrinconc', '$2a$10$bWXcz9jVeEKrX/XcBvryYO4YCdemusTx.PRkK/3ZCgvejaew/am0O', true);

INSERT INTO customer(uid)
VALUES (1003);

INSERT INTO user_role(user_id, role_id)
VALUES (1001, 1),
       (1001, 2),
       (1002, 2),
       (1002, 3),
       (1003, 2),
       (1004, 1),
       (1004, 2),
       (1005, 2),
       (1005, 3),
       (1006, 2),
       (1006, 3);