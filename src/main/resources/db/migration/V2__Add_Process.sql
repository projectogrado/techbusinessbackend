CREATE TABLE process
(
    id          serial                 NOT NULL,
    name        character varying(50)  NOT NULL,
    description character varying(150) NOT NULL,
    status      boolean                NOT NULL,
    PRIMARY KEY (id)
);
