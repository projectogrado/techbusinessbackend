ALTER TABLE administrator
    ADD COLUMN is_temporal_password bool;
ALTER TABLE employee
    ADD COLUMN is_temporal_password bool;