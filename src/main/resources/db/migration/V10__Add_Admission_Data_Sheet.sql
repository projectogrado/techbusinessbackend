CREATE TABLE admission_data_sheet
(
    id           SERIAL                 NOT NULL,
    date         date                   NOT NULL,
    reason       character varying(250) NOT NULL,
    observation  character varying(250) NOT NULL,
    employee_id  bigint,
    customer_id  bigint                 NOT NULL,
    equipment_id bigint                 NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE checklist_repair
(
    repair_route_service_id bigint NOT NULL,
    repair_route_process_id bigint NOT NULL,
    service_id              bigint NOT NULL,
    admission_data_sheet_id bigint NOT NULL,
    status                  bool,
    evidence                character varying(250),
    PRIMARY KEY (repair_route_service_id, repair_route_process_id, service_id, admission_data_sheet_id)
);
CREATE TABLE component
(
    id   SERIAL                 NOT NULL,
    name character varying(250) NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE component_status
(
    component_id            bigint NOT NULL,
    admission_data_sheet_id bigint NOT NULL,
    status                  bool   NOT NULL,
    PRIMARY KEY (component_id, admission_data_sheet_id)
);
CREATE TABLE discharge_data_sheet
(
    id                      SERIAL                 NOT NULL,
    date                    date                   NOT NULL,
    admission_data_sheet_id bigint                 NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE work_service
(
    service_id                bigint NOT NULL,
    "admission_data_sheet_id" bigint NOT NULL,
    PRIMARY KEY (service_id, "admission_data_sheet_id")
);

ALTER TABLE admission_data_sheet
    ADD CONSTRAINT fk_admission_data_sheet_equipment FOREIGN KEY (equipment_id) REFERENCES equipment (id);

ALTER TABLE admission_data_sheet
    ADD CONSTRAINT fk_admission_customer FOREIGN KEY (customer_id) REFERENCES customer (uid);

ALTER TABLE admission_data_sheet
    ADD CONSTRAINT fk_admission_employee FOREIGN KEY (employee_id) REFERENCES employee (uid);

ALTER TABLE component_status
    ADD CONSTRAINT fk_component_status_component FOREIGN KEY (component_id) REFERENCES component (id);

ALTER TABLE component_status
    ADD CONSTRAINT fk_component_status_admission_data_sheet FOREIGN KEY (admission_data_sheet_id)
        REFERENCES admission_data_sheet (id);

ALTER TABLE discharge_data_sheet
    ADD CONSTRAINT fk_discharge_data_sheet_admission_data_sheet FOREIGN KEY (admission_data_sheet_id)
        REFERENCES admission_data_sheet (id);

ALTER TABLE work_service
    ADD CONSTRAINT fk_work_service_service FOREIGN KEY (service_id) REFERENCES service (id);

ALTER TABLE work_service
    ADD CONSTRAINT fk_work_service_admission_data_sheet FOREIGN KEY (admission_data_sheet_id)
        REFERENCES admission_data_sheet (id);

ALTER TABLE checklist_repair
    ADD CONSTRAINT fk_checklist_work_service_checklist FOREIGN KEY (repair_route_service_id, repair_route_process_id)
        REFERENCES repair_route (service_id, process_id);

ALTER TABLE checklist_repair
    ADD CONSTRAINT fk_checklist_work_service_work_service FOREIGN KEY (service_id, admission_data_sheet_id)
        REFERENCES work_service (service_id, admission_data_sheet_id);
