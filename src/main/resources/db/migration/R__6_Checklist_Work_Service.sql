INSERT INTO work_service(id, service_id, admission_data_sheet_id)
VALUES (1002, 1002, 1002);

INSERT INTO checklist_repair(repair_route_id, work_service_id, status, evidence)
VALUES (1001, 1001, true, 'Evidence 1'),
       (1002, 1001, true, 'Evidence 2'),
       (1003, 1001, true, 'Evidence 3'),
       (1004, 1002, true, 'Evidence 1'),
       (1005, 1002, true, 'Evidence 2');