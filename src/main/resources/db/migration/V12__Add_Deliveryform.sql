create table delivery
(
    id                      serial                not null,
    name                    character varying(50) not null,
    lastname                character varying(50) not null,
    type_document           character varying(5)  not null,
    document                character varying(12) not null,
    phone                   character varying(10) not null,
    email                   character varying(50) not null,
    address                 character varying(50) not null,
    kinship                 character varying(50) not null,
    discharge_data_sheet_id int4                  not null,
    primary key (id)
);
ALTER TABLE delivery
    ADD CONSTRAINT fk_delivery_discharge FOREIGN KEY (discharge_data_sheet_id) REFERENCES discharge_data_sheet (id);