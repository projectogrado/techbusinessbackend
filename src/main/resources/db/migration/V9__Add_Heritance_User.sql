drop table user_role;
drop table user_tb;
drop table equipment;
drop table customer;

create table user_tb
(
    id       serial                not null,
    name     character varying(50) not null,
    lastname character varying(50) not null,
    card     character varying(12) not null,
    phone    character varying(10) not null,
    email    character varying(50) not null,
    address  character varying(50) not null,
    primary key (id)
);

create table customer
(
    uid bigint not null,
    primary key (uid),
    constraint fk_userId foreign key (uid) references user_tb (id)
);

create table administrator
(
    uid      bigint                 not null,
    username character varying(250) not null,
    password character varying(250) not null,
    primary key (uid),
    constraint fk_userId foreign key (uid) references user_tb (id)
);

create table employee
(
    uid      bigint                 not null,
    username character varying(250) not null,
    password character varying(250) not null,
    status   boolean                not null,
    primary key (uid),
    constraint fk_userId foreign key (uid) references user_tb (id)
);

create table user_role
(
    user_id integer,
    role_id integer,
    primary key (user_id, role_id)
);
ALTER TABLE user_role
    ADD CONSTRAINT fk_user_role_users FOREIGN KEY (user_id) REFERENCES user_tb (id);
ALTER TABLE user_role
    ADD CONSTRAINT fk_user_role_roles FOREIGN KEY (role_id) REFERENCES role (id);

CREATE TABLE equipment
(
    id        serial                 not null,
    mark      character varying(250) not null,
    model     character varying(250) not null,
    type      character varying(250) not null,
    hard_disk character varying(250) not null,
    ram       character varying(250) not null,
    board     character varying(250) not null,
    processor character varying(250) not null,
    status    character varying(250) not null,
    reader_cd character varying(250),
    other     character varying(250),
    PRIMARY KEY (id)
);
