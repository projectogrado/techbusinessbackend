ALTER TABLE equipment
    add column date_updated_status timestamp;

ALTER TABLE admission_data_sheet
    add column status bool;