CREATE TABLE service
(
    id          serial                 NOT NULL,
    name        character varying(50)  NOT NULL,
    description character varying(150) NOT NULL,
    status      boolean                NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE repair_route
(
    service_id bigint NOT NULL,
    process_id bigint NOT NULL,
    PRIMARY KEY (service_id, process_id)
);

ALTER TABLE repair_route
    ADD CONSTRAINT fk_repair_route_service FOREIGN KEY (service_id) REFERENCES service (id);
ALTER TABLE repair_route
    ADD CONSTRAINT fk_repair_route_process FOREIGN KEY (process_id) REFERENCES process (id);


