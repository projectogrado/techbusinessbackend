DROP TABLE delivery;

create table person_receive
(
    uid                     bigint                not null,
    kinship                 character varying(50) not null,
    discharge_data_sheet_id int4                  not null,
    primary key (uid),
    constraint fk_user_receive foreign key (uid) references user_tb (id),
    constraint fk_user_receive_discharge foreign key (discharge_data_sheet_id) references discharge_data_sheet (id)
);

alter table user_tb
    rename column card to document;

alter table user_tb
    add column type_document character varying(5);