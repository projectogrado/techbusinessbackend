package com.org.tech.exception;

public class QRCodeException extends RuntimeException {

    public QRCodeException(String message) {
        super(message);
    }
}
