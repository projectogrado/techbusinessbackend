package com.org.tech.exception;

public class LoginRedirectException extends RuntimeException {

    public LoginRedirectException(String message) {
        super(message);
    }
}
