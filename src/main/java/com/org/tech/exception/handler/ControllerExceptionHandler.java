package com.org.tech.exception.handler;

import com.org.tech.exception.AuthException;
import com.org.tech.exception.LoginRedirectException;
import com.org.tech.exception.PasswordMismatchException;
import com.org.tech.exception.QRCodeException;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.dto.ErrorMessageDto;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
@ResponseBody
public class ControllerExceptionHandler {

    public static final String GLOBAL = "Global";

    @ExceptionHandler(value = {ResourceNotFoundException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorMessageDto resourceNotFoundException(ResourceNotFoundException ex) {

        return ErrorMessageDto.builder()
                .source(GLOBAL)
                .message(ex.getMessage())
                .statusResponse(HttpStatus.NOT_FOUND.value())
                .build();
    }

    @ExceptionHandler(value = {PasswordMismatchException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessageDto passwordMismatchException(PasswordMismatchException ex) {

        return ErrorMessageDto.builder()
                .source(GLOBAL)
                .message(ex.getMessage())
                .statusResponse(HttpStatus.BAD_REQUEST.value())
                .build();
    }

    @ExceptionHandler(value = {AuthException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessageDto authException(AuthException ex) {

        return ErrorMessageDto.builder()
                .source("Auth")
                .message(ex.getMessage())
                .statusResponse(HttpStatus.BAD_REQUEST.value())
                .build();
    }

    @ExceptionHandler(value = {QRCodeException.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessageDto qrCodeException(QRCodeException ex) {

        return ErrorMessageDto.builder()
                .source("QRCode")
                .message(ex.getMessage())
                .statusResponse(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .build();
    }

    @ExceptionHandler(value = {LoginRedirectException.class})
    @ResponseStatus(value = HttpStatus.PERMANENT_REDIRECT)
    public ErrorMessageDto loginRedirectException(LoginRedirectException ex) {

        return ErrorMessageDto.builder()
                .source("Auth")
                .message(ex.getMessage())
                .statusResponse(HttpStatus.PERMANENT_REDIRECT.value())
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorMessageDto handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return ErrorMessageDto.builder()
                .statusResponse(HttpStatus.BAD_REQUEST.value())
                .message(toStringError(errors))
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public ErrorMessageDto constraintViolationException(
            ConstraintViolationException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getConstraintViolations().forEach(error -> {
            var fieldName = error.getPropertyPath().toString();
            String errorMessage = error.getMessage();
            errors.put(fieldName, errorMessage);
        });

        return ErrorMessageDto.builder()
                .statusResponse(HttpStatus.BAD_REQUEST.value())
                .message(toStringError(errors))
                .build();
    }

    private String toStringError(Map<String, String> errors) {
        var message = new StringBuilder();
        errors.forEach((field, value) -> {
            message.append(field);
            message.append(": ");
            message.append(value);
            message.append(", ");
        });
        return message.toString();
    }

}
