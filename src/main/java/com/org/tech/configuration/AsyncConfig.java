package com.org.tech.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
@EnableAsync
public class AsyncConfig {

    @Bean(name = "mailExecutor")
    public Executor mailExecutor() {
        return new ThreadPoolTaskExecutor();
    }

    @Bean(name = "assignJob")
    public Executor assignJobExecutor() {
        return new ThreadPoolTaskExecutor();
    }

    @Bean(name = "notification")
    public Executor notificationExecutor() {
        return new ThreadPoolTaskExecutor();
    }
}
