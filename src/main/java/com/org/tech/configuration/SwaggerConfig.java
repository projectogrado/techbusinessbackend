package com.org.tech.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.org.tech"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metadata());
    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("Software TechBusiness API")
                .description("TechBusiness")
                .version("0.0.1-SNAPSHOT")
                .license("Tech Business License")
                .contact(
                        new Contact(
                                "Ingrith Fernanda Santamaria Ballesteros & Johamn Sebastian Rincon Agredo",
                                "https://www.linkedin.com/in/johamn-sebastian-rinc%C3%B3n-agredo-7910781b0/",
                                "isantamaria@estudiante.uniajc.edu.co, jsrincon@estudiante.uniajc.edu.co"))
                .build();
    }


}
