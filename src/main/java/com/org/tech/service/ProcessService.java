package com.org.tech.service;


import com.org.tech.model.Process;

import java.util.List;

public interface ProcessService {

    Process create(Process processDTO);

    List<Process> getAllProcess();

    Process update(Process process);
}
