package com.org.tech.service;

import com.org.tech.model.Employee;

import java.util.List;
import java.util.Map;

public interface EmployeeService {

    Employee create(Employee employee);

    List<Employee> getAllEmployees(Map<String,String> params);

    Employee getEmployeeContext();

    Employee getEmployeeByUsername(String username);

    Employee getEmployeeLastUpdate(List<Long> ids);

    Employee update(Employee employee);

}
