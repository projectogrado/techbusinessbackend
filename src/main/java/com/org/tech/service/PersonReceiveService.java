package com.org.tech.service;

import com.org.tech.model.PersonReceive;

public interface PersonReceiveService {

    PersonReceive create(PersonReceive personReceive);
}
