package com.org.tech.service;

public interface AssignJobsService {

    void assignJobs(long admissionId);
}
