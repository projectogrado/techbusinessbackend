package com.org.tech.service.external.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.DestinationNotFoundException;
import com.org.tech.model.Employee;
import com.org.tech.model.Equipment;
import com.org.tech.model.dto.MailBodyDto;
import com.org.tech.model.dto.NotificationDto;
import com.org.tech.model.dto.PushNotificationDto;
import com.org.tech.providers.MessageProvider;
import com.org.tech.service.external.NotificationService;
import com.org.tech.utils.MailSenderUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class NotificationServiceImpl implements NotificationService {

    private final RestTemplate restTemplate = new RestTemplate();
    private final MessageProvider message;
    private final MailSenderUtil mailSenderUtil;

    @Value("${token.server.key}")
    private String tokenServer;
    @Value("${url.send.notification}")
    private String ulrNotification;

    @Autowired
    public NotificationServiceImpl(MessageProvider message, MailSenderUtil mailSenderUtil) {
        this.message = message;
        this.mailSenderUtil = mailSenderUtil;
    }

    @Override
    @Async("notification")
    public void sendNotificationByEmployee(Employee employee) {

        if (!employee.existTokenNotification()) {
            throw new DestinationNotFoundException(message.get(MessageEnum.DESTINATION_NOT_FOUND));
        }

        PushNotificationDto notification = PushNotificationDto.builder().to(employee.getTokenNotification()).build();
        sendNotification(notification);
    }

    @Override
    @Async("notification")
    public void sendNotificationForEmail(Equipment newEquipment) {
        var notificationMessage = message.get(MessageEnum.CHANGE_STATUS_EQUIPMENT, new Object[]{newEquipment.getStatus()});
        var body = MailBodyDto.builder()
                .content(notificationMessage)
                .email(newEquipment.getAdmissionDataSheet().getCustomer().getEmail())
                .subject(message.get(MessageEnum.SUBJECT_CHANGE_STATUS_EQUIPMENT))
                .build();

        mailSenderUtil.sendSimpleMail(body);
    }

    @Override
    public void sendAlertStatus(Equipment equipment) {
        var employee = equipment.getAdmissionDataSheet().getEmployee();
        if (employee.existTokenNotification()) {

            PushNotificationDto notification = PushNotificationDto.builder()
                    .notification(NotificationDto.builder()
                            .title(message.get(MessageEnum.TIME_EXCEEDED, new Object[]{equipment.getStatus()}))
                            .body(message.get(MessageEnum.TITLE_ALERT))
                            .build())
                    .to(employee.getTokenNotification()).build();

            sendNotification(notification);
        }
    }

    private void sendNotification(PushNotificationDto notification) {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", tokenServer);

        HttpEntity<PushNotificationDto> request = new HttpEntity<>(notification, headers);


        restTemplate.exchange(
                ulrNotification,
                HttpMethod.POST,
                request, String.class);
    }
}
