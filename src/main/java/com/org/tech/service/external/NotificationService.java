package com.org.tech.service.external;

import com.org.tech.model.Employee;
import com.org.tech.model.Equipment;

public interface NotificationService {

    void sendNotificationByEmployee(Employee employee);

    void sendNotificationForEmail(Equipment newEquipment);

    void sendAlertStatus(Equipment equipment);
}
