package com.org.tech.service;

import com.org.tech.model.Equipment;

import java.util.List;
import java.util.Map;

public interface EquipmentService {

    Equipment create(Equipment equipment);

    List<Equipment> getAllEquipment(Map<String, String> params);

    Equipment update(Equipment equipment);

    List<Equipment> getActiveEquipment();
}
