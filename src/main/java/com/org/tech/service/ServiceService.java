package com.org.tech.service;


import com.org.tech.model.Services;

import java.util.List;

public interface ServiceService {

    Services create(Services service);

    List<Services> getAllService();

    Services getServiceById(Long id);

    void delete(Long id);

    Services update(Services service);
}
