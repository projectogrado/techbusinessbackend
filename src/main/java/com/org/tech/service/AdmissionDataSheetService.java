package com.org.tech.service;

import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.enums.StatusEquipment;

public interface AdmissionDataSheetService {

    AdmissionDataSheet create(AdmissionDataSheet admissionDataSheet);

    AdmissionDataSheet getAdmissionDataSheetById(long id);

    void climbJob(long admissionDataSheetId);

    String terminateAdmission(long id , StatusEquipment status);

    String exportAdmissionPdf(long id);

    String sendPdf(long id);

}
