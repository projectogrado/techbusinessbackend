package com.org.tech.service;

import com.org.tech.model.Parameters;

public interface ParametersService {

    Parameters createParameters(Parameters parameters);

    Parameters getParameters();

    String getCron();

}
