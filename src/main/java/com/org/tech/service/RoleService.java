package com.org.tech.service;


import com.org.tech.model.Role;

import java.util.Set;

public interface RoleService {

    Set<Role> addRoleToUser(String name);
}
