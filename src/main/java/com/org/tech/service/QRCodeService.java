package com.org.tech.service;

public interface QRCodeService {

    String createQRCode(String content, int width, int height);
}
