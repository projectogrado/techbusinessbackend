package com.org.tech.service;

import com.org.tech.model.Administrator;

import java.util.List;

public interface AdministratorService {

    Administrator create(Administrator administrator);

    List<Administrator> getAllAdministrators();

    Administrator getAdministratorByUsername(String username);

    Administrator update(Administrator administrator);
}
