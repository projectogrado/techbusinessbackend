package com.org.tech.service;

import com.org.tech.model.Customer;

import java.util.List;

public interface CustomerService {

    Customer create(Customer customer);

    List<Customer> getAllCustomers();

    Customer getCustomerByDocument(String document);

    Customer update(Customer customer);
}
