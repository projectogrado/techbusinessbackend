package com.org.tech.service;

import com.org.tech.model.ChecklistRepair;
import com.org.tech.model.WorkService;

import java.util.List;

public interface WorkServiceService {

    WorkService create(WorkService workService);

    String addChecklistWorkService(List<ChecklistRepair> checklistRepairs, long id);

    List<WorkService> getAllWorkServiceByAdmission(long id);
}
