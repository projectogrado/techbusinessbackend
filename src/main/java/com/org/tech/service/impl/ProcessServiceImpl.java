package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.Process;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.ProcessRepository;
import com.org.tech.service.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProcessServiceImpl implements ProcessService {

    private final ProcessRepository processRepository;
    private final MessageProvider message;

    @Autowired
    public ProcessServiceImpl(ProcessRepository processRepository, MessageProvider message) {
        this.processRepository = processRepository;
        this.message = message;
    }

    @Override
    public Process create(Process process) {
        return processRepository.save(process);
    }

    @Override
    public List<Process> getAllProcess() {
        var sort = Sort.by("id");
        return processRepository.findAll(sort);
    }

    @Override
    public Process update(Process process) {
        if (!processRepository.existsById(process.getId())) {
            throw new ResourceNotFoundException(message.get(MessageEnum.PROCESS_NOT_FOUND));
        }
        return processRepository.save(process);
    }
}
