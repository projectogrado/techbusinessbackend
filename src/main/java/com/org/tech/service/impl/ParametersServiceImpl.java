package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.Parameters;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.ParametersRepository;
import com.org.tech.service.ParametersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParametersServiceImpl implements ParametersService {

    private final ParametersRepository parametersRepository;
    private final MessageProvider message;

    @Autowired
    public ParametersServiceImpl(ParametersRepository parametersRepository, MessageProvider message) {
        this.parametersRepository = parametersRepository;
        this.message = message;
    }

    @Override
    public Parameters createParameters(Parameters parameters) {
        return parametersRepository.save(parameters);
    }

    @Override
    public Parameters getParameters() {
        return parametersRepository.findTopByOrderByDateCreatedDesc()
                .orElseThrow(() -> new ResourceNotFoundException(message.get(MessageEnum.PARAMETERS_NOT_FOUND)));
    }

    @Override
    public String getCron() {
        return getParameters().getCronTask();
    }
}
