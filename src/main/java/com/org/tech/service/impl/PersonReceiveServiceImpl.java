package com.org.tech.service.impl;

import com.org.tech.model.PersonReceive;
import com.org.tech.repository.PersonReceiveRepository;
import com.org.tech.service.DischargeDataSheetService;
import com.org.tech.service.PersonReceiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonReceiveServiceImpl implements PersonReceiveService {

    private final PersonReceiveRepository personReceiveRepository;

    private final DischargeDataSheetService dischargeDataSheetService;

    @Autowired
    public PersonReceiveServiceImpl(PersonReceiveRepository personReceiveRepository,
                                    DischargeDataSheetService dischargeDataSheetService) {
        this.personReceiveRepository = personReceiveRepository;
        this.dischargeDataSheetService = dischargeDataSheetService;
    }

    @Override
    public PersonReceive create(PersonReceive personReceive) {

        var personPersisted = personReceiveRepository.save(personReceive);
        dischargeDataSheetService.closeAdmissionDataSheet(personReceive.getDischargeDataSheet().getId());
        return personPersisted;
    }
}
