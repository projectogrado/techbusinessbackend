package com.org.tech.service.impl;

import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.enums.StatusEquipment;
import com.org.tech.repository.AdmissionDataSheetRepository;
import com.org.tech.service.AssignJobsService;
import com.org.tech.service.BlacklistService;
import com.org.tech.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AssignJobsServiceImpl implements AssignJobsService {

    private final AdmissionDataSheetRepository admissionDataSheetRepository;
    private final EmployeeService employeeService;
    private final BlacklistService blacklistService;

    @Autowired
    public AssignJobsServiceImpl(AdmissionDataSheetRepository admissionDataSheetRepository,
                                 EmployeeService employeeService,
                                 BlacklistService blacklistService) {
        this.admissionDataSheetRepository = admissionDataSheetRepository;
        this.employeeService = employeeService;
        this.blacklistService = blacklistService;
    }

    @Override
    @Async("assignJob")
    public void assignJobs(long admissionId) {

        var admission = admissionDataSheetRepository.findById(admissionId)
                .orElseThrow(() -> new ResourceNotFoundException("message error"));

        var employee = employeeService.getEmployeeLastUpdate(blacklistService.getDiscardedEmployeeByAdmission(admissionId));
        admission.setEmployee(employee);
        admission.getEquipment().setStatus(StatusEquipment.TO_DO);
        admissionDataSheetRepository.save(admission);
        employeeService.update(employee);
    }

}
