package com.org.tech.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.Equipment;
import com.org.tech.model.enums.StatusEquipment;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.EquipmentRepository;
import com.org.tech.service.EquipmentService;
import com.org.tech.service.external.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EquipmentServiceImpl implements EquipmentService {

    private static final String SORT = "sort";
    private static final String DEFAULT_SORT = "id";
    private final EquipmentRepository equipmentRepository;
    private final ObjectMapper objectMapper;
    private final MessageProvider message;
    private final NotificationService notificationService;

    @Autowired
    public EquipmentServiceImpl(EquipmentRepository equipmentRepository,
                                ObjectMapper objectMapper,
                                MessageProvider message,
                                NotificationService notificationService) {
        this.equipmentRepository = equipmentRepository;
        this.objectMapper = objectMapper;
        this.message = message;
        this.notificationService = notificationService;
    }

    @Override
    public Equipment create(Equipment equipment) {
        return equipmentRepository.save(equipment);
    }

    @Override
    public List<Equipment> getAllEquipment(Map<String, String> params) {
        var sort = paramToSort(params);
        if (params.isEmpty()) {
            return equipmentRepository.findAll(sort);
        }
        return this.getAllFilter(params, sort);
    }

    @Override
    public Equipment update(Equipment equipment) {
        var oldEquipment = equipmentRepository.findById(equipment.getId())
                .orElseThrow(() -> new ResourceNotFoundException(message.get(MessageEnum.EQUIPMENT_NOT_FOUND)));
        sendNotificationChangeStatusEquipment(oldEquipment, equipment);
        return equipmentRepository.save(equipment);
    }

    @Override
    public List<Equipment> getActiveEquipment() {
        return equipmentRepository.findAllByAdmissionDataSheet_Status(false);
    }

    private void sendNotificationChangeStatusEquipment(Equipment old, Equipment newEquipment) {
        if (!old.getStatus().equals(newEquipment.getStatus())) {

            if (sendNotificationEmployee(newEquipment.getStatus())) {
                notificationService.sendNotificationByEmployee(newEquipment.getAdmissionDataSheet().getEmployee());
            }

            notificationService.sendNotificationForEmail(newEquipment);
        }
    }

    private boolean sendNotificationEmployee(StatusEquipment status) {
        switch (status) {
            case TO_DO:
            case NOVELTY:
                return true;
            default:
                return false;
        }
    }

    private List<Equipment> getAllFilter(Map<String, String> params, Sort sort) {
        var equipmentParams = objectMapper.convertValue(params, Equipment.class);
        var equipmentMatcher = ExampleMatcher.matching();
        Example<Equipment> equipmentExample = Example.of(equipmentParams, equipmentMatcher);
        return equipmentRepository.findAll(equipmentExample, sort);
    }

    private Sort paramToSort(Map<String, String> params) {
        String sort = params.getOrDefault(SORT, DEFAULT_SORT);
        params.remove(SORT);
        return Sort.by(sort);
    }
}
