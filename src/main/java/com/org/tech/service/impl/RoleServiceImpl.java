package com.org.tech.service.impl;

import com.org.tech.model.Role;
import com.org.tech.repository.RoleRepository;
import com.org.tech.service.RoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }


    @Override
    public Set<Role> addRoleToUser(String name) {
        Set<Role> roleSet = new HashSet<>();
        if (StringUtils.isNotBlank(name)) {
            roleSet.add(getRoleByName(name));
        }
        roleSet.add(getRoleByName("USER"));

        return roleSet;
    }

    private Role getRoleByName(String name) {
        return roleRepository.findByName(name);
    }
}
