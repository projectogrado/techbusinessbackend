package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.Services;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.ServiceRepository;
import com.org.tech.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceServiceImpl implements ServiceService {

    private final ServiceRepository serviceRepository;
    private final MessageProvider message;


    @Autowired
    public ServiceServiceImpl(ServiceRepository serviceRepository,
                              MessageProvider message) {
        this.serviceRepository = serviceRepository;
        this.message = message;
    }

    @Override
    public Services create(Services service) {
        return serviceRepository.save(service);
    }

    @Override
    public List<Services> getAllService() {
        return serviceRepository.findAll();
    }

    @Override
    public Services getServiceById(Long id) {
        return serviceRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(message.get(MessageEnum.SERVICE_NOT_FOUND)));
    }

    @Override
    public void delete(Long id) {
        if (!serviceRepository.existsById(id)) {
            throw new ResourceNotFoundException(message.get(MessageEnum.SERVICE_NOT_FOUND));
        }
        serviceRepository.deleteById(id);
    }

    @Override
    public Services update(Services service) {
        if (!serviceRepository.existsById(service.getId())) {
            throw new ResourceNotFoundException(message.get(MessageEnum.SERVICE_NOT_FOUND));
        }
        service.getRepairRoute().forEach(repairRoute -> repairRoute.setServices(service));
        return serviceRepository.save(service);
    }
}
