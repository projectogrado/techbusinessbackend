package com.org.tech.service.impl;

import com.org.tech.model.Equipment;
import com.org.tech.model.Parameters;
import com.org.tech.service.AlertService;
import com.org.tech.service.EquipmentService;
import com.org.tech.service.ParametersService;
import com.org.tech.service.external.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class AlertServiceImpl implements AlertService {
    private final EquipmentService equipmentService;
    private final NotificationService notificationService;
    private final ParametersService parametersService;

    @Autowired
    public AlertServiceImpl(NotificationService notificationService,
                            ParametersService parametersService,
                            EquipmentService equipmentService) {
        this.notificationService = notificationService;
        this.parametersService = parametersService;
        this.equipmentService = equipmentService;
    }

    @Override
    public void triggerAlertTime() {
        var parameters = parametersService.getParameters();
        List<Equipment> equipments = equipmentService.getActiveEquipment();
        equipments.forEach(equipment -> caseAlert(equipment, parameters));
    }

    private void caseAlert(Equipment equipment, Parameters parameters) {

        switch (equipment.getStatus()) {
            case TO_DO:
                validateStatus(equipment, parameters.getTimeToDo());
                break;
            case IN_REPAIR:
                validateStatus(equipment, parameters.getTimeInRepair());
                break;
            case NOT_REPAIRED:
                validateStatus(equipment, parameters.getTimeNoRepaired());
                break;
            case NOVELTY:
                validateStatus(equipment, parameters.getTimeNovelty());
                break;
            case REPAIRED:
                validateStatus(equipment, parameters.getTimeRepaired());
                break;
            default:
                log.trace(equipment.getStatus().toString());
                break;
        }
    }

    private void validateStatus(Equipment equipment, int ruleTime) {
        log.info("validando estado para alerta");
        LocalDateTime expectTime = equipment.getDateUpdatedStatus().plusHours(ruleTime);
        if (LocalDateTime.now().isAfter(expectTime)) {
            log.info("envio alerta");
            notificationService.sendAlertStatus(equipment);
        }
    }
}
