package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.PasswordMismatchException;
import com.org.tech.model.Administrator;
import com.org.tech.model.Employee;
import com.org.tech.model.dto.MailBodyDto;
import com.org.tech.model.dto.RecoverPasswordDto;
import com.org.tech.providers.MessageProvider;
import com.org.tech.service.AdministratorService;
import com.org.tech.service.EmployeeService;
import com.org.tech.service.PasswordResetService;
import com.org.tech.utils.GeneratePassword;
import com.org.tech.utils.MailSenderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordResetServiceImpl implements PasswordResetService {

    private final AdministratorService administratorService;
    private final EmployeeService employeeService;
    private final MessageProvider message;
    private final GeneratePassword generatePassword;
    private final PasswordEncoder passwordEncoder;
    private final MailSenderUtil mailSenderUtil;

    @Autowired
    public PasswordResetServiceImpl(AdministratorService administratorService, EmployeeService employeeService,
                                    MessageProvider message, GeneratePassword generatePassword, PasswordEncoder passwordEncoder,
                                    MailSenderUtil mailSenderUtil) {
        this.administratorService = administratorService;
        this.employeeService = employeeService;
        this.message = message;
        this.generatePassword = generatePassword;
        this.passwordEncoder = passwordEncoder;
        this.mailSenderUtil = mailSenderUtil;
    }

    @Override
    public String recoverPassword(String username) {
        String temporalPassword = generatePassword.generatePassword();

        var employee = employeeService.getEmployeeByUsername(username);

        if (null != employee) {
            employee.setPassword(encoderTemporalPassword(temporalPassword));
            employee.setIsTemporalPassword(true);
            employeeService.update(employee);
            sendMail(temporalPassword, employee.getEmail());
            return message.get(MessageEnum.RECOVERY_PASSWORD);
        }

        var administrator = administratorService.getAdministratorByUsername(username);

        if (null != administrator) {
            administrator.setPassword(encoderTemporalPassword(temporalPassword));
            administrator.setIsTemporalPassword(true);
            administratorService.update(administrator);
            sendMail(temporalPassword, administrator.getEmail());
            return message.get(MessageEnum.RECOVERY_PASSWORD);
        }

        throw new BadCredentialsException(message.get(MessageEnum.INVALID_LOGIN));
    }

    @Override
    public String setPassword(RecoverPasswordDto recoverPassword) {
        if (recoverPassword.getRol().equals("ADMIN")) {
            var admin = administratorService.getAdministratorByUsername(recoverPassword.getUsername());
            setPasswordAdministrator(recoverPassword, admin);
        } else {
            var employee = employeeService.getEmployeeByUsername(recoverPassword.getUsername());
            setPasswordEmployee(recoverPassword, employee);
        }
        return message.get(MessageEnum.RECOVER_PASSWORD);
    }

    private void setPasswordAdministrator(RecoverPasswordDto recoverPassword, Administrator administrator) {
        if (!passwordEncoder.matches(recoverPassword.getOldPassword(), administrator.getPassword())) {
            throw new PasswordMismatchException(message.get(MessageEnum.PASSWORD_MISMATCH));
        }
        administrator.setPassword(passwordEncoder.encode(recoverPassword.getPassword()));
        administrator.setIsTemporalPassword(false);
        administratorService.update(administrator);
    }

    private void setPasswordEmployee(RecoverPasswordDto recoverPassword, Employee employee) {
        if (!passwordEncoder.matches(recoverPassword.getOldPassword(), employee.getPassword())) {
            throw new PasswordMismatchException(message.get(MessageEnum.PASSWORD_MISMATCH));
        }
        employee.setPassword(passwordEncoder.encode(recoverPassword.getPassword()));
        employee.setIsTemporalPassword(false);
        employeeService.update(employee);
    }


    private String encoderTemporalPassword(String password) {
        return passwordEncoder.encode(password);
    }

    private void sendMail(String content, String email) {
        MailBodyDto body = MailBodyDto.builder()
                .content(content)
                .email(email)
                .subject(message.get(MessageEnum.SUBJECT_RECOVER_PASSWORD))
                .build();
        mailSenderUtil.sendSimpleMail(body);
    }
}
