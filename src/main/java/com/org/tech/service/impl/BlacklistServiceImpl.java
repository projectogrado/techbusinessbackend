package com.org.tech.service.impl;

import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.Blacklist;
import com.org.tech.model.keys.BlacklistKey;
import com.org.tech.repository.BlacklistRepository;
import com.org.tech.service.BlacklistService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BlacklistServiceImpl implements BlacklistService {

    private final BlacklistRepository blacklistRepository;

    public BlacklistServiceImpl(BlacklistRepository blacklistRepository) {
        this.blacklistRepository = blacklistRepository;
    }

    @Override
    public void create(AdmissionDataSheet admissionId) {

        var key = new BlacklistKey();
        key.setEmployeeId(admissionId.getEmployee().getId());
        key.setAdmissionId(admissionId.getId());

        var blacklist = Blacklist.builder()
                .id(key)
                .reason("Change")
                .admissionDataSheet(admissionId)
                .employee(admissionId.getEmployee())
                .build();

        blacklistRepository.save(blacklist);
    }

    @Override
    public List<Long> getDiscardedEmployeeByAdmission(long admissionId) {
        return blacklistRepository.findAllByIdAdmissionId(admissionId)
                .stream()
                .map(blacklist -> blacklist.getId().getEmployeeId())
                .collect(Collectors.toList());
    }
}
