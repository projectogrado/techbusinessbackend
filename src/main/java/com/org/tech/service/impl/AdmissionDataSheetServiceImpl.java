package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.WorkService;
import com.org.tech.model.dto.MailBodyDto;
import com.org.tech.model.enums.StatusEquipment;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.AdmissionDataSheetRepository;
import com.org.tech.service.AdmissionDataSheetService;
import com.org.tech.service.AssignJobsService;
import com.org.tech.service.BlacklistService;
import com.org.tech.service.QRCodeService;
import com.org.tech.service.ServiceService;
import com.org.tech.service.WorkServiceService;
import com.org.tech.utils.MailSenderUtil;
import com.org.tech.utils.PdfUtil;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;

@Service
public class AdmissionDataSheetServiceImpl implements AdmissionDataSheetService {

    private final AssignJobsService assignJobsService;
    private final AdmissionDataSheetRepository admissionRepository;
    private final BlacklistService blacklistService;
    private final MessageProvider message;
    private final MailSenderUtil mailSenderUtil;
    private final PdfUtil pdfUtil;
    private final QRCodeService qrCodeService;
    private final WorkServiceService workServiceService;
    private final ServiceService serviceService;
    @Value("${path.source}")
    private String dir;

    @Autowired
    public AdmissionDataSheetServiceImpl(AdmissionDataSheetRepository admissionRepository,
                                         AssignJobsService assignJobsService,
                                         BlacklistService blacklistService,
                                         MailSenderUtil mailSenderUtil,
                                         MessageProvider message,
                                         PdfUtil pdfUtil,
                                         QRCodeService qrCodeService,
                                         WorkServiceService workServiceService, ServiceService serviceService) {
        this.admissionRepository = admissionRepository;
        this.assignJobsService = assignJobsService;
        this.blacklistService = blacklistService;
        this.mailSenderUtil = mailSenderUtil;
        this.message = message;
        this.pdfUtil = pdfUtil;
        this.qrCodeService = qrCodeService;
        this.workServiceService = workServiceService;
        this.serviceService = serviceService;
    }

    @Override
    public AdmissionDataSheet create(AdmissionDataSheet admissionDataSheet) {
        var admission = admissionRepository.save(admissionDataSheet);
        workServiceService.create(initialServiceReview(admissionDataSheet));
        assignJobsService.assignJobs(admission.getId());
        return admission;
    }

    private WorkService initialServiceReview(AdmissionDataSheet admissionDataSheet) {
        return WorkService.builder()
                .admissionDataSheet(admissionDataSheet)
                .services(serviceService.getServiceById(1001L))
                .build();
    }

    @Override
    public AdmissionDataSheet getAdmissionDataSheetById(long id) {
        return admissionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(message.get(MessageEnum.ADMISSION_NOT_FOUND)));
    }

    @Override
    public void climbJob(long admissionDataSheetId) {

        var admission = getAdmissionDataSheetById(admissionDataSheetId);
        blacklistService.create(admission);
        admission.setEmployee(null);

        admissionRepository.save(admission);
        assignJobsService.assignJobs(admissionDataSheetId);
    }

    @Override
    public String terminateAdmission(long id, StatusEquipment status) {
        var admission = getAdmissionDataSheetById(id);
        admission.getEquipment().setStatus(status);
        admissionRepository.save(admission);
        return "Update status Ok";
    }

    @Override
    public String exportAdmissionPdf(long id) {
        try {
            var data = setValueForPdf(id);
            var file = pdfUtil.renderPdf(data, dir);
            byte[] inFileBytes = FileUtils.readFileToByteArray(file);
            return Base64.getEncoder().encodeToString(inFileBytes);
        } catch (IOException e) {
            throw new ResourceNotFoundException("Error file");
        }
    }

    @Override
    public String sendPdf(long id) {
        var admissionDataSheet = getAdmissionDataSheetById(id);
        var notificationMessage = message.get(MessageEnum.SEND_PDF_ADMISSION, new Object[]{id});
        var data = setValueForPdf(id);
        var file = pdfUtil.renderPdf(data, dir);
        var body = MailBodyDto.builder()
                .content("")
                .email(admissionDataSheet.getCustomer().getEmail())
                .subject(notificationMessage)
                .build();
        mailSenderUtil.sendEmailWithAttachment(body, file);
        return message.get(MessageEnum.SUCCESS_SEND);
    }


    private Context setValueForPdf(long id) {
        var context = new Context();
        var admissionDataSheet = getAdmissionDataSheetById(id);
        context.setVariable("admission", admissionDataSheet);
        context.setVariable("customer", admissionDataSheet.getEquipment().getAdmissionDataSheet().getCustomer());
        context.setVariable("equipment", admissionDataSheet.getEquipment());
        context.setVariable("qr", generateQr(id).getAbsolutePath());
        return context;
    }

    @SneakyThrows
    private File generateQr(long id) {
        String base = "http://ec2-18-221-50-26.us-east-2.compute.amazonaws.com:4000/discharge-data-sheet/" + id;
        String base64 = qrCodeService.createQRCode(base, 200, 200);
        var tempFile = File.createTempFile("qr_code", ".png", new File(dir));
        byte[] data = Base64.getDecoder().decode(base64);
        try (OutputStream stream = new FileOutputStream(tempFile)) {
            stream.write(data);
            tempFile.deleteOnExit();
        }
        return tempFile;
    }
}
