package com.org.tech.service.impl;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.QRCodeException;
import com.org.tech.providers.MessageProvider;
import com.org.tech.service.QRCodeService;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.EnumMap;

@Service
public class QRCodeServiceImpl implements QRCodeService {

    public static final String FORMAT = "utf-8";
    public static final String EXTENSION = "png";
    private final MessageProvider message;

    public QRCodeServiceImpl(MessageProvider message) {
        this.message = message;
    }

    @Override
    @SneakyThrows
    public String createQRCode(String content, int width, int height) {
        if (StringUtils.isNotBlank(content)) {
            var os = new ByteArrayOutputStream();

            @SuppressWarnings("rawtypes")
            EnumMap<EncodeHintType, Comparable> hints = new EnumMap<>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, FORMAT);
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
            hints.put(EncodeHintType.MARGIN, 2);

            var writer = new QRCodeWriter();
            var bitMatrix = writer
                    .encode(content, BarcodeFormat.QR_CODE, width, height, hints);

            var bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
            ImageIO.write(bufferedImage, EXTENSION, Base64.getEncoder().wrap(os));

            return os.toString(StandardCharsets.ISO_8859_1.name());

        }
        throw new QRCodeException(message.get(MessageEnum.NO_CONTENT));
    }
}
