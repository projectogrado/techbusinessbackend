package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.Customer;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.CustomerRepository;
import com.org.tech.service.CustomerService;
import com.org.tech.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final RoleService roleService;
    private final MessageProvider message;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, RoleService roleService,
                               MessageProvider message) {
        this.customerRepository = customerRepository;
        this.roleService = roleService;
        this.message = message;
    }

    @Override
    public Customer create(Customer customer) {
        customer.setRoles(roleService.addRoleToUser(null));
        return customerRepository.save(customer);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public Customer getCustomerByDocument(String document) {
        return customerRepository.findCustomerByDocument(document)
                .orElseThrow(() -> new ResourceNotFoundException(message.get(MessageEnum.CUSTOMER_NOT_FOUND)));
    }

    @Override
    public Customer update(Customer customer) {
        if (customerRepository.existsById(customer.getId())) {
            return customerRepository.save(customer);
        }
        throw new ResourceNotFoundException(message.get(MessageEnum.CUSTOMER_NOT_FOUND));
    }
}
