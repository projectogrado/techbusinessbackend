package com.org.tech.service.impl;

import com.org.tech.model.ChecklistRepair;
import com.org.tech.repository.ChecklistRepairRepository;
import com.org.tech.service.ChecklistRepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChecklistRepairServiceImpl implements ChecklistRepairService {

    private final ChecklistRepairRepository checklistRepairRepository;

    @Autowired
    public ChecklistRepairServiceImpl(ChecklistRepairRepository checklistRepairRepository) {
        this.checklistRepairRepository = checklistRepairRepository;
    }

    @Override
    public List<ChecklistRepair> saveAllChecklistRepair(List<ChecklistRepair> checklistRepairs) {
        return checklistRepairRepository.saveAll(checklistRepairs);
    }
}
