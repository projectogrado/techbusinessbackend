package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.Administrator;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.AdministratorRepository;
import com.org.tech.service.AdministratorService;
import com.org.tech.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdministratorServiceImpl implements AdministratorService {

    private final AdministratorRepository administratorRepository;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;
    private final MessageProvider message;


    @Autowired
    public AdministratorServiceImpl(
            AdministratorRepository administratorRepository, RoleService roleService,
            PasswordEncoder passwordEncoder, MessageProvider message) {
        this.administratorRepository = administratorRepository;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
        this.message = message;
    }

    @Override
    public Administrator create(Administrator administrator) {
        setDefaultFieldAdministrator(administrator);
        return administratorRepository.save(administrator);
    }

    @Override
    public List<Administrator> getAllAdministrators() {
        return administratorRepository.findAll();
    }

    @Override
    public Administrator getAdministratorByUsername(String username) {
        return administratorRepository.findByUsername(username).orElse(null);
    }

    @Override
    public Administrator update(Administrator administrator) {
        if (administratorRepository.existsById(administrator.getId())) {
            return administratorRepository.save(administrator);
        }
        throw new ResourceNotFoundException(message.get(MessageEnum.ADMINISTRATOR_NOT_FOUND));
    }

    private void setDefaultFieldAdministrator(Administrator administrator) {
        administrator.setRoles(roleService.addRoleToUser("ADMIN"));
        administrator.setPassword(passwordEncoder.encode(administrator.getPassword()));
    }
}
