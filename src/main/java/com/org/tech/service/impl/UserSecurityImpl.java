package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.LoginRedirectException;
import com.org.tech.model.UserApp;
import com.org.tech.providers.MessageProvider;
import com.org.tech.service.AdministratorService;
import com.org.tech.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service(value = "userService")
public class UserSecurityImpl implements UserDetailsService {

    private final EmployeeService employeeService;
    private final AdministratorService administratorService;
    private final MessageProvider message;


    @Autowired
    public UserSecurityImpl(EmployeeService employeeService,
                            AdministratorService administratorService, MessageProvider message) {
        this.employeeService = employeeService;
        this.administratorService = administratorService;
        this.message = message;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User employee = loginEmployee(username);
        if (employee != null) {
            return employee;
        }

        User administrator = loginAdmin(username);
        if (administrator != null) {
            return administrator;
        }

        throw new UsernameNotFoundException(message.get(MessageEnum.INVALID_LOGIN));
    }

    private User loginEmployee(String username) {
        var employee = employeeService.getEmployeeByUsername(username);

        if (null == employee) {
            return null;
        }

        return new User(employee.getUsername(), employee.getPassword(), getAuthority(employee));
    }

    private User loginAdmin(String username) {
        var administrator = administratorService.getAdministratorByUsername(username);

        if (null == administrator) {
            return null;
        }

        return new User(administrator.getUsername(), administrator.getPassword(),
                getAuthority(administrator));
    }

    public void isTemporalPassword(String username) {
        var administrator = administratorService.getAdministratorByUsername(username);
        if (null != administrator && administrator.getIsTemporalPassword()) {
            throw new LoginRedirectException(message.get(MessageEnum.ROLE_ADMIN));
        }

        var employee = employeeService.getEmployeeByUsername(username);
        if (null != employee && employee.getIsTemporalPassword()) {
            throw new LoginRedirectException(message.get(MessageEnum.ROLE_EMPLOYEE));
        }
    }

    private Set<SimpleGrantedAuthority> getAuthority(UserApp user) {

        return user.getRoles()
                .stream()
                .map(role -> new SimpleGrantedAuthority("ROLE_" + role.getName()))
                .collect(Collectors.toSet());

    }
}
