package com.org.tech.service.impl;

import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.DischargeDataSheet;
import com.org.tech.model.enums.StatusEquipment;
import com.org.tech.repository.DischargeDataSheetRepository;
import com.org.tech.service.AdmissionDataSheetService;
import com.org.tech.service.DischargeDataSheetService;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class DischargeDataSheetServiceImpl implements DischargeDataSheetService {

    private final DischargeDataSheetRepository dischargeDataSheetRepository;
    private final AdmissionDataSheetService admissionDataSheetService;

    public DischargeDataSheetServiceImpl(
            DischargeDataSheetRepository dischargeDataSheetRepository, AdmissionDataSheetService admissionDataSheetService) {
        this.dischargeDataSheetRepository = dischargeDataSheetRepository;
        this.admissionDataSheetService = admissionDataSheetService;
    }

    @Override
    public DischargeDataSheet getDischargeDataSheetById(long id) {
        return dischargeDataSheetRepository.findByAdmissionDataSheet_Id(id)
                .orElseGet(() -> createDischarge(id));
    }

    @Override
    public void closeAdmissionDataSheet(long id) {
        var dischargeDataSheet = dischargeDataSheetRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("no existe"));

        dischargeDataSheet.getAdmissionDataSheet().getEquipment().setStatus(StatusEquipment.DELIVERED);

        dischargeDataSheetRepository.save(dischargeDataSheet);

    }

    private DischargeDataSheet createDischarge(long id) {
        var dischargeDataSheet = DischargeDataSheet.builder()
                .deliverDate(new Date())
                .admissionDataSheet(admissionDataSheetService.getAdmissionDataSheetById(id))
                .build();

        return dischargeDataSheetRepository.save(dischargeDataSheet);
    }
}
