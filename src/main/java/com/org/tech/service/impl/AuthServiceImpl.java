package com.org.tech.service.impl;


import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.AuthException;
import com.org.tech.model.dto.JwtResponseDTO;
import com.org.tech.model.dto.LoginDto;
import com.org.tech.providers.MessageProvider;
import com.org.tech.security.JwtToken;
import com.org.tech.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AuthServiceImpl implements AuthService {

    private final JwtToken jwtToken;
    private final AuthenticationManager authenticationManager;
    private final MessageProvider message;

    @Autowired
    public AuthServiceImpl(JwtToken jwtToken, AuthenticationManager authenticationManager,
                           MessageProvider message) {
        this.jwtToken = jwtToken;
        this.authenticationManager = authenticationManager;
        this.message = message;
    }

    @Transactional
    @Override
    public JwtResponseDTO authentication(LoginDto login) {

        Authentication auth = authenticate(login.getUsername(), login.getPassword());
        SecurityContextHolder.getContext().setAuthentication(auth);

        return new JwtResponseDTO(jwtToken.generateToken(auth));
    }


    private Authentication authenticate(String username, String password) {

        try {
            return authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthException(
                    message.get(MessageEnum.USER_DISABLE), e.getCause());

        } catch (BadCredentialsException e) {
            throw new AuthException(
                    message.get(MessageEnum.INVALID_CREDENTIAL), e.getCause());
        }
    }
}
