package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.ChecklistRepair;
import com.org.tech.model.WorkService;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.WorkServiceRepository;
import com.org.tech.service.ChecklistRepairService;
import com.org.tech.service.RepairRouteService;
import com.org.tech.service.WorkServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WorkServiceServiceImpl implements WorkServiceService {

    private final WorkServiceRepository workServiceRepository;
    private final ChecklistRepairService checklistRepairService;
    private final RepairRouteService repairRouteService;
    private final MessageProvider message;

    @Autowired
    public WorkServiceServiceImpl(WorkServiceRepository workServiceRepository,
                                  ChecklistRepairService checklistRepairService, RepairRouteService repairRouteService, MessageProvider message) {
        this.workServiceRepository = workServiceRepository;
        this.checklistRepairService = checklistRepairService;
        this.repairRouteService = repairRouteService;
        this.message = message;
    }

    @Override
    public WorkService create(WorkService workService) {
        return workServiceRepository.save(workService);
    }

    @Override
    public String addChecklistWorkService(List<ChecklistRepair> checklistRepairs, long id) {

        var workService = getWorkServiceById(id);

        List<ChecklistRepair> checklistSaved = checklistRepairService
                .saveAllChecklistRepair(checklistRepairs.stream()
                        .map(checklistRepair -> builderData(checklistRepair, workService))
                        .collect(Collectors.toList()));

        workService.setChecklistRepairs(checklistSaved);

        workServiceRepository.save(workService);

        return message.get(MessageEnum.SUCCESS_SAVE);
    }

    @Override
    public List<WorkService> getAllWorkServiceByAdmission(long id) {
        return workServiceRepository.findAllByAdmissionDataSheet_Id(id);
    }

    private ChecklistRepair builderData(ChecklistRepair checklistRepair, WorkService workService) {
        checklistRepair.setRepairRoute(repairRouteService.getRepairRouteById(checklistRepair.getId().getRepairRouteId()));
        checklistRepair.setWorkService(workService);

        return checklistRepair;
    }

    private WorkService getWorkServiceById(long id) {
        return workServiceRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(message.get(MessageEnum.WORK_SERVICE_NOT_FOUND)));

    }
}
