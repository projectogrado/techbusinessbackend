package com.org.tech.service.impl;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.RepairRoute;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.RepairRouteRepository;
import com.org.tech.service.RepairRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepairRouteServiceImpl implements RepairRouteService {

    private final RepairRouteRepository repairRouteRepository;
    private final MessageProvider message;

    @Autowired
    public RepairRouteServiceImpl(RepairRouteRepository repairRouteRepository, MessageProvider message) {
        this.repairRouteRepository = repairRouteRepository;
        this.message = message;
    }

    @Override
    public RepairRoute getRepairRouteById(long id) {
        return repairRouteRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(message.get(MessageEnum.WORK_ROUTE_NOT_FOUND)));
    }

    @Override
    public List<RepairRoute> saveAllRepair(List<RepairRoute> repairRoutes) {
        return repairRouteRepository.saveAll(repairRoutes);
    }
}
