package com.org.tech.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.ResourceNotFoundException;
import com.org.tech.model.Employee;
import com.org.tech.providers.MessageProvider;
import com.org.tech.repository.EmployeeRepository;
import com.org.tech.service.EmployeeService;
import com.org.tech.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;
    private final MessageProvider message;

    @Autowired
    public EmployeeServiceImpl(
            EmployeeRepository employeeRepository,
            PasswordEncoder passwordEncoder,
            RoleService roleService,
            MessageProvider message, ObjectMapper objectMapper) {
        this.employeeRepository = employeeRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleService = roleService;
        this.message = message;
    }

    @Override
    public Employee create(Employee employee) {
        setDefaultFieldEmployee(employee);
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> getAllEmployees(Map<String, String> params) {
        var sort = Sort.by("id");
        if (params.size() == 0) {
            return employeeRepository.findAll(sort);
        }
        String documentParams = params.get("document");
        return employeeRepository.findAllByDocument(documentParams, sort);
    }

    @Override
    public Employee getEmployeeContext() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        return employeeRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException(message.get(MessageEnum.EMPLOYEE_NOT_FOUND)));
    }

    @Override
    public Employee getEmployeeByUsername(String username) {
        return employeeRepository.findByUsername(username).orElse(null);
    }

    @Override
    public Employee getEmployeeLastUpdate(List<Long> ids) {
        return ids.isEmpty()
                ? employeeRepository.findFirstByOrderByUpdatedDateTimeAsc()
                .orElseThrow(() -> new ResourceNotFoundException(message.get(MessageEnum.EMPLOYEE_NOT_FOUND)))

                : employeeRepository.findFirstByIdNotInOrderByUpdatedDateTimeAsc(ids)
                .orElseThrow(() -> new ResourceNotFoundException(message.get(MessageEnum.EMPLOYEE_NOT_FOUND)));
    }

    @Override
    public Employee update(Employee employee) {
        if (employeeRepository.existsById(employee.getId())) {
            employee.setUpdatedDateTime(new Date());
            return employeeRepository.save(employee);
        }
        throw new ResourceNotFoundException(message.get(MessageEnum.EMPLOYEE_NOT_FOUND));
    }


    private void setDefaultFieldEmployee(Employee employee) {
        employee.setRoles(roleService.addRoleToUser("EMPLOYEE"));
        employee.setIsTemporalPassword(false);
        employee.setUpdatedDateTime(new Date());
        employee.setPassword(passwordEncoder.encode(employee.getPassword()));
    }
}
