package com.org.tech.service;

import com.org.tech.model.ChecklistRepair;

import java.util.List;

public interface ChecklistRepairService {

    List<ChecklistRepair> saveAllChecklistRepair(List<ChecklistRepair> checklistRepairs);

}
