package com.org.tech.service;

import com.org.tech.model.AdmissionDataSheet;

import java.util.List;

public interface BlacklistService {

    void create(AdmissionDataSheet admissionId);

    List<Long> getDiscardedEmployeeByAdmission(long admissionId);
}
