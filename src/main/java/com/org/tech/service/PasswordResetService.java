package com.org.tech.service;

import com.org.tech.model.dto.RecoverPasswordDto;

public interface PasswordResetService {

    String recoverPassword(String username);

    String setPassword(RecoverPasswordDto password);
}
