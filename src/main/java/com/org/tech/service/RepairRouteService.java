package com.org.tech.service;

import com.org.tech.model.RepairRoute;

import java.util.List;

public interface RepairRouteService {

    RepairRoute getRepairRouteById(long id);

    List<RepairRoute> saveAllRepair(List<RepairRoute> repairRoutes);
}
