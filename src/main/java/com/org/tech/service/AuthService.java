package com.org.tech.service;


import com.org.tech.model.dto.JwtResponseDTO;
import com.org.tech.model.dto.LoginDto;

public interface AuthService {

    JwtResponseDTO authentication(LoginDto login);
}
