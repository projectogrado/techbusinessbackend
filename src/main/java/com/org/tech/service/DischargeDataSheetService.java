package com.org.tech.service;

import com.org.tech.model.DischargeDataSheet;

public interface DischargeDataSheetService {

    DischargeDataSheet getDischargeDataSheetById(long id);

    void closeAdmissionDataSheet(long id);

}
