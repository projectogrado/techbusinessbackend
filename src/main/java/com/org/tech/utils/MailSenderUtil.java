package com.org.tech.utils;

import com.org.tech.constants.MessageEnum;
import com.org.tech.exception.MessageException;
import com.org.tech.model.dto.MailBodyDto;
import com.org.tech.providers.MessageProvider;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.mail.MessagingException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class MailSenderUtil {

    private final MessageProvider message;
    private final JavaMailSender mailSender;
    private final FreeMarkerConfigurer freeMarkerConfigurer;


    @Autowired
    public MailSenderUtil(JavaMailSender mailSender, MessageProvider message,
                          FreeMarkerConfigurer freeMarkerConfigurer) {
        this.mailSender = mailSender;
        this.message = message;
        this.freeMarkerConfigurer = freeMarkerConfigurer;
    }

    @Async("mailExecutor")
    public void sendEmailWithAttachment(MailBodyDto mail, File file) {
        var nameFile = "admission.pdf";
        var mimeMessage = mailSender.createMimeMessage();
        try {
            var messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setTo(mail.getEmail());
            messageHelper.setText(templateEmail(mail.getContent(),"template-email-send-pdf.ftl"), true);
            messageHelper.setSubject(mail.getSubject());
            messageHelper.addAttachment(nameFile, file);
        } catch (MessagingException e) {
            throw new MessageException(message.get(MessageEnum.FAIL_SEND_MAIL));
        }
        mailSender.send(mimeMessage);
    }

    @Async("mailExecutor")
    public void sendSimpleMail(MailBodyDto mail) {
        var mimeMessage = mailSender.createMimeMessage();
        var messageHelper = new MimeMessageHelper(mimeMessage);
        try {
            messageHelper.setTo(mail.getEmail());
            messageHelper.setText(this.templateEmail(mail.getContent(),"template-email.ftl"), true);
            messageHelper.setSubject(mail.getSubject());
        } catch (MessagingException e) {
            throw new MessageException(message.get(MessageEnum.FAIL_SEND_MAIL));
        }
        mailSender.send(mimeMessage);
    }

    private String templateEmail(String content,String templates) {
        try {
            Map<String, Object> data = new HashMap<>();
            data.put("content", content);
            var template = freeMarkerConfigurer.createConfiguration()
                    .getTemplate(templates);
            return FreeMarkerTemplateUtils.processTemplateIntoString(template, data);
        } catch (IOException | TemplateException e) {
            throw new MessageException(message.get(MessageEnum.FAIL_GET_TEMPLATE));
        }
    }


}
