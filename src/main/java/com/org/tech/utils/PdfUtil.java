package com.org.tech.utils;

import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Component
public class PdfUtil {
    public static final String EXTENSION_PDF = ".pdf";
    private static final String PDF_RESOURCES = "/pdf-resources/";
    private static final String ERROR_FILE_NOT_FOUND = "File Not Found";

    private final SpringTemplateEngine templateEngine;

    @Autowired
    public PdfUtil(SpringTemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public File renderPdf(Context context, String dirFile) {

        String html = loadAndFillTemplate(context);
        var name = "admission";
        try {
            var file = File.createTempFile(name, EXTENSION_PDF, new File(dirFile));
            generatePdf(html, file);
            return file;
        } catch (IOException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, ERROR_FILE_NOT_FOUND, e);
        }
    }

    private void generatePdf(String html, File file) {
        try (OutputStream outputStream = new FileOutputStream(file)) {
            var renderer = new ITextRenderer(20f * 4f / 3f, 20);
            renderer.setDocumentFromString(html, new ClassPathResource(PDF_RESOURCES).getURL().toExternalForm());
            renderer.layout();
            renderer.createPDF(outputStream);
            file.deleteOnExit();
        } catch (IOException | DocumentException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, ERROR_FILE_NOT_FOUND, e);
        }
    }

    private String loadAndFillTemplate(Context context) {
        return templateEngine.process("pdf-admission", context);
    }
}
