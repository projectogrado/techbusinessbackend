package com.org.tech.task;

import com.org.tech.service.AlertService;
import com.org.tech.service.ParametersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
@EnableScheduling
public class DynamicScheduleConfig implements SchedulingConfigurer {

    private final AlertService alertService;
    private final ParametersService parametersService;

    @Autowired
    public DynamicScheduleConfig(AlertService alertService, ParametersService parametersService) {
        this.alertService = alertService;
        this.parametersService = parametersService;
    }

    @Bean
    public Executor taskExecutor() {
        return Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(taskExecutor());
        taskRegistrar.addCronTask(
                alertService::triggerAlertTime,
                parametersService.getCron()
        );
    }
}
