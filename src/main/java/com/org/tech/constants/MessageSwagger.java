package com.org.tech.constants;

public final class MessageSwagger {
    public static final String SOMETHING_WRONG = "Something went wrong";
    public static final String INTERNAL_ERROR = "Internal Server Error";
    public static final String NOT_FOUND = "Not Found Resource";

    private MessageSwagger() {
        throw new IllegalStateException("Utility class");
    }
}
