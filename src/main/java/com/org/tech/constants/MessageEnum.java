package com.org.tech.constants;

public enum MessageEnum {
    USER_DISABLE("user.disable"),
    INVALID_CREDENTIAL("invalid.credential"),
    INVALID_LOGIN("invalid.login"),
    SERVICE_NOT_FOUND("service.not.found"),
    PROCESS_NOT_FOUND("process.not.found"),
    WORK_ROUTE_NOT_FOUND("work.route.not.found"),
    WORK_SERVICE_NOT_FOUND("work.service.not.found"),
    EQUIPMENT_NOT_FOUND("equipment.not.found"),
    EMPLOYEE_NOT_FOUND("employee.not.found"),
    CUSTOMER_NOT_FOUND("customer.not.found"),
    ADMINISTRATOR_NOT_FOUND("administrator.not.found"),
    DESTINATION_NOT_FOUND("destination.not.found"),
    DISCHARGE_NOT_FOUND("discharge.not.found"),
    ADMISSION_NOT_FOUND("admission.not.found"),
    PARAMETERS_NOT_FOUND("parameters.not.found"),
    TIME_EXCEEDED("time.exceeded"),
    TITLE_ALERT("title.alert"),
    ROLES_NOT_FOUND("roles.not.found"),
    QR_ERROR("qr.error"),
    NO_CONTENT("no.content"),
    RECOVERY_PASSWORD("recovery.password"),
    SUBJECT_RECOVER_PASSWORD("subject.recover.password"),
    RECOVER_PASSWORD("recover.password"),
    PASSWORD_MISMATCH("password.mismatch"),
    FAIL_SEND_MAIL("failed.send.mail"),
    FAIL_GET_TEMPLATE("failed.get.template.email"),
    IS_TEMPORAL("is.temporal"),
    ROLE_ADMIN("role.admin"),
    ROLE_EMPLOYEE("role.employee"),
    CHANGE_STATUS_EQUIPMENT("change.status.equipment"),
    SUBJECT_CHANGE_STATUS_EQUIPMENT("subject.change.status.equipment"),
    SEND_PDF_ADMISSION("send.pdf.admission"),
    SUCCESS_SEND("success.send"),
    SUCCESS_SAVE("success.save");


    private final String code;

    MessageEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
