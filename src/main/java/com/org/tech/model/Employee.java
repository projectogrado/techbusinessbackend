package com.org.tech.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.org.tech.model.validation.anotation.ValidPassword;
import com.org.tech.model.validation.group.OnCreate;
import lombok.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "employee")
@PrimaryKeyJoinColumn(name = "uid")
public class Employee extends UserApp {

    @Column
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @Column
    @NotBlank
    @ValidPassword(groups = {OnCreate.class})
    private String password;

    @Column
    private boolean status;

    @Column
    private Boolean isTemporalPassword;

    @Column
    private String tokenNotification;

    @Column
    private Date updatedDateTime;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "employee")
    @JsonIgnoreProperties(value = "employee")
    private List<AdmissionDataSheet> works = new ArrayList<>();

    public boolean existTokenNotification() {
        return null != tokenNotification;
    }

    @Override
    @Generated
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var employee = (Employee) o;

        return getId() != null && getId().equals(employee.getId());
    }

    @Override
    @Generated
    public int hashCode() {
        return 949447908;
    }
}
