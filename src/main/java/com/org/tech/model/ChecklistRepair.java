package com.org.tech.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.org.tech.model.keys.ChecklistRepairKey;
import lombok.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Table(name = "checklist_repair")
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class ChecklistRepair {

    @EmbeddedId
    private ChecklistRepairKey id;

    @ManyToOne
    @MapsId("repairRouteId")
    @JoinColumn(name = "repair_route_id")
    @JsonIgnoreProperties(value = "services")
    private RepairRoute repairRoute;


    @ManyToOne
    @MapsId("workServiceId")
    @JoinColumn(name = "work_service_id")
    @JsonIgnoreProperties(value = "checklistRepairs")
    private WorkService workService;

    @Column
    private boolean status;

    @Column
    private String evidence;

    @Override
    @Generated
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ChecklistRepair that = (ChecklistRepair) o;

        return id != null && id.equals(that.id);
    }

    @Override
    @Generated
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}