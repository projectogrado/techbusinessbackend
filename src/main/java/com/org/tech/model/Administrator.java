package com.org.tech.model;

import com.org.tech.model.validation.anotation.ValidPassword;
import com.org.tech.model.validation.group.OnCreate;
import lombok.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "administrator")
@PrimaryKeyJoinColumn(name = "uid")
public class Administrator extends UserApp {

    @Column
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @Column
    @NotBlank
    @ValidPassword(groups = OnCreate.class)
    private String password;

    @Column
    @NotNull
    private Boolean isTemporalPassword;

    @Override
    @Generated
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Administrator that = (Administrator) o;

        return getId() != null && getId().equals(that.getId());
    }

    @Override
    @Generated
    public int hashCode() {
        return 1785962647;
    }
}
