package com.org.tech.model.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class RecoverPasswordDto {

    @NotNull
    private String oldPassword;

    @NotNull
    private String password;

    @NotNull
    private String verifiedPassword;

    @NotNull
    private String rol;

    @NotNull
    private String username;

    public boolean validatePasswordMismatch() {
        return password.equals(verifiedPassword);
    }
}
