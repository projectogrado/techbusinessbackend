package com.org.tech.model.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
public class LoginDto {

    @NotBlank
    @NotNull
    @Size(min = 8, max = 20)
    private String username;

    @NotBlank
    @NotNull
    @Size(min = 8, max = 30)
    private String password;
}
