package com.org.tech.model.dto;

import com.org.tech.model.factorie.DataNotification;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PushNotificationDto {

    @Builder.Default
    private NotificationDto notification = NotificationDto.builder().build();

    @Builder.Default
    private DataNotification data = DataNotification.builder().build();

    @Builder.Default
    private String priority = "high";

    @Builder.Default
    private String to = "/topics/all";

}
