package com.org.tech.model.dto;

import lombok.Data;

@Data
public class JwtResponseDTO {

    private final String token;
}
