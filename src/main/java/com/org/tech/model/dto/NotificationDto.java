package com.org.tech.model.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NotificationDto {

    @Builder.Default
    private String body = "notification";

    @Builder.Default
    private String title = "you have a new notification";
}
