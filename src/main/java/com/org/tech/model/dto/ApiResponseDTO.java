package com.org.tech.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ApiResponseDTO {
    int statusResponse;
    String message;

    public static ApiResponseDTO success(String message){
        return  ApiResponseDTO.builder()
                .statusResponse(HttpStatus.OK.value())
                .message(message)
                .build();
    }


}
