package com.org.tech.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.util.List;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "customer")
@PrimaryKeyJoinColumn(name = "uid")
public class Customer extends UserApp {

    @Exclude
    @OneToMany(mappedBy = "customer")
    @JsonIgnoreProperties(value = "customer")
    private List<AdmissionDataSheet> admissionDataSheets;

    @Override
    @Generated
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var customer = (Customer) o;

        return getId() != null && getId().equals(customer.getId());
    }

    @Override
    @Generated
    public int hashCode() {
        return 339958611;
    }
}
