package com.org.tech.model.keys;

import lombok.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class ChecklistRepairKey implements Serializable {

    @Column(name = "repair_route__id")
    private Long repairRouteId;

    @Column(name = "work_service_id")
    private Long workServiceId;

    @Override
    @Generated
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ChecklistRepairKey that = (ChecklistRepairKey) o;

        if (!Objects.equals(repairRouteId, that.repairRouteId)) return false;
        return Objects.equals(workServiceId, that.workServiceId);
    }

    @Override
    @Generated
    public int hashCode() {
        int result = repairRouteId != null ? repairRouteId.hashCode() : 0;
        result = 31 * result + (workServiceId != null ? workServiceId.hashCode() : 0);
        return result;
    }
}
