package com.org.tech.model.keys;

import lombok.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class BlacklistKey implements Serializable {

    @Column(name = "employee_id")
    private Long employeeId;

    @Column(name = "admission_id")
    private Long admissionId;


    @Override
    @Generated
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BlacklistKey that = (BlacklistKey) o;

        if (!Objects.equals(employeeId, that.employeeId)) return false;
        return Objects.equals(admissionId, that.admissionId);
    }

    @Override
    @Generated
    public int hashCode() {
        int result = employeeId != null ? employeeId.hashCode() : 0;
        result = 31 * result + (admissionId != null ? admissionId.hashCode() : 0);
        return result;
    }
}
