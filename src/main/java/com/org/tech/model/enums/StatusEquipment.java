package com.org.tech.model.enums;

public enum StatusEquipment {
    BY_ASSIGNING, TO_DO, IN_REPAIR, REPAIRED, NOT_REPAIRED, NOVELTY, DELIVERED

}
