package com.org.tech.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "person_receive")
@PrimaryKeyJoinColumn(name = "uid")
public class PersonReceive extends UserApp {

    @Column
    @NotBlank
    @Size(max = 50)
    private String kinship;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "discharge_data_sheet_id", referencedColumnName = "id")
    @JsonIgnoreProperties(value = "personReceive")
    private DischargeDataSheet dischargeDataSheet;

    @Override
    @Generated
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PersonReceive that = (PersonReceive) o;

        return getId() != null && getId().equals(that.getId());
    }

    @Override
    @Generated
    public int hashCode() {
        return 1599317862;
    }
}
