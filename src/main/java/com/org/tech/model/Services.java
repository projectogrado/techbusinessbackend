package com.org.tech.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.model.validation.group.OnUpdate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import java.util.List;

@Builder
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@AllArgsConstructor
@Table(name = "service")
public class Services {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Null(groups = {OnCreate.class})
    @NotNull(groups = {OnUpdate.class})
    private Long id;

    @NotBlank
    @Size(min = 5, max = 50)
    @Column(name = "name")
    private String name;

    @NotBlank
    @Size(min = 8, max = 150)
    @Column(name = "description")
    private String description;

    @Column(name = "status")
    private Boolean status;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "services", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties(value = "services")
    private List<RepairRoute> repairRoute;

    @Override
    @Generated
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        var services = (Services) o;

        return id != null && id.equals(services.id);
    }

    @Override
    @Generated
    public int hashCode() {
        return 1613619622;
    }
}
