package com.org.tech.model;

import com.org.tech.model.keys.BlacklistKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Table(name = "blacklist")
@Entity
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class Blacklist {

    @EmbeddedId
    private BlacklistKey id;

    @ManyToOne
    @MapsId("admissionId")
    @JoinColumn(name = "admission_id")
    private AdmissionDataSheet admissionDataSheet;


    @ManyToOne
    @MapsId("employeeId")
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @Column
    private String reason;


    @Override
    @Generated
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        var blacklist = (Blacklist) o;

        return id != null && id.equals(blacklist.id);
    }

    @Override
    @Generated
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}