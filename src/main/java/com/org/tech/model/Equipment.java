package com.org.tech.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.org.tech.model.enums.StatusEquipment;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.model.validation.group.OnUpdate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name = "equipment")
public class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Null(groups = {OnCreate.class})
    @NotNull(groups = {OnUpdate.class})
    private Long id;

    @Column
    @NotBlank
    private String mark;

    @Column
    @NotBlank
    private String model;

    @Column
    @NotBlank
    private String type;

    @Column
    @NotBlank
    private String hardDisk;

    @Column
    @NotBlank
    private String ram;

    @Column
    @NotBlank
    private String board;

    @Column
    @NotBlank
    private String readerCd;

    @Column
    @NotBlank
    private String other;

    @Column
    @NotBlank
    private String processor;

    @Column
    @Enumerated(EnumType.STRING)
    private StatusEquipment status;

    @UpdateTimestamp
    private LocalDateTime dateUpdatedStatus;

    @OneToOne(mappedBy = "equipment",cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = "equipment")
    private AdmissionDataSheet admissionDataSheet;

    @Override
    @Generated
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var equipment = (Equipment) o;

        return id != null && id.equals(equipment.id);
    }

    @Override
    @Generated
    public int hashCode() {
        return 1176201129;
    }
}
