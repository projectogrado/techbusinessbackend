package com.org.tech.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.model.validation.group.OnUpdate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;

@Getter
@Setter
@Builder
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "discharge_data_sheet")
public class DischargeDataSheet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Null(groups = {OnCreate.class})
    @NotNull(groups = {OnUpdate.class})
    private Long id;

    @Column(name = "date")
    @NotNull
    private Date deliverDate;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "admission_data_sheet_id", referencedColumnName = "id")
    @JsonIgnoreProperties(value = "dischargeDataSheet")
    private AdmissionDataSheet admissionDataSheet;

    @OneToOne(mappedBy = "dischargeDataSheet")
    @JsonIgnoreProperties(value = "dischargeDataSheet")
    private PersonReceive personReceive;

    @Override
    @Generated
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DischargeDataSheet that = (DischargeDataSheet) o;

        return id != null && id.equals(that.id);
    }

    @Override
    @Generated
    public int hashCode() {
        return 1562262368;
    }
}