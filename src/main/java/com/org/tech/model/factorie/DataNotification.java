package com.org.tech.model.factorie;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DataNotification {
    @Builder.Default
    String clickaction = "FLUTTERNOTIFICATIONCLICK";

    @Builder.Default
    String id = "1";

    @Builder.Default
    String status = "done";
}
