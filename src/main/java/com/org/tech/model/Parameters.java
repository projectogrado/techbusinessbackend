package com.org.tech.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.model.validation.group.OnUpdate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.time.LocalDateTime;

@Getter
@Entity
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name = "parameters")
public class Parameters {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Null(groups = {OnCreate.class})
    @NotNull(groups = {OnUpdate.class})
    private Long id;

    @Column
    @NotNull
    @Min(value = 0, message = "{field.min} 0")
    private Integer timeToDo;

    @Column
    @NotNull
    @Min(value = 0, message = "{field.min} 0")
    private Integer timeInRepair;

    @Column
    @NotNull
    @Min(value = 0, message = "{field.min} 0")
    private Integer timeRepaired;

    @Column
    @NotNull
    @Min(value = 0, message = "{field.min} 0")
    private Integer timeNoRepaired;

    @Column
    @NotNull
    @Min(value = 0, message = "{field.min} 0")
    private Integer timeNovelty;

    @Column
    @NotBlank(message = "{not.blank} cronTask")
    private String cronTask;

    @CreationTimestamp
    @JsonIgnore
    private LocalDateTime dateCreated;
}
