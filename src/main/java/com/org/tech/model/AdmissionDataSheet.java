package com.org.tech.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.model.validation.group.OnUpdate;
import lombok.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "admission_data_sheet")
public class AdmissionDataSheet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Null(groups = {OnCreate.class})
    @NotNull(groups = {OnUpdate.class})
    private Long id;

    @Column
    @CreatedDate
    private Date date;

    @Column
    @NotBlank
    private String reason;

    @Column
    @NotBlank
    private String observation;

    @Column
    private Boolean status;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id", nullable = false)
    @JsonIgnoreProperties(value = "admissionDataSheets")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    @JsonIgnoreProperties(value = "works")
    private Employee employee;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "equipment_id", nullable = false)
    @JsonIgnoreProperties(value = "admissionDataSheet")
    private Equipment equipment;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "admissionDataSheet")
    @JsonIgnoreProperties(value = "admissionDataSheet")
    private List<WorkService> workServices;

    @Override
    @Generated
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AdmissionDataSheet that = (AdmissionDataSheet) o;

        return id != null && id.equals(that.id);
    }

    @Override
    @Generated
    public int hashCode() {
        return 759001840;
    }
}