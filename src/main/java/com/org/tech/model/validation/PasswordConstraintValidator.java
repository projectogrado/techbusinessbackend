package com.org.tech.model.validation;

import com.org.tech.model.validation.anotation.ValidPassword;
import org.jetbrains.annotations.NotNull;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.RuleResult;
import org.passay.WhitespaceRule;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

    public static final int MIN_LENGTH = 8;
    public static final int MAX_LENGTH = 30;
    public static final int NUM = 1;

    @Override
    public void initialize(ValidPassword constraintAnnotation) {
        // default implementation ignored
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        PasswordValidator validator = rulesValidation();
        RuleResult result = validator.validate(new PasswordData(value));

        if (result.isValid()) {
            return true;
        }

        return isNotValid(context, validator, result);
    }

    private boolean isNotValid(ConstraintValidatorContext context, PasswordValidator validator,
                               RuleResult result) {

        List<String> messages = validator.getMessages(result);
        var messageTemplate = String.join(",", messages);

        context.buildConstraintViolationWithTemplate(messageTemplate)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();

        return false;
    }

    @NotNull
    private PasswordValidator rulesValidation() {
        return new PasswordValidator(Arrays.asList(
                new LengthRule(MIN_LENGTH, MAX_LENGTH),
                new CharacterRule(EnglishCharacterData.UpperCase, NUM),
                new CharacterRule(EnglishCharacterData.LowerCase, NUM),
                new CharacterRule(EnglishCharacterData.Digit, NUM),
                new CharacterRule(EnglishCharacterData.Special, NUM),
                new WhitespaceRule()
        ));
    }
}
