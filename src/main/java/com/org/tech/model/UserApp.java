package com.org.tech.model;

import com.org.tech.model.enums.TypeDocument;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.model.validation.group.OnUpdate;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Set;

@Data
@Entity
@Table(name = "user_tb")
@Inheritance(strategy = InheritanceType.JOINED)
public class UserApp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Null(groups = {OnCreate.class})
    @NotNull(groups = {OnUpdate.class})
    private Long id;

    @Column
    @NotNull
    private String name;

    @Column
    @NotNull
    private String lastname;

    @Column
    @NotNull
    private String document;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private TypeDocument typeDocument;

    @Column
    @Min(value = 10, message = "{field.min} 10")
    @NotNull
    private String phone;

    @Column
    @NotBlank(message = "{email.not.empty}")
    @Email
    @NotNull
    private String email;

    @Column
    @NotNull
    private String address;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;
}
