package com.org.tech.repository;

import com.org.tech.model.Parameters;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ParametersRepository extends JpaRepository<Parameters, Long> {

    Optional<Parameters> findTopByOrderByDateCreatedDesc();
}
