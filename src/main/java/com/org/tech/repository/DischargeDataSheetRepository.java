package com.org.tech.repository;

import com.org.tech.model.DischargeDataSheet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DischargeDataSheetRepository extends JpaRepository<DischargeDataSheet, Long> {

    Optional<DischargeDataSheet> findByAdmissionDataSheet_Id(long id);

}