package com.org.tech.repository;

import com.org.tech.model.Blacklist;
import com.org.tech.model.keys.BlacklistKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlacklistRepository extends JpaRepository<Blacklist, BlacklistKey> {

    List<Blacklist> findAllByIdAdmissionId(long admissionId);
}