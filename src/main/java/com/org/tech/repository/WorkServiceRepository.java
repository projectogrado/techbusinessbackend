package com.org.tech.repository;

import com.org.tech.model.WorkService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkServiceRepository extends JpaRepository<WorkService, Long> {

    List<WorkService> findAllByAdmissionDataSheet_Id(long id);
}
