package com.org.tech.repository;

import com.org.tech.model.RepairRoute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepairRouteRepository extends JpaRepository<RepairRoute, Long> {
}
