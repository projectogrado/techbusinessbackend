package com.org.tech.repository;

import com.org.tech.model.Employee;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Optional<Employee> findByUsername(String username);

    Optional<Employee> findFirstByOrderByUpdatedDateTimeAsc();

    Optional<Employee> findFirstByIdNotInOrderByUpdatedDateTimeAsc(List<Long> ids);

    List<Employee> findAllByDocument(String document, Sort sort);
}
