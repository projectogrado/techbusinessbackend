package com.org.tech.repository;

import com.org.tech.model.PersonReceive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonReceiveRepository extends JpaRepository<PersonReceive, Long> {

}
