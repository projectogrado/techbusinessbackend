package com.org.tech.repository;

import com.org.tech.model.ChecklistRepair;
import com.org.tech.model.keys.ChecklistRepairKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChecklistRepairRepository extends JpaRepository<ChecklistRepair, ChecklistRepairKey> {
}
