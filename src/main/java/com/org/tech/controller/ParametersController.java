package com.org.tech.controller;

import com.org.tech.constants.MessageSwagger;
import com.org.tech.model.Parameters;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.service.ParametersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/parameters")
@Api(value = "Parameters", tags = {"Parameters"})
public class ParametersController {

    private final ParametersService parametersService;

    @Autowired
    public ParametersController(ParametersService parametersService) {
        this.parametersService = parametersService;
    }

    @PostMapping(path = "/insert-parameters")
    @ApiOperation(value = "insert value for parameters", response = Parameters.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<Parameters> create(@Validated({OnCreate.class})
                                             @RequestBody Parameters parameters) {

        return ResponseEntity.status(HttpStatus.CREATED)
                .body(parametersService.createParameters(parameters));
    }

    @GetMapping(path = "/last-parameter")
    @ApiOperation(value = "insert value for parameters", response = Parameters.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<Parameters> getLastParameters() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(parametersService.getParameters());
    }
}
