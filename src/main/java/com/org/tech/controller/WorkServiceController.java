package com.org.tech.controller;

import com.org.tech.constants.MessageSwagger;
import com.org.tech.model.ChecklistRepair;
import com.org.tech.model.WorkService;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.service.WorkServiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/work-service")
@Api(value = "Work service", tags = {"Work service"})
public class WorkServiceController {

    private final WorkServiceService workServiceService;

    @Autowired
    public WorkServiceController(WorkServiceService workService) {
        this.workServiceService = workService;
    }

    @GetMapping(path= "all/admission/{id}")
    @ApiOperation(value = "get all services by admission", response = WorkService.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<List<WorkService>> getWorkServiceById(@PathVariable long id){
        return ResponseEntity.ok(workServiceService.getAllWorkServiceByAdmission(id));
    }
    @PostMapping(path = "/insert-work-service")
    @ApiOperation(value = "create work service", response = WorkService.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<WorkService> create(@Validated({OnCreate.class})
                                              @RequestBody WorkService workService) {

        return ResponseEntity.status(HttpStatus.CREATED)
                .body(workServiceService.create(workService));
    }

    @PatchMapping(path = "/add-checklist-work-service")
    @ApiOperation(value = "create work service", response = WorkService.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<String> addChecklist(@RequestBody List<ChecklistRepair> checklistRepair,
                                               @RequestParam(value = "id") long id) {

        return ResponseEntity.status(HttpStatus.OK)
                .body(workServiceService.addChecklistWorkService(checklistRepair, id));
    }

}
