package com.org.tech.controller;

import com.org.tech.constants.MessageSwagger;
import com.org.tech.model.Customer;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.model.validation.group.OnUpdate;
import com.org.tech.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customer")
@Api(value = "customer", tags = {"Customers"})
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping(path = "/insert-customer")
    @ApiOperation(value = "create customer", response = Customer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<Customer> create(
            @Validated({OnCreate.class}) @RequestBody Customer customer) {
        return ResponseEntity.status(HttpStatus.CREATED).body(customerService.create(customer));
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "get all customer", response = Customer.class, responseContainer = "List")
    public ResponseEntity<List<Customer>> getAllCustomer() {
        return ResponseEntity.ok(customerService.getAllCustomers());
    }

    @GetMapping(path = "/{document}")
    @ApiOperation(value = "get customer by document", response = Customer.class)
    public ResponseEntity<Customer> getCustomerByDocument(@PathVariable("document") String document) {
        return ResponseEntity.ok(customerService.getCustomerByDocument(document));
    }

    @PutMapping(path = "/update-customer")
    @ApiOperation(value = "update customer", response = Customer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<Customer> update(
            @Validated({OnUpdate.class}) @RequestBody Customer customer) {
        return ResponseEntity.ok(customerService.update(customer));
    }
}
