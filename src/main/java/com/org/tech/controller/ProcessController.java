package com.org.tech.controller;

import com.org.tech.constants.MessageSwagger;
import com.org.tech.model.Process;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.model.validation.group.OnUpdate;
import com.org.tech.service.ProcessService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/process")
@Api(value = "Process", tags = {"Process"})
public class ProcessController {

    private final ProcessService processService;

    @Autowired
    public ProcessController(ProcessService processService) {
        this.processService = processService;
    }

    @PostMapping(path = "/insert-process")
    @ApiOperation(value = "create process", response = Process.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<Process> create(
            @Validated({OnCreate.class}) @RequestBody Process process) {
        return ResponseEntity.status(HttpStatus.CREATED).body(processService.create(process));
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "get all process", response = Process.class)
    public ResponseEntity<List<Process>> getAllProcess() {
        return ResponseEntity.ok(processService.getAllProcess());
    }

    @PutMapping(path = "/update-process")
    @ApiOperation(value = "update process", response = Process.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<Process> update(
            @Validated({OnUpdate.class}) @RequestBody Process process) {
        return ResponseEntity.ok(processService.update(process));
    }
}
