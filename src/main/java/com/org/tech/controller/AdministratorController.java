package com.org.tech.controller;

import com.org.tech.constants.MessageSwagger;
import com.org.tech.model.Administrator;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.model.validation.group.OnUpdate;
import com.org.tech.service.AdministratorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin")
@Api(value = "Administrator", tags = {"Administrator"})
public class AdministratorController {

    private final AdministratorService administratorService;

    @Autowired
    public AdministratorController(AdministratorService administratorService) {
        this.administratorService = administratorService;
    }

    @PostMapping(path = "/insert-administrator")
    @ApiOperation(value = "create administrator", response = Administrator.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    @Validated(OnCreate.class)
    public ResponseEntity<Administrator> create(@Validated({OnCreate.class})
                                                @RequestBody Administrator administrator) {

        return ResponseEntity.status(HttpStatus.CREATED)
                .body(administratorService.create(administrator));
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "retrieve administrator", response = Administrator.class, responseContainer = "List")
    public ResponseEntity<List<Administrator>> getAllAdministrators() {
        return ResponseEntity.ok(administratorService.getAllAdministrators());
    }


    @PutMapping(path = "/update-administrator")
    @ApiOperation(value = "update administrator", response = Administrator.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    @Validated(OnUpdate.class)
    public ResponseEntity<Administrator> update(@Validated({OnUpdate.class})
                                                @RequestBody Administrator administrator) {
        return ResponseEntity.ok(administratorService.update(administrator));
    }

}
