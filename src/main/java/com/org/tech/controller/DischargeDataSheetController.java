package com.org.tech.controller;

import com.org.tech.constants.MessageSwagger;
import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.DischargeDataSheet;
import com.org.tech.service.DischargeDataSheetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/discharge")
@Api(value = "Discharge Data Sheet", tags = {"Discharge Data Sheet"})
public class DischargeDataSheetController {

    private final DischargeDataSheetService dischargeDataSheetService;

    @Autowired
    public DischargeDataSheetController(DischargeDataSheetService dischargeDataSheetService) {
        this.dischargeDataSheetService = dischargeDataSheetService;
    }

    @GetMapping(path = "/{id}")
    @ApiOperation(value = "get discharge data sheet by id", response = AdmissionDataSheet.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = MessageSwagger.NOT_FOUND),
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<DischargeDataSheet> getDischargeDataSheetByAdmissionId(@PathVariable long id) {
        return ResponseEntity.ok(dischargeDataSheetService.getDischargeDataSheetById(id));
    }
}
