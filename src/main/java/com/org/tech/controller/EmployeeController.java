package com.org.tech.controller;

import com.org.tech.constants.MessageSwagger;
import com.org.tech.model.Employee;
import com.org.tech.model.UserApp;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.model.validation.group.OnUpdate;
import com.org.tech.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/employee")
@Api(value = "Employee", tags = {"Employee"})
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping(path = "/insert-employee")
    @ApiOperation(value = "create employee", response = UserApp.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<Employee> create(@Validated({OnCreate.class})
                                           @RequestBody Employee employee) {
        return ResponseEntity.status(HttpStatus.CREATED).body(employeeService.create(employee));
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "retrieve employee", response = Employee.class, responseContainer = "List")
    public ResponseEntity<List<Employee>> getAllEmployees(@RequestParam Map<String, String> params) {
        return ResponseEntity.ok(employeeService.getAllEmployees(params));
    }

    @GetMapping(path = "/me")
    @ApiOperation(value = "retrieve employee logged", response = Employee.class)
    public ResponseEntity<Employee> getAllContext() {
        return ResponseEntity.ok(employeeService.getEmployeeContext());
    }

    @PutMapping(path = "/update-employee")
    @ApiOperation(value = "update employee", response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<Employee> update(@Validated({OnUpdate.class})
                                           @RequestBody Employee employee) {
        return ResponseEntity.ok(employeeService.update(employee));
    }
}
