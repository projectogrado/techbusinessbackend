package com.org.tech.controller;

import com.org.tech.constants.MessageSwagger;
import com.org.tech.model.AdmissionDataSheet;
import com.org.tech.model.dto.ApiResponseDTO;
import com.org.tech.model.enums.StatusEquipment;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.service.AdmissionDataSheetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.org.tech.model.dto.ApiResponseDTO.success;

@RestController
@RequestMapping("/admission")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET})
@Api(value = "Admission Data Sheet", tags = {"Admission Data Sheet"})
public class AdmissionDataSheetController {

    private final AdmissionDataSheetService admissionService;

    @Autowired
    public AdmissionDataSheetController(AdmissionDataSheetService admissionService) {
        this.admissionService = admissionService;
    }

    @PostMapping(path = "/insert-admission")
    @ApiOperation(value = "create admission", response = AdmissionDataSheet.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<AdmissionDataSheet> create(
            @Validated({OnCreate.class}) @RequestBody AdmissionDataSheet admissionDataSheet) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(admissionService.create(admissionDataSheet));
    }

    @PatchMapping(path = "/update-status-admission")
    @ApiOperation(value = "update status an admission", response = ApiResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<ApiResponseDTO> terminateAdmission(@RequestParam("id") long id,
                                                             @RequestParam("status") StatusEquipment status) {
        return ResponseEntity.ok(success(admissionService.terminateAdmission(id, status)));

    }

    @GetMapping(path = "/public/{id}")
    @ApiOperation(value = "get AdmissionDataSheet by id", response = AdmissionDataSheet.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = MessageSwagger.NOT_FOUND),
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<AdmissionDataSheet> getAdmissionDataSheet(@PathVariable long id) {
        return ResponseEntity.ok(admissionService.getAdmissionDataSheetById(id));
    }

    @GetMapping(path = "/generate-pdf/{id}")
    @ApiOperation(value = "get AdmissionDataSheet by id", response = AdmissionDataSheet.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = MessageSwagger.NOT_FOUND),
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<String> getPdf(@PathVariable long id) {
        return ResponseEntity.ok(admissionService.exportAdmissionPdf(id));
    }

    @GetMapping(path = "/send-pdf/{id}")
    @ApiOperation(value = "get AdmissionDataSheet by id", response = AdmissionDataSheet.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = MessageSwagger.NOT_FOUND),
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<ApiResponseDTO> sendPdf(@PathVariable long id) {
        return ResponseEntity.ok(success(admissionService.sendPdf(id)));
    }

    @GetMapping(path = "/climb-job")
    @ApiOperation(value = "climb job ", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<String> climbJob(@RequestParam("admission") Long admissionId) {
        admissionService.climbJob(admissionId);
        return ResponseEntity.ok("ok");
    }
}
