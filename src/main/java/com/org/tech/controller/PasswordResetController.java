package com.org.tech.controller;

import com.org.tech.constants.MessageEnum;
import com.org.tech.constants.MessageSwagger;
import com.org.tech.exception.PasswordMismatchException;
import com.org.tech.model.dto.RecoverPasswordDto;
import com.org.tech.providers.MessageProvider;
import com.org.tech.service.PasswordResetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/password-reset")
@Api(value = "password reset", tags = {"password reset"})
public class PasswordResetController {

    private final PasswordResetService passwordResetService;
    private final MessageProvider message;

    @Autowired
    public PasswordResetController(PasswordResetService passwordResetService, MessageProvider message) {
        this.passwordResetService = passwordResetService;
        this.message = message;
    }

    @PostMapping(path = "/recover-password")
    @ApiOperation(value = "password reset", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<String> resetPassword(@RequestParam("username") String username) {
        return ResponseEntity.ok(passwordResetService.recoverPassword(username));
    }


    @PutMapping(path = "/update-password")
    @ApiOperation(value = "password reset", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<String> setPassword(@RequestBody RecoverPasswordDto recoverPasswordDto) {
        if (recoverPasswordDto.validatePasswordMismatch()) {
            return ResponseEntity.ok(passwordResetService.setPassword(recoverPasswordDto));
        }
        throw new PasswordMismatchException(message.get(MessageEnum.PASSWORD_MISMATCH));
    }


}
