package com.org.tech.controller;

import com.org.tech.model.dto.JwtResponseDTO;
import com.org.tech.model.dto.LoginDto;
import com.org.tech.service.AuthService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
@Api(value = "Authentication", tags = {"Authentication"})
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<JwtResponseDTO> authentication(@Valid @RequestBody LoginDto login) {
        return ResponseEntity.ok(authService.authentication(login));
    }
}
