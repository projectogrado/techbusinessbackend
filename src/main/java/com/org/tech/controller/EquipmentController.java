package com.org.tech.controller;

import com.org.tech.constants.MessageSwagger;
import com.org.tech.model.Equipment;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.model.validation.group.OnUpdate;
import com.org.tech.service.EquipmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/equipment")
@Api(value = "equipment", tags = {"Equipment"})
public class EquipmentController {

    private final EquipmentService equipmentService;

    @Autowired
    public EquipmentController(EquipmentService equipmentService) {
        this.equipmentService = equipmentService;
    }

    @PostMapping(path = "/insert-equipment")
    @ApiOperation(value = "create equipment", response = Equipment.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<Equipment> create(
            @Validated({OnCreate.class}) @RequestBody Equipment equipment) {
        return ResponseEntity.status(HttpStatus.CREATED).body(equipmentService.create(equipment));
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "get all equipment", response = Equipment.class)
    public ResponseEntity<List<Equipment>> getAll(@RequestParam Map<String, String> params) {
        return ResponseEntity.ok(equipmentService.getAllEquipment(params));
    }

    @PutMapping(path = "/update-equipment")
    @ApiOperation(value = "update equipment", response = Equipment.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<Equipment> update(
            @Validated({OnUpdate.class}) @RequestBody Equipment equipment) {
        return ResponseEntity.ok(equipmentService.update(equipment));
    }
}
