package com.org.tech.controller;

import com.org.tech.constants.MessageSwagger;
import com.org.tech.model.RepairRoute;
import com.org.tech.model.Services;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.model.validation.group.OnUpdate;
import com.org.tech.service.ServiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/services")
@Api(value = "Service", tags = {"Services"})
public class ServiceController {

    private final ServiceService serviceService;

    @Autowired
    public ServiceController(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    @PostMapping(path = "/insert-service")
    @ApiOperation(value = "create service", response = Services.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<Services> create(
            @Validated({OnCreate.class}) @RequestBody Services service) {
        return ResponseEntity.status(HttpStatus.CREATED).body(serviceService.create(service));
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "get all service", response = Services.class)
    public ResponseEntity<List<Services>> getAllService() {
        return ResponseEntity.ok(serviceService.getAllService());
    }

    @PutMapping(path = "/update-service")
    @ApiOperation(value = "Update services", response = Services.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<Services> update(
            @Validated({OnUpdate.class}) @RequestBody Services servicesEntity) {
        return ResponseEntity.ok(serviceService.update(servicesEntity));
    }

    @GetMapping(path = "/{id}/checklist")
    @ApiOperation(value = "get check_list", response = Services.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<List<RepairRoute>> getChecklist(@PathVariable long id) {
        return ResponseEntity.ok(serviceService.getServiceById(id).getRepairRoute());
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "delete Service for id", response = Services.class)
    public String deleteById(@PathVariable(name = "id") Long id) {
        serviceService.delete(id);
        return "Se eliminó correctamente";
    }
}
