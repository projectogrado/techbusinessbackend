package com.org.tech.controller;

import com.org.tech.constants.MessageSwagger;
import com.org.tech.model.PersonReceive;
import com.org.tech.model.validation.group.OnCreate;
import com.org.tech.service.PersonReceiveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person-receive")
@Api(value = "Person Receive", tags = {"Person Receive"})
public class PersonReceiveController {

    private final PersonReceiveService personReceiveService;

    @Autowired
    public PersonReceiveController(PersonReceiveService personReceiveService) {
        this.personReceiveService = personReceiveService;
    }

    @PostMapping(path = "/insert-person-receive")
    @ApiOperation(value = "create person receive", response = PersonReceive.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<PersonReceive> create(@Validated({OnCreate.class})
                                                @RequestBody PersonReceive personReceive) {

        return ResponseEntity.status(HttpStatus.CREATED)
                .body(personReceiveService.create(personReceive));
    }
}
