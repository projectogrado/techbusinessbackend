package com.org.tech.providers;

import com.org.tech.constants.MessageEnum;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class MessageProvider {

    private final MessageSource messageSource;

    private MessageSourceAccessor accessor;

    public MessageProvider(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @PostConstruct
    private void init() {
        accessor = new MessageSourceAccessor(messageSource, LocaleContextHolder.getLocale());
    }

    public String get(MessageEnum code) {
        return accessor.getMessage(code.getCode());
    }

    public String get(MessageEnum code, Object[] args) {
        return accessor.getMessage(code.getCode(), args);
    }

}
